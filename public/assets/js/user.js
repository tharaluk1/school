$(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    let dataTableUser = $('#dataTableUser').DataTable({
        "dom": "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'l><'col-xs-12 col-sm-4 col-md-4 col-lg-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'i><'col-xs-12 col-sm-4 col-md-4 col-lg-6'p>>",
        "language": dataTableLanguage(),
        "responsive": true,
        "scrollX": true,
        "fixedHeader": true,
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        "stateSave": true,
        stateSaveCallback: function(settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
        },
        stateLoadCallback: function(settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
        },
        "ajax": {
            url: base_url + "users/data_table_query",
            "type": "GET",
            dataSrc: function(d) {
                let start = d.start;
                $.each(d.data, function(index, item) {
                    item['no'] = parseInt(start) + index + 1;
                });
                return d.data;
            },
            error: function(xhr, error, thrown) {
                //console.log(error)
            }
        },
        "order": [
            [2, "asc"]
        ],
        "columns": [{
            "data": 'no'
        }, {
            "data": 'image'
        }, {
            "data": "name"
        }, {
            "data": "email"
        }, {
            "data": "role"
        }, {
            "data": "last_login"
        }, {
            "data": null,
            'defaultContent': ''
        }, {
            "data": null,
            'defaultContent': ''
        }],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": [0],
            "width": "5%",
            "className": 'text-center'
        }, {
            "searchable": false,
            "orderable": false,
            "targets": [1],
            "width": "10%",
            "className": 'text-center',
            render: function(data, type, full, meta) {
                if (type === 'display') {
                    data = '<div><img src="' + full.image + '" onerror="this.src=\'/uploads/users/default.png\'" width="40" height="40" alt="" class="img-avtar"></div>'
                    return data
                }
            }
        }, {
            "searchable": true,
            "orderable": true,
            "targets": [2],
            "width": "20%",
            "className": 'text-center'
        }, {
            "searchable": true,
            "orderable": true,
            "targets": [3],
            "width": "10%",
            "className": 'text-center'
        }, {
            "searchable": true,
            "orderable": true,
            "targets": [4],
            "width": "10%",
            "className": 'text-center'

        }, {
            "searchable": true,
            "orderable": true,
            "targets": [5],
            "width": "15%",
            "className": 'text-center',
            "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss')
        }, {
            "searchable": true,
            "orderable": false,
            "targets": [6],
            "width": "10%",
            "className": 'text-center',
            render: function(data, type, full, meta) {
                if (type === 'display') {
                    if (full.status == 1) {
                        data = '<div><input type="checkbox" id="' + full.id + ',false" class="js-switch switchUser" checked/></div>'
                    } else {
                        data = '<div><input type="checkbox" id="' + full.id + ',true" class="js-switch switchUser"/></div>'
                    }
                    return data
                }
            }
        }, {
            "searchable": false,
            "orderable": false,
            "targets": [7],
            "width": "30%",
            "className": 'text-center',
            render: function(data, type, full, meta) {
                if (type === 'display') {
                    data = '<div>' +
                        '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnView" title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>' +
                        '&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnEdit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>' +
                        '&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnDelete"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
                        '</div>';
                    return data
                }
            }
        }]
    })


    dataTableUser.buttons('.buttons-html5').remove();

    dataTableUser.on('draw', function() {

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        elems.forEach(function(html) {
            var switchery = new Switchery(html, { size: 'small' });
        });
        // elems.onchange = function() {
        //     alert(changeCheckbox.checked);
        // };
        // window.updateUserStatus = (id, status) => {
        //     $.get( '<?php echo url('users/change_status') ?>/'+id, {
        //       status: status
        //     }, (data, status) => {
        //       if (data=='done') {
        //         // code
        //       }else{
        //         alert('Unable to change Status ! Try Again');
        //       }
        //     })

        $('.switchUser').change(function() {
            let idStatus = $(this).attr("id").split(",");
            let id = idStatus[0];
            let status = idStatus[1];
            $.get(base_url + 'users/change_status/' + id, {
                status: status
            }, (data, status) => {
                if (data == 'done') {
                    // code
                } else {
                    //alert('Unable to change Status ! Try Again');
                }
            });
        });

        $('.btnView').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการดูข้อมูล", function() {
                window.location = base_url + 'users/view/' + id;
            }, function() {
                // alert("cancel: " + id);
            });
        });

        $('.btnEdit').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการแก้ไขข้อมูล", function() {
                window.location = base_url + 'users/edit/' + id;
            }, function() {
                // alert("cancel: " + id);
            });
        });

        $('.btnDelete').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                window.location = base_url + 'users/delete/' + id;
            }, function() {
                // alert("cancel: " + id);
            });

        });
    });
});