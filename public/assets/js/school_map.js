let mMap = null;
let mLat = schoolLat;
let mLng = schoolLng;
let makerLocation = null;
let schoolLocation = L.latLng(mLat, mLng)
$(document).ready(function() {
    initInputLatLng()
    loadMap()
})

function loadMap() {
    mMap = L.map('mapid', {
        center: schoolLocation,
        zoom: 14
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mMap);

    makerLocation = L.marker(schoolLocation, {
        draggable: true
    }).addTo(mMap);

    makerLocation.on('move', function(ev) {
        let makerLatlng = ev.latlng
        $("#school_lat").val(makerLatlng.lat);
        $("#school_lng").val(makerLatlng.lng);
        console.log(makerLatlng);
    });

    setLocation();
}

function setLocation() {
    if (mMap != null) {
        if (mLat != null && mLng != null) {
            schoolLocation = L.latLng(mLat, mLng);
            mMap.setView(schoolLocation);
            makerLocation.setLatLng(schoolLocation);
        }
    }
}

function initInputLatLng() {
    $("#school_lat").blur(function() {
        mLat = $("#school_lat").val()
        setLocation();
    });

    $("#school_lng").blur(function() {
        mLng = $("#school_lng").val()
        setLocation();
    });
}