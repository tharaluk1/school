$(document).ready(function() {

    $('.form-validate').validate({
        debug: false
    });

    $.fn.dataTable.ext.errMode = 'none';
    let dataTableSchoolData = $('#dataTableSchoolData').DataTable({
        "dom": "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'l><'col-xs-12 col-sm-4 col-md-4 col-lg-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'i><'col-xs-12 col-sm-4 col-md-4 col-lg-6'p>>",
        "language": dataTableLanguage(),
        "responsive": true,
        "scrollX": true,
        "fixedHeader": true,
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        "stateSave": true,
        stateSaveCallback: function(settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
        },
        stateLoadCallback: function(settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
        },
        "ajax": {
            url: base_url + "manage_schools_data/data_table_query",
            "type": "GET",
            dataSrc: function(d) {
                console.log(d);
                let start = d.start;
                $.each(d.data, function(index, item) {
                    item['no'] = parseInt(start) + index + 1;
                });
                return d.data;
            },
            error: function(xhr, error, thrown) {
                //console.log(error)
            }
        },
        "order": [
            [2, "desc"]
        ],
        "columns": [{
            "data": 'no'
        }, {
            "data": "school_affair"
        }, {
            "data": "name"
        }, {
            "data": "updated_date"
        }, {
            "data": null,
            'defaultContent': ''
        }, ],
        "columnDefs": [{
                "width": "5%",
                "searchable": false,
                "orderable": false,
                "targets": [0],
                "className": 'text-center'
            },
            {
                "searchable": true,
                "orderable": true,
                "targets": [1],
                "width": "25%",
                "className": 'text-center'
            }, {
                "searchable": true,
                "orderable": true,
                "targets": [2],
                "width": "35%",
                "className": 'text-center'
            }, {
                "targets": [3],
                "searchable": false,
                "orderable": true,
                "width": "20%",
                "className": 'text-center',
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss')
            }, {
                "searchable": false,
                "orderable": false,
                "targets": [4],
                "width": "15%",
                "className": 'text-center',
                render: function(data, type, full, meta) {
                    if (type === 'display') {
                        data = '<div>' +
                            '<a href="javascript:void(0);" id="' + full.id + '"  class="btn btn-sm btn-default btnEdit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>' +
                            '&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnDelete"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
                            '</div>';
                        return data
                    }
                }
            }
        ]
    })
    dataTableSchoolData.buttons('.buttons-html5').remove();

    dataTableSchoolData.on('draw', function() {

        $('.btnEdit').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการแก้ไขข้อมูล", function() {
                window.location = base_url + 'manage_schools_data/edit/' + id;
            }, function() {
                // alert("cancel: " + id);
            });
        });

        $('.btnDelete').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                window.location = base_url + 'manage_schools_data/delete/' + id;
            }, function() {
                // alert("cancel: " + id);
            });

        });
    });

    $('#btn_submit').on('click', function() {
        if (school_check_list_image['1'] == undefined)
            school_check_list_image['1'] = []

        if (school_check_list_image['2'] == undefined)
            school_check_list_image['2'] = []

        if (school_check_list_image['3'] == undefined)
            school_check_list_image['3'] = []

        if (school_check_list_image['4'] == undefined)
            school_check_list_image['4'] = []

        let school_check_list_image_1 = ""
        for (var i = 0; i < school_check_list_image['1'].length; i++) {
            if (school_check_list_image['1'][i].file_base == "")
                continue;
            school_check_list_image_1 += (school_check_list_image['1'][i].file_base + "|")
        }

        let school_check_list_image_2 = ""
        for (var i = 0; i < school_check_list_image['2'].length; i++) {
            if (school_check_list_image['2'][i].file_base == "")
                continue;
            school_check_list_image_2 += (school_check_list_image['2'][i].file_base + "|")
        }

        let school_check_list_image_3 = ""
        for (var i = 0; i < school_check_list_image['3'].length; i++) {
            if (school_check_list_image['3'][i].file_base == "")
                continue;
            school_check_list_image_3 += (school_check_list_image['3'][i].file_base + "|")
        }

        let school_check_list_image_4 = ""
        for (var i = 0; i < school_check_list_image['4'].length; i++) {
            if (school_check_list_image['4'][i].file_base == "")
                continue;
            school_check_list_image_4 += (school_check_list_image['4'][i].file_base + "|")
        }

        $('#frm_school_data').append('<input type="hidden" name="school_check_list_1" value="' + school_check_list_image_1 + '" /> ');
        $('#frm_school_data').append('<input type="hidden" name="school_check_list_2" value="' + school_check_list_image_2 + '" /> ');
        $('#frm_school_data').append('<input type="hidden" name="school_check_list_3" value="' + school_check_list_image_3 + '" /> ');
        $('#frm_school_data').append('<input type="hidden" name="school_check_list_4" value="' + school_check_list_image_4 + '" /> ');
        $('#frm_school_data').submit();
    });
});

var clone = {};
var debug = true;

// FileClicked()
function fileClicked(event) {
    var fileElement = event.target;
    if (fileElement.value != "") {
        if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
        clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
    }
    //What ever else you want to do when File Chooser Clicked
}

// FileChanged()
function fileChanged(event) {
    var fileElement = event.target;
    if (fileElement.value == "") {
        if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
        clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
        $(fileElement).remove(); //'Removing Original'
    }
    //What ever else you want to do when File Chooser Changed
}

function school_check_list_image_click(event, element_id) {
    if (school_check_list_image[element_id] == undefined)
        school_check_list_image[element_id] = []
}

function school_check_list_image_change(event, element_id) {
    let target = $(event).clone()


    if (event.value == "" || target[0].files == undefined) {
        return
    }

    if (school_check_list_image[element_id] == undefined) {
        $(event).val("");
        return
    }

    $(event).val("");

    if ((school_check_list_image[element_id].length + target[0].files.length) <= max_upload_check_list) {

        let id = ".image_check_list_item_" + element_id;

        //$(id).append(target[0].outerHTML);

        let show_check_list_image_id = "show_check_list_image_" + element_id;

        for (let i = 0; i < target[0].files.length; i++) {
            if (target[0].files && target[0].files[i]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    let file = target[0].files[i]
                    let file_item = {
                        "name": file.name,
                        "lastModified": file.lastModified + '_' + make_id(),
                        "file_base": e.target.result
                    }
                    school_check_list_image[element_id].push(file_item)

                    let size = (school_check_list_image[element_id].length - 1)

                    let preview_id = 'image_preview_' + file_item.lastModified + '_' + element_id;

                    let image_tag_preview = '<div class="col-md-2 img-wraps" id="' + preview_id + '"><span onclick="on_delete_image_check_lists(' + preview_id + ',' + element_id + ',' + '\'' + file_item.lastModified + '\'' + ',\'add\'' + ')" class="closes" title="Delete">&times;</span><img src="' + e.target.result + '" class="img-responsive image_check_list"></div>'

                    $('#' + show_check_list_image_id).append(image_tag_preview);

                    show_count(element_id, size + 1)
                };

                reader.readAsDataURL(target[0].files[i]);
            }
        }

    } else {
        $(event).val("");
        showAlertDialog('แจ้งเตือน', "ไม่สามารถเพิ่มรูปภาพเกิน " + max_upload_check_list + " รูป", function(dialog) {

        })
    }

    console.log($('#image_check_list_' + element_id));
}

function on_delete_image_check_lists(preview_id, element_id, file_lastModified, action) {
    if (school_check_list_image[element_id] == undefined)
        school_check_list_image[element_id] = []
    preview_id.remove()

    if (action == 'delete') {
        //TODO: Call api delete
        let body = {
            "school_id": school_id,
            "school_check_list_id": element_id,
            "school_check_list_image_id": file_lastModified,
        }

        send_delete_image_check_list(body);
    }

    for (var j = 0; j < school_check_list_image[element_id].length; j++) {
        if (school_check_list_image[element_id][j].lastModified == file_lastModified) {
            school_check_list_image[element_id].splice(j, 1);
            break
        }
    }

    show_count(element_id, school_check_list_image[element_id].length)
}

function show_count(element_id, size) {
    $('#current_add_image_' + element_id).text(size);
}

function make_id() {
    let length = 10
    let result = '';
    let characters = 'abcefgh0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function send_delete_image_check_list(data) {
    $.ajax({
        type: "POST",
        url: base_url + 'manage_schools_data/school_check_list_image_delete',
        data: data,
        dataType: 'json',
        success: function(result) {
            console.log(result)
        }
    });
}