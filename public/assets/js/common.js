function showConfirmDialog(title = "แจ้งเตือน",
    message,
    onConfirmClick,
    onCancelClick) {
    $.confirm({
        title: title,
        content: message,
        type: 'blue',
        columnClass: 'medium',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'ยืนยัน',
                btnClass: 'btn-green',
                action: function() {
                    onConfirmClick()
                }
            },
            cancel: {
                text: 'ยกเลิก',
                btnClass: 'btn-default',
                action: function() {
                    onCancelClick()
                }
            }
        }
    });
}

function showAlertDialog(title = "แจ้งเตือน", message, onClick) {
    $.confirm({
        title: title,
        content: message,
        type: 'blue',
        columnClass: 'medium',
        typeAnimated: true,
        autoClose: true,
        buttons: {
            confirm: {
                text: 'ตกลง',
                btnClass: 'btn-green',
                action: function() {
                    onClick()
                }
            }
        }
    });
}