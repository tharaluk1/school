function dataTableLanguage() {
    return {
        "search": "ค้นหา",
        "searchPlaceholder": "พิมพ์ข้อความที่นี่",
        "emptyTable": "ไม่พบข้อมูล",
        "info": "แสดง _START_ ถึง _END_ จากทั้งหมด _TOTAL_ แถว",
        "lengthMenu": "แสดง _MENU_ แถว",
        "paginate": {
            "first": "แรก",
            "last": "สุดท้าย",
            "next": "ถัดไป",
            "previous": "ย้อน"
        }
    };
}