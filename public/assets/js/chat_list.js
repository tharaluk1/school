$(document).ready(function() {

    $('.form-validate').validate({
        debug: false
    });

    $.fn.dataTable.ext.errMode = 'none';
    let dataTableImg = $('#dataTableImg').DataTable({
        "dom": "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'l><'col-xs-12 col-sm-4 col-md-4 col-lg-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'i><'col-xs-12 col-sm-4 col-md-4 col-lg-6'p>>",
        "language": dataTableLanguage(),
        "responsive": true,
        "scrollX": true,
        "fixedHeader": true,
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        "stateSave": true,
        stateSaveCallback: function(settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
        },
        stateLoadCallback: function(settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
        },
        "ajax": {
            url: base_url + "manage_chat/data_chat_list",
            "type": "GET",
            dataSrc: function(d) {
                let start = d.start;
                $.each(d.data, function(index, item) {
                    item['no'] = parseInt(start) + index + 1;
                });
                return d.data;
                
            },
            error: function(xhr, error, thrown) {
                //console.log(error)
            }
        },
        "order": [
            [2, "desc"]
        ],
        "columns": [{
            "data": 'no'
        }, {
            "data": "cnt"
        }, {
            "data": "u_email"
        }, {
            "data": "update_date"
        }, {
            "data": null,
            'defaultContent': ''
        }, ],
        "columnDefs": [{
                "width": "5%",
                "searchable": false,
                "orderable": false,
                "targets": [0],
                "className": 'text-center'
            },
            {
                "searchable": true,
                "orderable": true,
                "targets": [1],
                "width": "25%",
                "className": 'text-center'
            }, {
                "searchable": true,
                "orderable": true,
                "targets": [2],
                "width": "35%",
                "className": 'text-center'
            }, {
                "targets": [3],
                "searchable": false,
                "orderable": true,
                "width": "20%",
                "className": 'text-center',
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss')
            }, {
                "searchable": false,
                "orderable": false,
                "targets": [4],
                "width": "15%",
                "className": 'text-center',
                render: function(data, type, full, meta) {
                    // console.log(full);
                    if (type === 'display') {
                        data = '<div>' +
                            '<a href="javascript:void(0);"  room="' + full.room +'"  class="btn btn-sm btn-default btnEdit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>' +
                            '&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" room="' + full.room + '" class="btn btn-sm btn-default btnDelete"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
                            '</div>';
                        return data
                    }
                }
            }
        ]
    })
    dataTableImg.buttons('.buttons-html5').remove();

    dataTableImg.on('draw', function() {

        $('.btnEdit').on('click', function() {
            let room = $(this).attr("room")

            //console.log(ids);return;

            showConfirmDialog("แจ้งเตือน", "เข้าสู่ห้องแชท", function() {
                window.location = `${base_url}/manage_chat/room/${room}`;
            }, function() {
                // alert("cancel: " + id);
            });
        });

        $('.btnDelete').on('click', function() {
            let room = $(this).attr("room");
            // alert(room);return;
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                window.location = base_url + 'manage_chat/delete_chat_room/' + room;
            }, function() {
                // alert("cancel: " + id);
            });

        });
    });

    
});