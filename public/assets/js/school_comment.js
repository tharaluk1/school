$(document).ready(function() {

    let school_id_filter = "0";
    let school_checklist_id_filter = "0";

    $.fn.dataTable.ext.errMode = 'none';
    let dataTableSchoolComment = $('#dataTableSchoolComment').DataTable({
            //"dom": "<'row'<'col-md-4'l><'#school_name.col-md-6'><'col-md-2'f>>",
            "dom": "<'row'<'col-xs-12 col-sm-6 col-md-6 col-lg-2'l><'#school_name.col-xs-12 col-sm-6 col-md-6 col-lg-4 dataTables_length margin_top_8px'><'#check_list_name.col-xs-12 col-sm-6 col-md-6 col-lg-4 dataTables_length margin_top_8px'><'col-xs-12 col-sm-6 col-md-6 col-lg-2 dataTables_length margin_top_8px'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-xs-12 col-sm-5'i><'col-xs-12 col-sm-7'p>>",
            "language": dataTableLanguage(),
            "responsive": true,
            "scrollX": true,
            "fixedHeader": true,
            "processing": true,
            "autoWidth": true,
            "serverSide": true,
            "stateSave": true,
            stateSaveCallback: function(settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
            },
            stateLoadCallback: function(settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
            },
            "ajax": {
                url: base_url + "manage_schools_comment/data_table_query/" + school_id_filter + "/" + school_checklist_id_filter,
                "type": "GET",
                dataSrc: function(d) {
                    let start = d.start;
                    $.each(d.data, function(index, item) {
                        item['no'] = parseInt(start) + index + 1;
                    });
                    return d.data;
                },
                error: function(xhr, error, thrown) {
                    //console.log(error)
                }
            },
            "order": [
                [2, "desc"]
            ],
            "columns": [{
                "data": 'no'
            }, {
                "data": "comment_name"
            }, {
                "data": "comment_message"
            }, {
                "data": "update_date"
            }, {
                "data": null,
                'defaultContent': ''
            }],
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": [0],
                "width": "5%",
                "className": 'text-center'
            }, {
                "searchable": true,
                "orderable": true,
                "targets": [1],
                "width": "20%",
                "className": 'text-center'
            }, {
                "searchable": true,
                "orderable": true,
                "targets": [2],
                "width": "45%",
                "className": 'text-left'
            }, {
                "targets": [3],
                "searchable": false,
                "orderable": true,
                "className": 'text-center',
                "width": "15%",
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss')
            }, {
                "searchable": false,
                "orderable": false,
                "targets": [4],
                "width": "10%",
                "className": 'text-center',
                render: function(data, type, full, meta) {
                    if (type === 'display') {
                        data = '<div>' +
                            '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnDelete"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
                            '&nbsp;&nbsp;' +
                            '<label><input type="checkbox" class="items" id="' + full.id + '" name="items[]" value="' + full.id + '"></label>' +
                            '</div>';
                        return data
                    }
                }
            }]
        })
        //dataTableSchoolComment.buttons('.buttons-html5').remove();

    dataTableSchoolComment.on('draw', function() {

        $('.btnDelete').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                window.location = base_url + 'manage_schools_comment/delete/' + id;
            }, function() {
                // alert("cancel: " + id);
            });

        });
    });



    $("#school_name").append('<div><label class="label_regular" for="school_name_filter">ชื่อสถานศึกษา</label> <select id="school_name_filter" class="form-control input-sm"><option selected value="0">ทั้งหมด</option></select></div>');
    $("#check_list_name").append('<div><label class="label_regular" for="check_list_name_filter">ชื่อ Check List</label> <select id="check_list_name_filter" class="form-control input-sm"><option selected value="0">ทั้งหมด</option></select></div>');

    loadSchool();

    loadSchoolCheckList();

    $('#btn_delete_select').on('click', function() {
        console.log($(".items:checked"));
        if ($(".items:checked").length < 1) {
            showAlertDialog("แจ้งเตือน", "กรุณาเลือกรายการที่ต้องการลบ", function() {});
        } else {
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                var delete_ids = [];
                $.each($(".items:checked"), function() {
                    delete_ids.push($(this).val());
                });
                window.location = base_url + 'manage_schools_comment/delete/' + encodeURIComponent(delete_ids.join(","));
            }, function() {
                // alert("cancel: " + id);
            });
        }
    });

    $('body').on('click', '.items', function() {
        checkSelectAll();
    });
    $('body').on('click', '#select_all', function() {
        if ($(this).is(':checked')) {
            selectAll()
        } else {
            unSelectAll();
        }
    });

    $("#school_name_filter").change(function() {
        school_id_filter = $(this).val();
        reloadTableView();
    });

    $("#check_list_name_filter").change(function() {
        school_checklist_id_filter = $(this).val();
        reloadTableView();
    });

    function reloadTableView() {
        dataTableSchoolComment.ajax.url(base_url + "manage_schools_comment/data_table_query/" + school_id_filter + "/" + school_checklist_id_filter).load();
    }

    function checkSelectAll() {
        if (($('.items').length == $('.items:checked').length) && $('.items').length > 0) {
            $("#select_all").prop("checked", true);
        } else {
            $("#select_all").removeAttr("checked");
        }
    }

    function selectAll() {
        var items = $('.items');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox') items[i].checked = true;
        }
    }

    function unSelectAll() {
        var items = $('.items');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox') items[i].checked = false;
        }
    }



    function loadSchool() {
        $.ajax(
            base_url + "api/v1/school/list", {
                type: "POST",
                dataType: "json",
                headers: {
                    "ZIGMANICE-API-KEY": zigmanice_api_key
                }
            }
        ).done(function(res) {
            if (res.statusCode == 200) {
                res.data.school_list.forEach(item => {
                    $("#school_name_filter").append('<option value="' + item.school_id + '">' + item.name + '</option>');
                });
            } else {

            }
        }).fail(function() {

        });
    }

    function loadSchoolCheckList() {
        $.ajax(
            base_url + "api/v1/school/check_list_master", {
                type: "POST",
                dataType: "json",
                headers: {
                    "ZIGMANICE-API-KEY": zigmanice_api_key
                }
            }
        ).done(function(res) {
            if (res.statusCode == 200) {
                res.data.forEach(item => {
                    $("#check_list_name_filter").append('<option value="' + item.id + '">' + item.name + '</option>');
                });
            } else {

            }
        }).fail(function() {

        });
    }
});