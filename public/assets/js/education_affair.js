$(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';

    let tableEducationAffairList = $('#tableEducationAffairList').DataTable({
        "dom": "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'l><'col-xs-12 col-sm-4 col-md-4 col-lg-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-xs-12 col-sm-8 col-md-8 col-lg-6'i><'col-xs-12 col-sm-4 col-md-4 col-lg-6'p>>",
        "language": dataTableLanguage(),
        "responsive": false,
        "scrollX": true,
        "fixedHeader": true,
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        "stateSave": true,
        stateSaveCallback: function(settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
        },
        stateLoadCallback: function(settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
        },
        "ajax": {
            url: base_url + "manage_affiliation_schools/data_table_query",
            "type": "GET",
            dataSrc: function(d) {
                let start = d.start;
                $.each(d.data, function(index, item) {
                    item['no'] = parseInt(start) + index + 1;
                });
                return d.data;
            },
            error: function(xhr, error, thrown) {
                //console.log(error)
            }
        },
        "order": [
            [2, "desc"]
        ],
        "lengthMenu": [
            [5, 60, 80, -1],
            [5, 60, 80, 'ทั้งหมด']
        ],
        "columns": [{
            "data": 'no'
        }, {
            "data": "school_affiliation"
        }, {
            "data": "updated_date"
        }, {
            "data": null,
            'defaultContent': ''
        }, ],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": [0],
            "width": "5%",
            "className": 'text-center'
        }, {
            "searchable": true,
            "orderable": true,
            "targets": [1],
            "width": "50%",
            "className": 'text-center'
        }, {
            "targets": [2],
            "searchable": false,
            "orderable": true,
            "width": "30%",
            "className": 'text-center',
            "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss')
        }, {
            "searchable": false,
            "orderable": false,
            "targets": [3],
            "width": "15%",
            "className": 'text-center',
            render: function(data, type, full, meta) {
                if (type === 'display') {
                    data = '<div>' +
                        '<a href="javascript:void(0);" id="' + full.id + '"  class="btn btn-sm btn-default btnEdit" title="Edit" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>' +
                        '&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" id="' + full.id + '" class="btn btn-sm btn-default btnDelete"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a>' +
                        '</div>';
                    return data
                }
            }
        }]
    });
    tableEducationAffairList.buttons('.buttons-html5').remove();

    tableEducationAffairList.on('draw', function() {

        $('.btnEdit').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการแก้ไขข้อมูล", function() {
                window.location = base_url + 'manage_affiliation_schools/edit/' + id;
            }, function() {
                // alert("cancel: " + id);
            });
        });

        $('.btnDelete').on('click', function() {
            let id = $(this).attr("id");
            showConfirmDialog("แจ้งเตือน", "ยืนยันการลบข้อมูล", function() {
                window.location = base_url + 'manage_affiliation_schools/delete/' + id;
            }, function() {
                // alert("cancel: " + id);
            });

        });
    });
});