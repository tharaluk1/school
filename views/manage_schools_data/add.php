<?php
defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php include viewPath('includes/header'); ?>
<link rel="stylesheet" href="<?php echo $url->assets ?>/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        เมนูจัดการข้อมูลโรงเรียน
        <small>
            School Data
        </small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- form start -->
    <form class="form-horizontal form-validate" id="frm_school_data"
          action="<?php echo url('manage_schools_data/insert') ?>" method="post" enctype="multipart/form-data">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">เพิ่ม / แก้ไข</h3>

                <div class="box-tools pull-right">
                    <a href="<?php echo url('manage_schools_data') ?>" class="btn btn-flat btn-default"><i
                                class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_name">
                            ชื่อโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_name" name="school_name" required
                                   placeholder="ชื่อโรงเรียน" type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_affiliation">
                            สังกัดสถานศึกษา
                        </label>
                        <div class="col-sm-8">
                            <select class="form-control select2" id="school_affiliation" name="school_affiliation">
                                <option value="0">-= กรุณาเลือก =-</option>
                                <?php
                                foreach ($school_affiliation_list as $key => $row) {
                                    echo '<option value="' . $row->id . '">' . $row->school_affiliation . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_founded">
                            วัน / เดือน / ปี ก่อตั้ง
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_founded" name="school_founded" placeholder="วัน / เดือน / ปี ก่อตั้ง" type="text"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_address">
                            ที่อยู่
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="7" id="school_address" name="school_address"
                                      placeholder="ที่อยู่"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <label for="school_province">จังหวัด</label>
                                    <select class="form-control select2" id="school_province"
                                            name="school_province">
                                        <option value="0">-= กรุณาเลือก =-</option>
                                        <?php
                                        foreach ($province as $key => $row) {
                                            echo '<option value="' . $row->PROVINCE_ID . '">' . $row->PROVINCE_NAME . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <label for="school_amphur">อำเภอ</label>
                                    <select class="form-control select2" id="school_amphur" name="school_amphur">
                                        <option value="0">-= กรุณาเลือก =-</option>
                                        <?php
                                        foreach ($amphur as $key => $row) {
                                            echo '<option value="' . $row->AMPHUR_ID . '">' . $row->AMPHUR_NAME . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <label for="school_district">ตำบล</label>
                                    <select class="form-control select2" id="school_district"
                                            name="school_district">
                                        <option value="0">-= กรุณาเลือก =-</option>
                                        <?php
                                        foreach ($district as $key => $row) {
                                            echo '<option value="' . $row->DISTRICT_ID . '">' . $row->DISTRICT_NAME . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <label for="school_post_code">รหัสไปรษณีย์</label>
                                    <input class="form-control" id="school_post_code" name="school_post_code"
                                           placeholder="รหัสไปรษณีย์" type="text"/>

                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_tel">
                            เบอร์โทรศัพท์
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="school_tel" name="school_tel"
                                      placeholder="เบอร์โทรศัพท์" rows="7"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_type">
                            ประเภทโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="school_type" id="school_type1" value="ST-001"> โรงเรียนรัฐบาล
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_type" id="school_type2" value="ST-002"> โรงเรียนเอกชน
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_level">
                            ระดับที่เปิดสอน
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_level" name="school_level"
                                   placeholder="ระดับที่เปิดสอน" type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_style">
                            ลักษณะโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style1" value="SS-001"> ชายล้วน
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style2" value="SS-002"> หญิงล้วน
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style3" value="SS-003"> สหศึกษา
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_primary_education">
                            จำนวนนักเรียน ประถมศึกษาตอนต้น
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_primary_education"
                                   name="school_student_primary_education"
                                   placeholder="จำนวนนักเรียน ประถมศึกษาตอนต้น" type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_upperelementary">
                            จำนวนนักเรียน ประถมศึกษาตอนปลาย
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_upperelementary"
                                   name="school_student_upperelementary"
                                   placeholder="จำนวนนักเรียน ประถมศึกษาตอนปลาย" type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_junior_high_school">
                            จำนวนนักเรียน ม. ต้น
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_junior_high_school"
                                   name="school_student_junior_high_school" placeholder="จำนวนนักเรียน ม. ต้น"
                                   type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_high_school">
                            จำนวนนักเรียน ม. ปลาย
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_high_school"
                                   name="school_student_high_school" placeholder="จำนวนนักเรียน ม. ปลาย"
                                   type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_web">
                            เว็บไซต์
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_web" name="school_web" placeholder="เว็บไซต์"
                                   type="text">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_web">
                            ตำแหน่งโรงเรียน
                        </label>
                        <!-- <div class="col-sm-8">
                           <div id="mapid" style="height:250px;"></div>
                        </div> -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Download</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <!--<div class="form-group">
                    <label class="col-sm-3 control-label" for="school_update_date">
                        รายละเอียดข้อมูลอัพเดทวันที่
                    </label>
                    <div class="col-sm-8">
                        <input class="form-control" id="school_update_date" name="school_update_date" placeholder="รายละเอียดข้อมูลอัพเดทวันที่" type="text" value="<?php /*echo date('Y-m-d H:i:s'); */ ?>">
                        </input>
                    </div>
                </div>-->
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_source">
                                แหล่งที่มาของข้อมูล
                            </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_source" name="school_source"
                                       placeholder="แหล่งที่มาของข้อมูล" type="text" value="">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if (count($school_download) > 0) {
                            foreach ($school_download as $key => $v) {
                                echo '
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="school_download_' . $v->id . '">' . $v->title . '</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="school_download[' . $v->id . ']" id="school_download_' . $v->id . '" placeholder="Upload School Download" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf">
                                </div>
                            </div>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Checklist</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <?php
                            $i = 1;
                            foreach ($school_checklist as $key => $v) {
                                $active = '';
                                if ($i == 1) {
                                    $active = 'active';
                                }
                                echo '<li class="' . $active . '"><a href="#tab_' . $v->id . '" data-toggle="tab" aria-expanded="true">' . $v->name . '</a></li>';
                                $i++;
                            }
                            ?>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <?php
                            $i = 1;
                            foreach ($school_checklist as $key => $v) {
                                $active = '';
                                if ($i == 1) {
                                    $active = 'active';
                                }
                                echo '<div class="tab-pane ' . $active . '" id="tab_' . $v->id . '">';
                                ?>
                                <b>How to use:</b>

                                <p>Exactly like the original bootstrap tabs except you should use
                                    the custom wrapper <code>.nav-tabs-custom</code> to achieve this style.</p>
                                A wonderful serenity has taken possession of my entire soul,
                                like these sweet mornings of spring which I enjoy with my whole heart.
                                I am alone, and feel the charm of existence in this spot,
                                which was created for the bliss of souls like mine. I am so happy,
                                my dear friend, so absorbed in the exquisite sense of mere tranquil existence,
                                that I neglect my talents. I should be incapable of drawing a single stroke
                                at the present moment; and yet I feel that I never was a greater artist than now.
                                <?php
                                echo '</div>';
                                $i++;
                            }
                            ?>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Sharing</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <label for="school_sharing_tag">Tag Name</label>
                            <input type="text" class="form-control" id="school_sharing_tag" placeholder="Tag Name">
                        </div>
                        <div class="col-xs-7">
                            <label for="school_sharing_url">URL</label>
                            <input type="text" class="form-control" id="school_sharing_url" placeholder="URL">
                        </div>
                        <div class="col-xs-1">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-primary" id="btn_add_tag">เพิ่ม</button>
                        </div>
                    </div>
                    <div id="form_tag"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- Default box -->
        <div class="box">
            <div class="box-footer">
                <button type="button" id="btn_submit" class="btn btn-flat btn-primary">Save</button>
            </div>
            <!-- /.box-footer-->

        </div>
        <!-- /.box -->
    </form>
</section>
<!-- /.content -->
<script src="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    var txt_form_tag_no = 1;

    $(document).ready(function () {
        $('.form-validate').validate();
        //Initialize Select2 Elements
        $('.select2').select2();
        //Date picker
        $('#school_founded').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true∏
        });

       //loadMap()
    });

    function loadMap(){
        var map = L.map('mapid', {
            center: [51.505, -0.09],
            zoom: 13
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
    }

    $('#btn_add_tag').on('click', function () {
        if ($('input[name="school_sharing_tag[]"]').length < 10) {
            var school_sharing_tag = $('#school_sharing_tag').val();
            var school_sharing_url = $('#school_sharing_url').val();
            var txt_form_tag = '<div id="tab_' + txt_form_tag_no + '"><hr><div class="row"><div class="col-xs-4"><label for="school_sharing_tag">Tag Name</label><input type="text" class="form-control" id="school_sharing_tag" name="school_sharing_tag[]" placeholder="Tag Name" value="' + school_sharing_tag + '"></div><div class="col-xs-7"><label for="school_sharing_url">URL</label><input type="text" class="form-control" id="school_sharing_url" name="school_sharing_url[]" placeholder="URL" value="' + school_sharing_url + '"></div><div class="col-xs-1"><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><button type="button" class="btn btn-danger btn_delete_tag" id="' + txt_form_tag_no + '">ลบ</button></div></div></div>';
            $('#form_tag').append(txt_form_tag);
            $('#school_sharing_tag').val('');
            $('#school_sharing_url').val('');
            txt_form_tag_no++;
        }
    });

    $(document).on("click", ".btn_delete_tag", function () {
        var id = $(this).attr('id');
        $('#tab_' + id).remove();
    });

    $('#btn_submit').on('click', function () {
        $('#frm_school_data').submit();
    });

    $('#school_province').on('change', function () {
        var province = $(this).val();
        if (province != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_amphur_list') ?>",
                type: "post",
                data: {
                    province: province
                },
                success: function (result) {
                    $('#school_amphur').html(result);
                }
            });
        }
    });
    $('#school_amphur').on('change', function () {
        var amphur = $(this).val();
        if (amphur != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_district_list') ?>",
                type: "post",
                data: {
                    amphur: amphur
                },
                success: function (result) {
                    $('#school_district').html(result);
                }
            });
        }
    });
    $('#school_district').on('change', function () {
        var amphur = $('#school_amphur').val();
        if (amphur != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_post_code') ?>",
                type: "post",
                data: {
                    amphur: amphur
                },
                success: function (result) {
                    $('#school_post_code').val(result);
                }
            });
        }
    });
</script>
<?php include viewPath('includes/footer'); ?>