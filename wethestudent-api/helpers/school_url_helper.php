<?php

class SchoolUrl
{
    public static function getSchoolDownloadUrl($school_id)
    {
        $base_url = base_url();
        return str_replace('/api/', '/', $base_url) . "uploads/school_data/{$school_id}/school_download/";
    }

    public static function getSchoolIconDownloadUrl()
    {
        $base_url = base_url();
        return str_replace('/api/', '/', $base_url) . "uploads/school_download/";
    }

    public static function getSchoolAboutUrl()
    {
        $base_url = base_url();
        return str_replace('/api/', '/', $base_url) . "uploads/about_us/";
    }

    public static function getSchoolImageUrl($school_id = "")
    {
        if ($school_id == "") {
            return "/school_image/";
        } else {
            $base_url = base_url();
            return str_replace('/api/', '/', $base_url) . "uploads/school_data/" . $school_id . "/school_image/";
        }
    }

    public static function getSchoolDataUrl()
    {
        $base_url = base_url();
        return str_replace('/api/', '/', $base_url) . "uploads/school_data/";
    }
    // /home/wethestudent-developer/wethestudents-project/wethestudents-backend/public/uploads/school_data/3/school_checklist
    public static function getSchoolCheckListUrl()
    {
        return "school_checklist/";
    }

    public static function getDefaultImageUrl()
    {
        $base_url = base_url();
        return str_replace('/api/', '/', $base_url) . "uploads/no-img.jpg";
    }

    public static function getSchoolCheckListInfographic()
    {
        return "images_standard/";
    }

}
