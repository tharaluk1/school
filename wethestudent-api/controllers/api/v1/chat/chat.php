<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chat extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel("chat_model");
    }

    public function get_chat_post()
    { 
        $name = $this->inputPost("name");
        $email = $this->inputPost("email");
        $mdRoom = md5($name.$email);
        
        $data = $this->chat_model->get_chat_all($mdRoom);
        $this->sendResponse($data);
    }

    public function get_chat_row_post()
    { 
        $name = $this->inputPost("name");
        $email = $this->inputPost("email");
        $take = $this->inputPost("take");

        $mdRoom = md5($name.$email);

        $data = $this->chat_model->get_chat_row($mdRoom,$take);
        $this->sendResponse($data);
    }

    public function send_text_post()
    {
        // $room = $this->input->post('room');
        $name = $this->post('name');
        $email = $this->post('email');
        $text = $this->post('text');
        $date_now = date('Y-m-d H:i:s');
        $mdRoom = md5($name.$email);


        $data = array(
            "room" => $mdRoom,
            "email" => $email,
            "name" => $name,
            "text" => $text,
            "type" => 2,
            "flag" => 0,
            "create_date" => $date_now,
            "update_date" => $date_now
        );

        $result = $this->chat_model->insert_chat_user($data);
        $this->sendResponse($result);
    }

}