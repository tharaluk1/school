<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Rating extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel("Rating_model");
    }

    public function create_post()
    {
        $school_id = $this->inputPost("school_id");
        $school_check_list_id = $this->inputPost("school_check_list_id");
        $school_check_list_item_id = json_decode($this->inputPost("school_check_list_item_id"));
        $school_check_list_item_rating = json_decode($this->inputPost("school_check_list_item_rating"));

        if (is_array($school_check_list_item_id) && is_array($school_check_list_item_rating)) {
            if (count($school_check_list_item_id) == count($school_check_list_item_rating)) {

                foreach ($school_check_list_item_id as $key => $item_id) {
                    $data_rating = array(
                        "school_id" => $school_id,
                        "school_check_list_id" => $school_check_list_id,
                        "school_check_list_item_id" => $item_id,
                        "rating" => $school_check_list_item_rating[$key],
                        "date_update" => $this->getDateNow(),
                        "status" => 1);

                    $this->Rating_model->create($data_rating);

                    $this->Rating_model->calculate_rating($school_id, $school_check_list_id, $item_id);
                }
                $this->sendResponse($this->Rating_model->calculate_rating_check_list_item(
                    $school_id,$school_check_list_id
                ));
            }
        }
        $this->sendResponseError();
    }

    public function description_post()
    {
        $school_id = $this->inputPost('school_id');
        $school_check_list_id = $this->inputPost('school_check_list_id');

        $this->sendResponse($this->Rating_model->calculate_rating_check_list_item(
            $school_id,$school_check_list_id
        ));
    }
}
