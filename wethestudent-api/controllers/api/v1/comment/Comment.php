<?php

class Comment extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel("Comment_model");
    }

    public function list_post()
    {
        $school_id = $this->inputPost("school_id");
        $check_list_id = $this->inputPost("check_list_id");
        $offset = $this->createOffsetWithPageNo();
        $page = $this->inputPost("page");
        $data = $this->Comment_model->getCommentList($school_id,
            $check_list_id, $page, $this->limit_per_page, $offset);

        $this->sendResponse($data);
    }

    public function create_post()
    {
        $school_id = $this->inputPost("school_id");
        $check_list_id = $this->inputPost("check_list_id");
        $comment_name = $this->inputPost("comment_name");
        $comment_message = $this->inputPost("comment_message");
        $date_now = $this->getDateNow();
        $data = array(
            "school_id" => $school_id,
            "check_list_id" => $check_list_id,
            "comment_name" => $comment_name,
            "comment_message" => $comment_message,
            "create_date" => $date_now,
            "update_date" => $date_now,
            "update_by" => null,
            "status" => 1,
        );

        $response_data = array(
            "total_comment" => 0,
            "list" => array(),
        );
        $comment_id = $this->Comment_model->create($data);
        if ($comment_id) {
            $data['comment_id'] = $comment_id;
            $data["create_at"] = $data['create_date'];
            unset($data['status']);
            unset($data['update_by']);
            unset($data['create_date']);
            unset($data['update_date']);

            $response_data['list'] = $data;
            $response_data['total_comment'] = $this->Comment_model->count_comment($school_id, $check_list_id);
            $this->sendResponseSuccess($response_data);
        } else {
            $this->sendResponseError();
        }
    }
}
