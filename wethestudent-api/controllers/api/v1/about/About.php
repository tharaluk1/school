<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel("About_model");
    }

    function index_post()
    {
        $data = $this->About_model->getAbout();
        $this->sendResponse($data);
    }
}