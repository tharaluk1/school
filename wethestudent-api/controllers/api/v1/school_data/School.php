<?php
defined('BASEPATH') or exit('No direct script access allowed');

class School extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loadModel("Schools_model");
    }

    public function education_affair_list_post()
    {
        $data = $this->Schools_model->getSchoolAffiliationList(
            "id as id,school_affiliation as name,updated_date as update_at",
            "school_affiliation asc"
        );
        $this->sendResponse($data);
    }

    public function list_post()
    {
        $education_affair_id = $this->inputPost("education_affair_id");
        $keyword = $this->inputPost("keyword");
        $sortBy = $this->inputPost("sort_by");
        $lat = $this->inputPost("lat");
        $lng = $this->inputPost("lng");
        $page = $this->inputPost('page');
        $offset = $this->createOffsetWithPageNo();
        $limit = $this->limit_per_page;

        $data = $this->Schools_model->getSchoolData($this->str_array_to_array($education_affair_id),
            $keyword, $sortBy, $lat, $lng, $limit, $offset, $page);

        $statusCode = ResponseCode::HTTP_NOT_FOUND;
        if (sizeof($data['school_list']) > 0) {
            $statusCode = ResponseCode::HTTP_OK;
        }

        $this->sendResponseCode($statusCode, $data);
    }

    public function description_post()
    {
        $school_id = $this->inputPost("school_id");
        if (strlen($school_id) == 0) {
            $this->sendResponseError();
        }

        $data = $this->Schools_model->getSchoolDescription($school_id);
        $this->sendResponse($data);
    }

    public function check_list_master_post()
    {
        $data = $this->Schools_model->getSchoolCheckListMaster();
        $this->sendResponse($data);
    }

    public function Upload_Img_Font_post()
    {

        $email = $this->inputPost("email");
        $name = $this->inputPost("name");
        $tel = $this->inputPost("tel");

        $school_id = $this->inputPost("school_id");
        $school_checklist_id = $this->inputPost("school_checklist_id");

        $check = $this->input->post("images");
        $check1 = $this->input->post("images1");
        $check2 = $this->input->post("images2");
        $check3 = $this->input->post("images3");
        $check4 = $this->input->post("images4");

        if($check != ''){
            $img = explode(",",$check);
        }
        if($check1 != ''){
            $img1 = explode(",",$check1);
        }
        if($check2 != ''){
            $img2 = explode(",",$check2);
        }
        if($check3 != ''){
            $img3 = explode(",",$check3);
        }
        if($check4 != ''){
            $img4 = explode(",",$check4);
        }

        $images = $check != "" ? base64_decode($img[1]) : "";
        $images1 = $check1 != "" ?  base64_decode($img1[1]) : "";
        $images2 = $check2 != "" ?  base64_decode($img2[1]) : "";
        $images3 = $check3 != "" ?  base64_decode($img3[1]) : "";
        $images4 = $check4 != "" ?  base64_decode($img4[1]) : "";

        if($images != ""){
            $Message1 = $this->api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images);
        }
        if($images1 != ""){
            $Message = $this->api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images1);
        }
        if($images2 != ""){
            $Message = $this->api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images2);
        }
        if($images3 != ""){
            $Message = $this->api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images3);
        }
        if($images4 != ""){
            $Message = $this->api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images4);
        }

        $this->sendResponse("Sucess");           

    }

    public function api_upload_img($email,$name,$school_id,$school_checklist_id,$tel,$images)
    {
        $image_name = md5(uniqid(rand(), true));
            $filename = $image_name . '.' . 'png';
            $path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/school_data/' . $school_id . '/school_checklist/';

            if (!is_dir($path)) {
                mkdir($path, 0777, TRUE);
            }

            file_put_contents($path . $filename, $images);

            $date_now = date('Y-m-d H:i:s');
            $data_insert = array(
                'school_id' => $school_id,
                'school_checklist_id' => $school_checklist_id,
                'path' => $filename,
                'created_date' => $date_now,
                'created_by' => 0,
                'updated_date' => $date_now,
                'updated_by' => 0,
                'status' => 1,
            );

            $success_id_checklist = $this->schools_model->InsertImg_New($data_insert);

            if($success_id_checklist != null){
                $data_insert_cus = array(
                    'id_school_checklist' => $success_id_checklist,
                    'name' => $name,
                    'email' => $email,
                    'tel' => $tel,
                    'status_confirm' => 0,
                );
                $success_cus = $this->schools_model->Insert_school_data_checklist_cus($data_insert_cus);
                if($success_cus){
                    $Message = "Img Registered Successfully..";
                }else{
                    $Message = "checklist_cus Fail";
                }         
            }
            else
            {
                $Message = "Upload Img Fail";
            }

            return $Message;
    }

}
