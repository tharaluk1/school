<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Mobility2u_Controller extends RestController
{

    public $limit_per_page = 5;

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function loadModel($model)
    {
        $this->load->model($model);
    }

    public function sendResponseCode($status_code, $data)
    {
        $res = array(
            "statusCode" => $status_code,
            "message" => $status_code == ResponseCode::HTTP_NOT_FOUND ? "ไม่พบข้อมูล" : "สำเร็จ",
            "data" => $data,
        );
        $this->response($res, 200); // Send an HTTP 200 success
    }

    public function sendResponseError()
    {
        $res = array(
            "statusCode" => ResponseCode::HTTP_ERROR,
            "message" => "ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง",
            "data" => null,
        );

        $this->response($res, 200); // Send an HTTP 200 success
    }

    public function sendResponseErrorMessage($message = "")
    {
        $res = array(
            "statusCode" => ResponseCode::HTTP_ERROR,
            "message" => $message,
            "data" => array(),
        );

        $this->response($res, ResponseCode::HTTP_OK); // Send an HTTP 200 success
    }

    public function sendResponseSuccess($data = array())
    {
        $res = array(
            "statusCode" => ResponseCode::HTTP_OK,
            "message" => "Success",
            "data" => $data,
        );

        $this->response($res, ResponseCode::HTTP_OK); // Send an HTTP 200 success
    }

    public function sendResponse($data = array())
    {
        if (!$data) {
            $data = array();
        }
        $size = 1;
        if (is_array($data)) {
            $size = sizeof($data);
        }

        $res = array(
            "statusCode" => $size == 0 ? ResponseCode::HTTP_NOT_FOUND : ResponseCode::HTTP_OK,
            "message" => $size == 0 ? "ไม่พบข้อมูล" : "สำเร็จ",
            "data" => $data,
        );

        $this->response($res, ResponseCode::HTTP_OK); // Send an HTTP 200 success
    }

    public function inputPost($parameter = "")
    {
        try {
            return $this->input->post($parameter);
        } catch (exception $e) {
            return "";
        }
    }

    public function inputGet($parameter = "")
    {
        try {
            return $this->input->get($parameter);
        } catch (exception $e) {
            return "";
        }
    }

    public function calculateLocationDistance($lat1, $lon1, $lat2, $lon2, $unit = "K")
    {
        return $this->locationutil->calculateDistance($lat1, $lon1, $lat2, $lon2, $unit);
    }

    public function createOffsetWithPageNo($limit = -1)
    {
        $page_no = $this->inputPost("page");
        if ($page_no < 1) {
            return -1;
        }

        $this->limit_per_page = $limit == -1 ? $this->limit_per_page : $limit;
        return ($page_no - 1) * $this->limit_per_page;
    }

    public function str_array_to_array($str_array)
    {
        try {
            $json = json_decode($str_array, false);
            if (is_array($json)) {
                return $json;
            }

        } catch (Exception $e) {

        }
        return array();
    }

    public function getDateNow($format = "Y-m-d H:i:s")
    {
        return date($format);
    }

    public function public_url()
    {
        return str_replace(APP_API . '/', '', base_url());
    }

}
