<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mobility2u_Model extends CI_Model
{

    /**
     * Default Table Primary Key
     */
    public $table_key = 'id';

    /**
     * Get Data from table
     *
     * @return array Data
     */
    public function get()
    {
        return $this->db->get($this->table)->result();
    }

    /**
     * Get Data from table by id
     *
     * @return object Data Ex. {}
     */
    public function getById($id)
    {
        return $this->db->get_where($this->table, [$this->table_key => $id])->row();
    }

    /**
     * Get a particular field/row from table by its primary key eg. id
     *
     * @param int $id Primary Key Example - id
     * @param string $row coloumn name Example - name
     *
     * @return string
     *
     */

    public function getRowById($id, $row)
    {
        return $this->db->get_where($this->table, [$this->table_key => $id])->row()->{$row};
    }

    /**
     * Create/Insert the row in Table
     *
     * @param array $data
     *
     * @return int Inserted Id
     */
    public function create($data)
    {

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    /**
     * Create/Insert the multiple rows in Table
     *
     * @param array $data
     *
     * @return int Inserted Id
     */
    public function create_batch($data)
    {

        $this->db->insert_batch($this->table, $data);
        return $this->db->insert_id();

    }

    /**
     * Update the row in Table by id
     *
     * @param array $data
     *
     * @return int Updated Id
     */
    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    /**
     * Delete the row in Table by id
     *
     * @param int $id
     *
     * @return boolean true
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return true;
    }

    /**
     * Get Data Using Where condition from Table
     * Quick Function to extract information from table
     *
     * @param array $whereArg
     * @param array $args Other conditions like order
     *
     * @return boolean true
     */
    public function getByWhere($whereArg, $args = [])
    {

        if (isset($args['order'])) {
            $this->db->order_by($args['order'][0], $args['order'][1]);
        }

        return $this->db->get_where($this->table, $whereArg)->result();
    }

    /**
     * Predict Id of table using simple algo
     *
     * @return int
     */
    public function predictId()
    {
        $this->db->order_by($this->table_key, 'desc');
        return ($query = $this->db->get_where($this->table)) && $query->num_rows() > 0 ? $query->row()->id + 1 : 1;
    }

    public function getDateNow($format = "Y-m-d H:i:s")
    {
        return date($format);
    }
    /**
     * Return the total number of rows in the table
     *
     * @return int
     */
    public function countAll()
    {
        return $this->db->count_all_results($this->table);
    }

    public function formulaDistance($lat, $lng, $unit = "K")
    {
        return $this->locationutil->formulaDistance($lat, $lng, $unit);
    }

    public function _school_affiliation()
    {
        return 'school_affiliation';
    }

    public function _school()
    {
        return 'school';
    }

    public function _province()
    {
        return 'province';
    }

    public function _amphur()
    {
        return 'amphur';
    }

    public function _district()
    {
        return 'district';
    }

    public function _school_data_download()
    {
        return 'school_data_download';
    }

    public function _school_download()
    {
        return 'school_download';
    }

    public function _about_us()
    {
        return 'about_us';
    }

    public function _school_sharing()
    {
        return 'school_sharing';
    }

    public function _school_comment()
    {
        return 'school_comment';
    }

    public function _school_check_list_rating_logs()
    {
        return 'school_check_list_rating_logs';
    }

    public function _school_check_list_rating()
    {
        return 'school_check_list_rating';
    }
    public function _school_checklist_items()
    {
        return 'school_checklist_items';
    }

    public function _school_checklist_items_master()
    {
        return 'school_checklist_items_master';
    }
    public function _school_type()
    {
        return 'school_type';
    }

    public function _school_style()
    {
        return 'school_style';
    }

    public function _school_checklist_master()
    {
        return 'school_checklist_master';
    }

    public function _school_data_checklist()
    {
        return "school_data_checklist";
    }
    
    public function _school_data_standard()
    {
        return "school_data_standard";
    }

    public function public_url()
    {
        return str_replace(APP_API . '/', '', base_url());
    }
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
