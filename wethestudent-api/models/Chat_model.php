<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chat_model extends Mobility2u_Model
{
    public function get_chat_all($room)
    {
        $this->db->where('room', $room);
        $query = $this->db->get('chat');
        // print_r($this->db->last_query());exit;
        return $query->result();
    }
    
    public function get_chat_row($room,$take)
    {
        $this->db->where('room', $room);
        $this->db->order_by('id', 'desc');
        $this->db->limit($take,0);
        $query = $this->db->get('chat');
        // print_r($this->db->last_query());exit;
        return $query->result();
    }


    public function insert_chat_user($data)
    {
        return $this->db->insert('chat', $data);
    }

}