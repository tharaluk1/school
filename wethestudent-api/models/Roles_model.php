<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_model extends Mobility2u_Model {

	public $table = 'roles';

	public function __construct()
	{
		parent::__construct();
	}

}

/* End of file Roles_model.php */
/* Location: ./application/models/Roles_model.php */