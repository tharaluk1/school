<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comment_model extends Mobility2u_Model
{
    public function getCommentList($school_id,
        $check_list_id, $page, $limit, $offset) {
        $this->db->select("sc.id as comment_id,sc.school_id,
        sc.check_list_id,
        sc.comment_name,
        sc.comment_message,
        sc.create_date as create_at");

        $where = "sc.school_id = '{$school_id}' AND sc.check_list_id = '{$check_list_id}'";

        $this->db->from($this->_school_comment() . ' as sc');
        $this->db->where($where);

        if ($offset > -1) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by("sc.create_date","DESC");
        
        $query = $this->db->get();

        $response_data = array(
            "total_comment" => $this->count_comment($school_id, $check_list_id),
            "list" => $query->result(),
        );

        return $response_data;
    }

    public function create($data = array())
    {
        $this->db->insert($this->_school_comment(), $data);
        return $this->db->insert_id();
    }

    public function count_comment($school_id, $check_list_id)
    {
        $where = "sc.school_id = '{$school_id}' AND sc.check_list_id = '{$check_list_id}'";
        $this->db->where($where);
        $this->db->from($this->_school_comment() . ' as sc');
        return $this->db->count_all_results();
    }
}
