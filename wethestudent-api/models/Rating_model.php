<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rating_model extends Mobility2u_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create($data)
    {
        return $this->db->insert($this->_school_check_list_rating_logs(), $data);
    }

    public function calculate_rating($school_id, $school_check_list_id, $school_check_list_item_id)
    {
        $rating_data = $this->select_rating($school_id, $school_check_list_id, $school_check_list_item_id);
        $sum_rating = $this->sum_rating_logs($school_id, $school_check_list_id, $school_check_list_item_id);
        $count_rating = $this->count_rating_logs($school_id, $school_check_list_id, $school_check_list_item_id);

        $data = array(
            "school_id" => $school_id,
            "check_list_id" => $school_check_list_id,
            "check_list_item_id" => $school_check_list_item_id,
            "check_list_item_sum_rating" => $sum_rating,
            "check_list_item_total_rating" => $count_rating,
            "check_list_item_avg_rating" => $sum_rating / $count_rating,
            "date_update" => $this->getDateNow(),
            "status" => 1,
        );

        if ($rating_data != null) {
            unset($data['school_id']);
            unset($data['check_list_id']);
            unset($data['check_list_item_id']);
            $this->update_rating($data, array(
                "school_id" => $school_id,
                "check_list_id" => $school_check_list_id,
                "check_list_item_id" => $school_check_list_item_id,
            ));
        } else {
            $this->insert_rating($data);
        }

        $rating_data = $this->select_rating($school_id, $school_check_list_id, $school_check_list_item_id);

        return $rating_data;
    }

    public function insert_rating($data)
    {
        return $this->db->insert($this->_school_check_list_rating(), $data);
    }

    public function update_rating($data, $where = array())
    {
        return $this->db->update($this->_school_check_list_rating(), $data, $where);
    }

    public function select_rating($school_id, $school_check_list_id, $school_check_list_item_id)
    {
        $this->db->select('sclr.school_id,sclr.check_list_id,sclr.check_list_item_id,
        sclr.check_list_item_sum_rating,sclr.check_list_item_total_rating,
        sclr.check_list_item_avg_rating');
        $this->db->where("sclr.school_id", $school_id);
        $this->db->where("sclr.check_list_id", $school_check_list_id);
        $this->db->where("sclr.check_list_item_id", $school_check_list_item_id);
        $this->db->where("sclr.status", 1);
        $query = $this->db->get($this->_school_check_list_rating() . ' as sclr');
        return $query->row();
    }

    public function sum_rating_logs($school_id, $school_check_list_id, $school_check_list_item_id)
    {
        $this->db->select_sum('rating');
        $this->db->where("school_id", $school_id);
        $this->db->where("school_check_list_id", $school_check_list_id);
        $this->db->where("school_check_list_item_id", $school_check_list_item_id);
        $this->db->where("status", 1);
        return $this->db->get($this->_school_check_list_rating_logs())->row()->rating;
    }

    public function count_rating_logs($school_id, $school_check_list_id, $school_check_list_item_id)
    {
        $this->db->where("school_id", $school_id);
        $this->db->where("school_check_list_id", $school_check_list_id);
        $this->db->where("school_check_list_item_id", $school_check_list_item_id);
        $this->db->where("status", 1);
        $this->db->from($this->_school_check_list_rating_logs());
        return $this->db->count_all_results();
    }

    public function calculate_rating_check_list_item($school_id, $school_check_list_id)
    {
        $max_rating = 5;
        $response_data = array(
            "total_rating_review" => 0,
            "total_rating" => array(
                "current" => 0,
                "max" => $max_rating,
                "icon" => $this->get_rating_icon(0)
            ),
            "rating_item" => array(

            ),
        );
        $this->db->select('sclr.school_id,sclr.check_list_id,sclr.check_list_item_id,
        sclr.check_list_item_sum_rating,sclr.check_list_item_total_rating,
        sclr.check_list_item_avg_rating,scim.name as label,scim.name_eng as label_object');
        $this->db->join($this->_school_checklist_items_master() . ' as scim', 'scim.id = sclr.check_list_item_id');
        $this->db->where("sclr.school_id", $school_id);
        $this->db->where("sclr.check_list_id", $school_check_list_id);
        $this->db->where("sclr.status", 1);
        $this->db->order_by("scim.id ASC");
        $query = $this->db->get($this->_school_check_list_rating() . ' as sclr');

        if (count($query->result()) == 0) {
            $this->db->select('scim.name as label,scim.name_eng as label_object');
            $this->db->order_by("scim.id ASC");
            $query = $this->db->get($this->_school_checklist_items_master() . ' as scim');
            foreach ($query->result() as $item) {
                $response_data['rating_item'][$item->label_object]['label'] = $item->label;
                $response_data['rating_item'][$item->label_object]['current'] = 0;
                $response_data['rating_item'][$item->label_object]['max'] = $max_rating;
                $response_data['rating_item'][$item->label_object]['icon'] = $this->get_rating_icon(0);
            }
            return $response_data;
        }
        $total_rating_review = 0;
        $total_rating = 0;

        foreach ($query->result() as $item) {
            $total_rating_review += $item->check_list_item_total_rating;
            $rating = round(doubleval($item->check_list_item_avg_rating), 1, PHP_ROUND_HALF_UP);
            $total_rating += $rating;
            $response_data['rating_item'][$item->label_object]['label'] = $item->label;
            $response_data['rating_item'][$item->label_object]['current'] = $rating;
            $response_data['rating_item'][$item->label_object]['max'] = $max_rating;
            $response_data['rating_item'][$item->label_object]['icon'] = $this->get_rating_icon(doubleval($item->check_list_item_avg_rating));
        }
        $response_data['total_rating_review'] = $total_rating_review / count($query->result());
        $response_data['total_rating']['current'] = round(doubleval($total_rating / count($query->result())), 1, PHP_ROUND_HALF_UP);
        $response_data['total_rating']['icon'] = $this->get_rating_icon($response_data['total_rating']['current']);
        return $response_data;
    }

    public function get_rating_icon($value)
    {
//         0.1 - 1 = 1
        // 1.1 - 2 = 2
        // 2.1 - 3 = 3
        // 3.1 - 4 = 4
        // 4.1 - 5 = 5
        if ($value > 4.0) {
            return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_5.png';
        }
        if ($value > 3.0) {
            return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_4.png';
        }
        if ($value > 2.0) {
            return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_3.png';
        }
        if ($value > 1.0) {
            return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_2.png';
        }
        if ($value > 0.0) {
            return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_1.png';
        }
        return $this->public_url() . getenv('RATING_ICON_PATH') . 'rating_0.png';
    }
}
