<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Schools_model extends Mobility2u_Model
{
    public function getProvince()
    {
        $this->db->order_by('PROVINCE_NAME', 'asc');
        $query = $this->db->get('province');
        return $query->result();
    }

    public function getAmphur($province_id)
    {
        $this->db->where('PROVINCE_ID', $province_id);
        $this->db->order_by('AMPHUR_NAME', 'asc');
        $query = $this->db->get('amphur');
        return $query->result();
    }

    public function getDistrict($amphur_id)
    {
        $this->db->where('AMPHUR_ID', $amphur_id);
        $this->db->order_by('DISTRICT_NAME', 'asc');
        $query = $this->db->get('district');
        return $query->result();
    }

    public function getPostCode($amphur_id)
    {
        $this->db->where('AMPHUR_ID', $amphur_id);
        $query = $this->db->get('amphur');
        return $query->row();
    }

    public function getSchoolAffiliationList($select = "*", $order_by = null)
    {
        $this->db->select($select);
        $this->db->where('status', 1);
        if ($order_by != null) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get('school_affiliation');
        return $query->result();
    }

    public function getSchoolAffiliationByID($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get('school_affiliation');
        return $query->row();
    }

    public function getSchool($select = "*", $where = null)
    {
        $this->db->select($select);

        if ($where != null) {
            $this->db->where($where);
        }

        $query = $this->db->get($this->_school());
        return $query->result();
    }

    public function getSchoolData($education_affair_id,
        $keyword,
        $sortBy,
        $lat,
        $lng,
        $limit,
        $offset, $page) {
        $this->db->start_cache();

        $where = "1=1  AND s.status = '1'";
        if (strlen($keyword) > 0) {
            $where .= " AND (s.name LIKE '%{$keyword}%' OR s.address LIKE '%{$keyword}%'
            OR p.PROVINCE_NAME LIKE '%{$keyword}%' OR a.AMPHUR_NAME LIKE '%{$keyword}%'
            OR d.DISTRICT_NAME LIKE '%{$keyword}%')";
        }
        if (sizeof($education_affair_id) > 0) {
            $this->db->where_in('sa.id', $education_affair_id);
        }
        $select = "s.id as school_id,s.name,s.affiliation as education_affair_id,sa.school_affiliation as education_affair,
        s.address,p.PROVINCE_NAME as province,a.AMPHUR_NAME as amphur,d.DISTRICT_NAME as district,s.post_code,s.tel,
        s.web as website,s.lat,s.lng,s.update_date as update_at,IF(s.school_image='','" . SchoolUrl::getDefaultImageUrl() . "',CONCAT('" . SchoolUrl::getSchoolDataUrl() . "',s.id,
        '" . SchoolUrl::getSchoolImageUrl("") . "',s.school_image))  as school_image";

        if (strlen($lat) == 0) {
            $lat = "0";
        }

        if (strlen($lng) == 0) {
            $lng = "0";
        }

        $select .= "," . $this->formulaDistance($lat, $lng) . ' as distance_km';

        switch ($sortBy) {
            case "current_location":
                //$this->db->order_by("");
                $this->db->order_by("distance_km ASC");
                break;
            case "date_update":
                $this->db->order_by("s.update_date DESC");
                break;
            case "alphabet":
            default:
                $this->db->order_by("s.name ASC");
                break;
        }
        $this->db->select($select);
        $this->db->from($this->_school() . " as s");
        $this->db->join($this->_school_affiliation() . ' as sa', 'sa.id = s.affiliation');
        $this->db->join($this->_province() . ' as p', 'p.PROVINCE_ID = s.province');
        $this->db->join($this->_amphur() . ' as a', 'a.AMPHUR_ID = s.amphur');
        $this->db->join($this->_district() . ' as d', 'd.DISTRICT_ID = s.district');
        $this->db->where($where);

        $this->db->stop_cache();

        $query_total = $this->db->count_all_results();

        if ($offset > -1) {
            $this->db->limit($limit, $offset);
        }

        $query = $this->db->get();

        $this->db->flush_cache();

        $response = $query->result();

        return array(
            "school_list" => $response,
            "total_list" => $query_total,
            "limit_per_page" => intval($limit),
            "current_page" => intval($page),
            "total_page" => intval(ceil($query_total / $limit)));
    }

    public function getSchoolDataById($school_id)
    {
        $lat = "";
        $lng = "";
        $this->db->start_cache();

        $where = "s.id = '{$school_id}'";

        $select = "s.id as school_id,s.name,s.affiliation as education_affair_id,sa.school_affiliation as education_affair,
        s.address,p.PROVINCE_NAME as province,a.AMPHUR_NAME as amphur,d.DISTRICT_NAME as district,s.post_code,s.tel,
        s.founded,s.level as school_level,s.primary_education,s.student_upperelementary,s.junior_high_school,s.high_school,
        st.name as school_type,ss.name as school_style,s.web as website,s.lat,s.lng,s.update_date as update_at";
// s.level,s.primary_education,s.student_upperelementary,s.junior_high_school,
        //s.high_school,
        if (strlen($lat) == 0) {
            $lat = "0";
        }

        if (strlen($lng) == 0) {
            $lng = "0";
        }

        $select .= "," . $this->formulaDistance($lat, $lng) . ' as distance_km';

        $this->db->select($select);
        $this->db->from($this->_school() . " as s");
        $this->db->join($this->_school_affiliation() . ' as sa', 'sa.id = s.affiliation');
        $this->db->join($this->_province() . ' as p', 'p.PROVINCE_ID = s.province');
        $this->db->join($this->_amphur() . ' as a', 'a.AMPHUR_ID = s.amphur');
        $this->db->join($this->_district() . ' as d', 'd.DISTRICT_ID = s.district');
        $this->db->join($this->_school_type() . ' as st', 'st.code = s.school_type');
        $this->db->join($this->_school_style() . ' as ss', 'ss.code = s.school_style');
        $this->db->where($where);

        $this->db->stop_cache();

        $query = $this->db->get();

        $this->db->flush_cache();

        $response = $query->row();

        $response->student_data = array(
            "total" => $response->primary_education + $response->student_upperelementary + $response->junior_high_school + $response->high_school,
            "kindergarten" => $response->primary_education,
            "elementary_school" => $response->student_upperelementary,
            "junior_high_school" => $response->junior_high_school,
            "senior_high_school" => $response->high_school,
        );

        unset($response->primary_education);
        unset($response->student_upperelementary);
        unset($response->junior_high_school);
        unset($response->high_school);
        return $response;
    }

    public function getSchoolDescription($school_id)
    {
        $response_data = array(
            "school_data" => $this->getSchoolDataById($school_id),
            "school_download" => array(
                "update_at" => $this->getMaxDateSchoolDownload($school_id)->update_at,
                "data_source_by" => $this->getSchoolDownloadDataSourceBy($school_id)->data_source_by,
                "list" => $this->getSchoolDownload($school_id),
            ),
            "school_check_list" => $this->getSchoolCheckListBySchoolId($school_id),
            "school_sharing" => $this->getSchoolSharing($school_id),
        );

        return array($response_data);
    }

    public function getSchoolDownload($school_id = null)
    {
        $school_download_url = SchoolUrl::getSchoolDownloadUrl($school_id);

        $school_icon_download_url = SchoolUrl::getSchoolIconDownloadUrl();

        $this->db->select("sd.title,CONCAT('" . $school_icon_download_url . "',sd.icon) as icon_url,CONCAT('" . $school_download_url . "',sdd.path) as file_url");
        if ($school_id != null) {
            $this->db->where("sdd.school_id = '{$school_id}'");
        }

        $this->db->join($this->_school_download() . ' as sd', 'sd.id = sdd.school_download_id');
        $query = $this->db->get($this->_school_data_download() . ' as sdd');

        return $query->result();
    }

    public function getMaxDateSchoolDownload($school_id)
    {
        $this->db->select_max('updated_date', "update_at");
        $this->db->where("school_id", $school_id);
        $query = $this->db->get($this->_school_data_download());
        return $query->row();
    }

    public function getSchoolDownloadDataSourceBy($school_id)
    {
        $this->db->select('source as data_source_by');
        $this->db->where('id', $school_id);
        $query = $this->db->get($this->_school());
        return $query->row();
    }

    public function getSchoolSharing($school_id)
    {
        $this->db->select('tag_name as tag_name,url as tag_url,created_date as created_at');
        $this->db->where('school_id', $school_id);
        $query = $this->db->get($this->_school_sharing());
        return $query->result();
    }

    public function getSchoolCheckListMaster()
    {
        $query = $this->db->get($this->_school_checklist_master());
        return $query->result();
    }

    public function getSchoolCheckListBySchoolId($school_id)
    {
        $this->db->select();
        // $this->db->where('school_id', $school_id);
        $query = $this->db->get($this->_school_checklist_master());

        $school_check_list = array(
            "check_list" => array(),
            "check_list_item" => array(),
        );
        $check_list = array();

        foreach ($query->result() as $row) {
            $check_list[$row->name_eng] = array(
                "check_list_id" => $row->id,
                "label" => $row->name,
                "update_at" => $row->updated_date,
                "image_list" => $this->getCheckListImageBySchoolCheckList($school_id, $row->id),
                "image_infographic" => $this->getSchoolCheckListInfographic($school_id, $row->id),
            );
        }
        $school_check_list["check_list"] = $check_list;

        $check_list_item = array();
        foreach ($this->getSchoolCheckListItem($school_id) as $item) {
            $check_list_item[$item->name_eng] = array(
                "check_list_item_id" => $item->check_list_item_id,
                "label" => $item->label);
        }
        $school_check_list["check_list_item"] = $check_list_item;

        return $school_check_list;

        // array(
        //     "check_list" => array(
        //         "classroom" => array(
        //             "check_list_id" => "1",
        //             "label" => "ห้องเรียน",
        //             "update_at" => "2020-02-14",
        //             "image_list" => array(array(
        //                 "image_url" => "image_url",
        //             )),
        //             "image_infographic" => "url",
        //         ),
        //         "toilet" => array(
        //             "check_list_id" => "2",
        //             "label" => "ห้องน้ำ",
        //             "update_at" => "2020-02-14",
        //             "image_list" => array(),
        //             "image_infographic" => "url",
        //         ),
        //         "cafeteria" => array(
        //             "check_list_id" => "3",
        //             "label" => "โรงอาหาร",
        //             "update_at" => "2020-02-14",
        //             "image_list" => array(),
        //             "image_infographic" => "url",
        //         ),
        //         "sport_field" => array(
        //             "check_list_id" => "4",
        //             "label" => "ลานกีฬา",
        //             "update_at" => "2020-02-14",
        //             "image_list" => array(),
        //             "image_infographic" => "url",
        //         )),
        //     "check_list_item" => array(
        //         "cleanliness" => array(
        //             "check_list_item_id" => "1",
        //             "label" => "ความสะอาด",
        //         ),
        //         "sufficiency" => array(
        //             "check_list_item_id" => "2",
        //             "label" => "ความพอเพียง",
        //         ),
        //         "safety" => array(
        //             "check_list_item_id" => "3",
        //             "label" => "ความปลอดภัย",
        //         ),
        //         "beautiful" => array(
        //             "check_list_item_id" => "4",
        //             "label" => "สวยงามน่าใช้",
        //         ),
        //     ),
        // );
    }

    public function getCheckListImageBySchoolCheckList($school_id, $school_check_list_id)
    {
        $this->db->select("IF(sdc.path='','" . SchoolUrl::getDefaultImageUrl() . "',CONCAT('" . SchoolUrl::getSchoolDataUrl() . "',sdc.school_id,'/','" . SchoolUrl::getSchoolCheckListUrl() . "',sdc.path)) as image_url");
        $this->db->where(array(
            "sdc.school_id" => $school_id,
            "sdc.school_checklist_id" => $school_check_list_id,
        ));
        $this->db->where("status != 1");

        $query = $this->db->get($this->_school_data_checklist() . ' as sdc');
        //print_r($this->db->last_query());exit;
        return $query->result();
    }

    public function getSchoolCheckListInfographic($school_id, $school_check_list_id)
    {
        $this->db->select("scs.path as image_infographic");
        $this->db->where(array(
            "scs.school_id" => $school_id,
            "scs.school_checklist_id" => $school_check_list_id,
        ));
        $this->db->from($this->_school_data_standard() . ' as scs');
        $query = $this->db->get();
        if (sizeof($query->result()) > 0) {
            return SchoolUrl::getSchoolDataUrl() . $school_id . '/' . SchoolUrl::getSchoolCheckListInfographic() . $query->row()->image_infographic;
        }
        return "";
    }
    public function getSchoolCheckListItem($school_id)
    {
        $this->db->select("scim.id as check_list_item_id,scim.name as label,scim.name_eng");
        $this->db->join($this->_school_checklist_items_master() . ' as scim', "scim.id = sci.school_checklist_item_id");
        $this->db->where(array(
            "sci.school_id" => $school_id,
        ));
        $this->db->from($this->_school_checklist_items() . ' as sci');
        $query = $this->db->get();
        return $query->result();
    }

    public function InsertImg_New($data_insert)
    {
        $this->db->insert('school_data_checklist', $data_insert);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function Insert_school_data_checklist_cus($data_insert_cus)
    {
        return $this->db->insert('school_data_checklist_cus', $data_insert_cus);
    }
}
