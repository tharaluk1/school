<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About_model extends Mobility2u_Model
{

    public function getAbout()
    {
        $this->db->select("title,description,IF(image = '', '', CONCAT('" . SchoolUrl::getSchoolAboutUrl() . "',image))  as image_url,line_id,updated_date as updated_at");
        $this->db->from($this->_about_us());
        $this->db->limit(1, 0);
        $query = $this->db->get();

        $item = $query->row();
        if (isset($item->line_id)) {
            if (substr($item->line_id, 0, 4) == "http" || substr($item->line_id, 0, 4) == "https") {

            } else {
                $item->line_id = "https://" . $item->line_id;
            }
        }
        return $item;
    }
}
