<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_affiliation_schools extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'จัดการข้อมูลสังกัดสถานศึกษา';
        $this->page_data['page']->menu = 'manage_affiliation_schools';
    }

    public function index()
    {
//        $this->page_data['data'] = $this->schools_model->getSchoolAffiliationList();
        $this->loadView('manage_affiliation_schools/list', $this->page_data);
    }

    public function add()
    {
        $this->loadView('manage_affiliation_schools/add', $this->page_data);
    }

    public function edit($id)
    {
        if (empty($id)) {
            redirect('manage_affiliation_schools', 'refresh');
        }
        $this->page_data['id'] = $id;
        $this->page_data['data'] = $this->schools_model->getSchoolAffiliationByID($id);
        $this->loadView('manage_affiliation_schools/edit', $this->page_data);
    }

    public function delete($id)
    {
        if (empty($id)) {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        } else {
            $date_now = $this->getDateNow();
            $data = array(
                'updated_date' => $date_now,
                'updated_by' => logged('id'),
                'status' => 0,
            );
            $update = $this->db->update('school_affiliation', $data, array('id' => $id));
            $this->showSuccessMessage('ลบข้อมูลสำเร็จ');
        }
        redirect('manage_affiliation_schools', 'refresh');
    }

    public function insert()
    {
        $school_affiliation = $_POST['school_affiliation'];
        if ($school_affiliation != '') {
            $date_now = $this->getDateNow();
            $data = array(
                'school_affiliation' => $school_affiliation,
                'created_date' => $date_now,
                'created_by' => logged('id'),
                'updated_date' => $date_now,
                'updated_by' => logged('id'),
                'status' => 1,
            );
            $insert = $this->db->insert('school_affiliation', $data);
            $this->showSuccessMessage('เพิ่มข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_affiliation_schools', 'refresh');
    }

    public function update($id)
    {
        if (empty($id)) {
            redirect('manage_affiliation_schools');
        }
        $school_affiliation = $_POST['school_affiliation'];
        if ($school_affiliation != '') {
            $date_now = $this->getDateNow();
            $data = array(
                'school_affiliation' => $school_affiliation,
                'updated_date' => $date_now,
                'updated_by' => logged('id'),
            );
            $update = $this->db->update('school_affiliation', $data, array('id' => $id));
            $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_affiliation_schools', 'refresh');
    }

    //TODO: DataTable Query
    function data_table_query()
    {
        $this->jsonResponse($this->schools_model->data_table_affiliation_schools_query($this->inputGet()));
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
