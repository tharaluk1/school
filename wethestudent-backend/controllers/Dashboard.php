<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Mobility2u_Controller
{

    public function index()
    {
        $this->loadView('dashboard', $this->page_data);
    }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */