<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_schools_comment extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'จัดการ Comment';
        $this->page_data['page']->menu = 'manage_schools_comment';
    }

    public function index()
    {
        $this->page_data['schools'] = array();
        $this->load->view('manage_schools_comment/list', $this->page_data);
    }

    public function delete($id)
    {
        if (empty($id)) {
            redirect('manage_schools_comment');
        }

        $ids = explode(',', urldecode($id));

        foreach ($ids as $key => $id) {
            $this->db->delete('school_comment', array('id' => $id));
        }

        $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        redirect('manage_schools_comment', 'refresh');
    }

    public function data_table_query()
    {
        $school_id = $this->inputSegment(3);
        $school_checklist_id = $this->inputSegment(4);

        $this->jsonResponse($this->schools_model->data_table_schools_comment_query($this->inputGet(),
            $school_id, $school_checklist_id));
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
