<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_about_us extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'จัดการ About us';
        $this->page_data['page']->menu  = 'manage_about_us';
    }

    public function index()
    {
        $this->page_data['data'] = $this->schools_model->getAboutUs();
        $this->load->view('manage_about_us/edit', $this->page_data);
    }

    public function update()
    {
        $title       = $_POST['title'];
        $description = $_POST['description'];
        $line_id     = $_POST['line_id'];
        $image_delete = $_POST['image_delete'];

        if ($title != '') {
            $date_now = date('Y-m-d H:i:s');
            $data     = array(
                'title'        => $title,
                'description'  => $description,
                'line_id'      => $line_id,
                'updated_date' => $date_now,
                'updated_by'   => logged('id'),
                'status'       => 1,
            );

            if($image_delete=='1' && empty($_FILES['image']['name'])){
                $data["image"] = "";
            }
            
            $update = $this->db->update('about_us', $data, array('id' => 1));
            if (!empty($_FILES['image']['name'])) {

                $path       = $_FILES['image']['name'];
                $ext        = pathinfo($path, PATHINFO_EXTENSION);
                $image_name = md5($_FILES["image"]["name"] . time()) . '.' . $ext;
                $this->uploadlib->initialize([
                    'file_name' => $image_name . '.' . $ext,
                ]);
                $this->checkFolder('about_us');
                $image = $this->uploadlib->uploadImage('image', 'about_us');
                if ($image['status']) {
                    $this->db->update('about_us', ['image' => $image['data']['orig_name']], ['id' => 1]);
                }
            }
            $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_about_us', 'refresh');
    }

    public function checkFolder($path)
    {
        $file_path = '../public/uploads/' . $path;
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777);
        }
    }

    public function delete_image(){
        $date_now = date('Y-m-d H:i:s');
        $data     = array(
            'image'      => "",
            'updated_date' => $date_now,
            'updated_by'   => logged('id'),
            'status'       => 1
        );
        $update = $this->db->update('about_us', $data, array('id' => 1));

        $this->sendResponse($update);
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
