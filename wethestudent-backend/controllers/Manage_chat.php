<?php 

class Manage_chat extends Mobility2u_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        $this->page_data['page']->title = 'รายการห้องแชท';
        $this->page_data['page']->menu = 'manage_chat';
    }

    public function index()
    {
        $this->page_data['page']->submenu = 'manage_chat';
        $this->page_data['schools'] = array();
        //var_dump ( $this->page_data['page']);exit;
        $this->load->view('chat/chat_list', $this->page_data);
    }
    
    public function room($room)
    {
        // var_dump($room);exit;
        if (empty($room)) {
            redirect('manage_chat');
        }

        $this->page_data['page']->submenu = 'manage_chat';
        $this->page_data['room'] = strval($room);
        // $this->page_data['school_data'] = $this->schools_model->getDetail_cus_ByID($id,$school_checklist_id);
        // print_r($this->page_data['school_data']);exit;
        // $this->page_data['school_checklist'] = $this->schools_model->getSchoolCheckListMaster();
        // $this->page_data['school_sharing'] = $this->schools_model->getSchoolSharingByID($id);

        $this->load->view('chat/chat_room', $this->page_data);
    }

    public function data_chat_list()
    {
        $this->jsonResponse($this->chat_model->chat_list_data($this->inputGet()));
    }

    public function data_chat_room_list()
    {
        $room = $_POST['room'];
        $data = $this->chat_model->get_chat_room($room);
        $this->jsonResponse($data);
    }

    // public function data_chat_room_row()
    // {
    //     $room = $_POST['room'];
    //     $data = $this->chat_model->get_chat_room_row($room);
    //     $this->jsonResponse($data);
    // }

    public function send_text_admin()
    {
        $room = $this->input->post('room');
        $name = $this->input->post('name');
        $text = $this->input->post('text');
        $type = $this->input->post('type');
        $flag = $this->input->post('flag');
        $date_now = date('Y-m-d H:i:s');

        $data = array(
            "room" => $room,
            "name" => $name,
            "text" => $text,
            "type" => $type,
            "flag" => $flag,
            "create_date" => $date_now,
            "update_date" => $date_now
        );

        $result = $this->chat_model->insert_chat_admin($data);
        $this->jsonResponse($result);
    }

    public function delete_chat_room($room)
    {
        $result = $this->chat_model->delete_chat_text_room(array("room" => $room,));
        $this->showSuccessMessage('ลบข้อมูลสำเร็จ');
        redirect('manage_chat');
    }

}