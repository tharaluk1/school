<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tables extends Mobility2u_Controller {

	public function simple()
	{
		$this->view('simple');
	}

	public function data()
	{
		$this->view('data');
	}






	private function view($key)
	{
		$this->load->view('adminlte/tables/'.$key, $this->page_data);
	}

}

/* End of file Tables.php */
/* Location: ./application/controllers/adminlte/Tables.php */