<?php 

class log_chat extends Mobility2u_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        $this->page_data['page']->title = 'ประวัติ';
        $this->page_data['page']->menu = 'log_chat';
    }

    public function index()
    {
        $this->page_data['page']->submenu = 'log_chat';
        $this->page_data['schools'] = array();
        //var_dump ( $this->page_data['page']);exit;
        $this->load->view('chat/logchat_list', $this->page_data);
    }
    
    public function detail($room)
    {

        if (empty($room)) {
            redirect('log_chat');
        }

        $this->page_data['page']->submenu = 'log_chat';
        $this->page_data['room'] = $room;
        // $this->page_data['school_data'] = $this->schools_model->getDetail_cus_ByID($id,$school_checklist_id);
        // print_r($this->page_data['school_data']);exit;
        // $this->page_data['school_checklist'] = $this->schools_model->getSchoolCheckListMaster();
        // $this->page_data['school_sharing'] = $this->schools_model->getSchoolSharingByID($id);
        $this->load->view('chat/logchat_detail', $this->page_data);
    }

    public function data_log_chat_list()
    {
        $this->jsonResponse($this->chat_model->log_chat_list_data($this->inputGet()));
    }

    public function log_chat_detail()
    { 
        $room = $_POST['room'];
        $data = $this->chat_model->get_log_chat_detail($room);
        $this->jsonResponse($data);
        // print_r($data);exit;
    }

    

}