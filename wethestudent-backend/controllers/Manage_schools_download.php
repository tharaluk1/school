<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_schools_download extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'จัดการข้อมูลโรงเรียน';
        $this->page_data['page']->menu  = 'manage_schools';
    }

    public function index()
    {
        $this->page_data['page']->submenu = 'manage_schools_download';
        $this->page_data['schools']       = array();
        $this->load->view('manage_schools_download/list', $this->page_data);
    }

    public function add()
    {
        $this->page_data['page']->submenu = 'manage_schools_download';
        $this->load->view('manage_schools_download/add', $this->page_data);
    }

    public function edit($id)
    {
        $this->page_data['page']->submenu = 'manage_schools_download';
        $this->page_data['id']            = $id;
        $this->page_data['data']          = $this->schools_model->getSchoolDownloadByID($id);
        $this->load->view('manage_schools_download/edit', $this->page_data);
    }

    public function insert()
    {
        $title = $_POST['title'];
        if ($title != '') {
            $date_now = date('Y-m-d H:i:s');
            $data     = array(
                'title'        => $title,
                'created_date' => $date_now,
                'created_by'   => logged('id'),
                'updated_date' => $date_now,
                'updated_by'   => logged('id'),
                'status'       => 1,
            );
            $insert = $this->db->insert('school_download', $data);
            $id     = $this->db->insert_id();
            if (!empty($_FILES['icon']['name'])) {

                $path       = $_FILES['icon']['name'];
                $ext        = pathinfo($path, PATHINFO_EXTENSION);
                $image_name = md5($_FILES["icon"]["name"] . time()) . '.' . $ext;
                $this->uploadlib->initialize([
                    'file_name' => $image_name . '.' . $ext,
                ]);
                $this->checkFolder('school_download');
                $icon = $this->uploadlib->uploadImage('icon', 'school_download');
                if ($icon['status']) {
                    $this->db->update('school_download', ['icon' => $icon['data']['orig_name']], array('id' => $id));
                }
            }
            $this->showSuccessMessage('เพิ่มข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_schools_download', 'refresh');
    }

    public function update($id)
    {
        if (empty($id)) {
            redirect('manage_schools_download');
        }

        $title = $_POST['title'];
        if ($title != '') {
            $date_now = date('Y-m-d H:i:s');
            $data     = array(
                'title'        => $title,
                'updated_date' => $date_now,
                'updated_by'   => logged('id'),
            );
            $update = $this->db->update('school_download', $data, ['id' => $id]);
            if (!empty($_FILES['icon']['name'])) {

                $path       = $_FILES['icon']['name'];
                $ext        = pathinfo($path, PATHINFO_EXTENSION);
                $image_name = md5($_FILES["icon"]["name"] . time()) . '.' . $ext;
                $this->uploadlib->initialize([
                    'file_name' => $image_name . '.' . $ext,
                ]);
                $this->checkFolder('school_download');
                $icon = $this->uploadlib->uploadImage('icon', 'school_download');
                if ($icon['status']) {
                    $this->db->update('school_download', ['icon' => $icon['data']['orig_name']], ['id' => $id]);
                }
            }
            $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_schools_download', 'refresh');
    }

    public function checkFolder($path)
    {
        $file_path = '../public/uploads/' . $path;
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777);
        }
    }

    public function delete($id)
    {
        if (empty($id)) {
            redirect('manage_schools_download');
        }

        $data = $this->schools_model->getSchoolDownloadByID($id);

        $file_path = '../public/uploads/school_download/' . $data->icon;
        @unlink($file_path);
        $this->db->delete('school_download', array('id' => $id));
        $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        redirect('manage_schools_download', 'refresh');
    }

    public function data_table_query()
    {
        $this->jsonResponse($this->schools_model->data_table_schools_download_query($this->inputGet()));
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
