<?php 

class manage_schools_img extends Mobility2u_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        $this->page_data['page']->title = 'จัดการรูปภาพ';
        $this->page_data['page']->menu = 'manage_schools';
    }

    public function index()
    {
        $this->page_data['page']->submenu = 'manage_schools_img';
        $this->page_data['schools'] = array();
        //var_dump ( $this->page_data['page']);exit;
        $this->load->view('manage_schools_img/list', $this->page_data);
    }  

    public function edit($querystrin)
    {

        $s = explode("and", strtolower($querystrin));
        $id = $s[0];
        $school_checklist_id= $s[1];

        if (empty($id)) {
            redirect('manage_schools_img');
        }

        $this->page_data['page']->submenu = 'manage_schools_img';
        $this->page_data['id'] = $id;
        $this->page_data['school_data'] = $this->schools_model->getDetail_cus_ByID($id,$school_checklist_id);
        // print_r($this->page_data['school_data']);exit;
        $this->page_data['school_checklist'] = $this->schools_model->getSchoolCheckListMaster();
        $this->page_data['school_sharing'] = $this->schools_model->getSchoolSharingByID($id);

        $this->load->view('manage_schools_img/edit', $this->page_data);
    }

    public function data_table_query_img()
    {
        $this->jsonResponse($this->schools_model->data_table_schools_data_query_cus($this->inputGet()));
    }

    public function update_school_check_list_cus()
    {
        $check_list_id = $this->input->post("check_list_id");

        $result = $this->schools_model->update_school_check_list_image_cus(
            array(
                "id" => $check_list_id,
            )
        );

        $this->sendResponse($result);
    }
}

?>