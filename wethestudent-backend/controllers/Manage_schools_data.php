<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manage_schools_data extends Mobility2u_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->page_data['page']->title = 'จัดการข้อมูลโรงเรียน';
        $this->page_data['page']->menu = 'manage_schools';
    }

    public function index()
    {
        $this->page_data['page']->submenu = 'manage_schools_data';
        $this->page_data['schools'] = array();
        $this->load->view('manage_schools_data/list', $this->page_data);
    }

    public function add()
    {
        $this->page_data['page']->submenu = 'manage_schools_data';
        $this->page_data['school_affiliation_list'] = $this->schools_model->getSchoolAffiliationList();
        $this->page_data['school_download'] = $this->schools_model->getSchoolDownload();
        $this->page_data['school_checklist'] = $this->schools_model->getSchoolCheckListMaster();
        $this->page_data['school_checklist_items'] = $this->schools_model->getCheckListItemsMaster();
        $this->page_data['province'] = $this->schools_model->getProvince();
        $this->load->view('manage_schools_data/add', $this->page_data);
    }

    public function edit($id)
    {
        if (empty($id)) {
            redirect('manage_schools_data');
        }

        $this->page_data['page']->submenu = 'manage_schools_data';
        $this->page_data['id'] = $id;
        $this->page_data['school_data'] = $this->schools_model->getSchoolDataByID($id);
        $this->page_data['school_affiliation_list'] = $this->schools_model->getSchoolAffiliationList();
        $this->page_data['province'] = $this->schools_model->getProvince();
        $this->page_data['amphur'] = $this->schools_model->getAmphur($this->page_data['school_data']->province);
        $this->page_data['district'] = $this->schools_model->getDistrict($this->page_data['school_data']->amphur);
        $this->page_data['school_checklist'] = $this->schools_model->getSchoolCheckListMaster();
        $this->page_data['school_sharing'] = $this->schools_model->getSchoolSharingByID($id);
        $this->page_data['school_download'] = $this->schools_model->getSchoolDownload();
        $school_download_files_data = $this->schools_model->getSchoolDownloadFiles($id);

        $school_download_files = array();
        $school_download_files_id = array();
        foreach ($school_download_files_data as $key => $v) {
            $school_download_files[$v->school_download_id] = $v->path;
            $school_download_files_id[$v->school_download_id] = $v->id;
        }

        $this->page_data['school_download_files_id'] = $school_download_files_id;
        $this->page_data['school_download_files'] = $school_download_files;
        $this->load->view('manage_schools_data/edit', $this->page_data);
    }

    public function get_amphur_list()
    {
        $province = $_POST['province'];
        if ($province > 0) {
            $amphur_list = $this->schools_model->getAmphur($province);
            $html = '<select class="form-control" id="school_amphur" name="school_amphur">';
            foreach ($amphur_list as $key => $v) {
                $html .= '<option value="' . $v->AMPHUR_ID . '">' . $v->AMPHUR_NAME . '</option>';
            }
            $html .= '</select>';
            echo $html;
        } else {
            $html = '<select class="form-control" id="school_amphur" name="school_amphur"><option value="0">-= กรุณาเลือก =-</option></select>';
            echo $html;
        }
    }

    public function get_district_list()
    {
        $amphur = $_POST['amphur'];
        if ($amphur > 0) {
            $district_list = $this->schools_model->getDistrict($amphur);
            $html = '<select class="form-control" id="school_district" name="school_district">';
            foreach ($district_list as $key => $v) {
                $html .= '<option value="' . $v->DISTRICT_ID . '">' . $v->DISTRICT_NAME . '</option>';
            }
            $html .= '</select>';
            echo $html;
        } else {
            $html = '<select class="form-control" id="school_district" name="school_district"><option value="0">-= กรุณาเลือก =-</option></select>';
            echo $html;
        }
    }

    public function get_post_code()
    {
        $amphur = $_POST['amphur'];
        if ($amphur > 0) {
            $post_code = $this->schools_model->getPostCode($amphur);
            echo $post_code->POSTCODE;
        } else {
            echo '';
        }
    }

    public function insert()
    {

        $school_name = $_POST['school_name'];
        $school_affiliation = $_POST['school_affiliation'];
        $school_founded = dateFormat($_POST['school_founded'], 'DD/MM/YYYY', 'YYYY-MM-DD', 'BE2AD');
        $school_address = $_POST['school_address'];
        $school_province = $_POST['school_province'];
        $school_amphur = $_POST['school_amphur'];
        $school_district = $_POST['school_district'];
        $school_post_code = $_POST['school_post_code'];
        $school_tel = $_POST['school_tel'];
        $school_type = $_POST['school_type'];
        $school_level = !empty($_POST['school_level']) ? implode(',', $_POST['school_level']) : '';
        $school_style = $_POST['school_style'];
        $school_student_primary_education = $_POST['school_student_primary_education'];
        $school_student_upperelementary = $_POST['school_student_upperelementary'];
        $school_student_junior_high_school = $_POST['school_student_junior_high_school'];
        $school_student_high_school = $_POST['school_student_high_school'];
        $school_web = $_POST['school_web'];
        $school_lat = $_POST['school_lat'];
        $school_lng = $_POST['school_lng'];
        $school_update_date = $this->getDateNow(); //$_POST['school_update_date'];
        $school_source = $_POST['school_source'];
        $school_sharing_tag = isset($_POST['school_sharing_tag']) ? $_POST['school_sharing_tag'] : array();
        $school_sharing_url = isset($_POST['school_sharing_url']) ? $_POST['school_sharing_url'] : array();

        if ($school_name != '') {
            $date_now = date('Y-m-d H:i:s');
            $data = array(
                'name' => $school_name,
                'affiliation' => $school_affiliation,
                'founded' => $school_founded,
                'address' => $school_address,
                'province' => $school_province,
                'amphur' => $school_amphur,
                'district' => $school_district,
                'post_code' => $school_post_code,
                'tel' => $school_tel,
                'school_type' => $school_type,
                'level' => $school_level,
                'school_style' => $school_style,
                'primary_education' => $school_student_primary_education,
                'student_upperelementary' => $school_student_upperelementary,
                'junior_high_school' => $school_student_junior_high_school,
                'high_school' => $school_student_high_school,
                'web' => $school_web,
                'lat' => $school_lat,
                'lng' => $school_lng,
                'update_date' => $school_update_date,
                'source' => $school_source,
                'created_date' => $date_now,
                'created_by' => logged('id'),
                'updated_date' => $date_now,
                'updated_by' => logged('id'),
                'status' => 1,
            );

            $insert = $this->db->insert('school', $data);
            $id = $this->db->insert_id();

            $school_checklist = $this->schools_model->getSchoolCheckListMaster();

            $school_checklist_items = $this->schools_model->getCheckListItemsMaster();
            foreach ($school_checklist as $value) {
                foreach ($school_checklist_items as $v) {
                    $data_set = array(
                        'school_id' => $id,
                        'school_checklist_id' => $value->id,
                        'school_checklist_item_id' => $v->id,
                        'name' => $v->name,
                        'created_date' => $date_now,
                        'created_by' => logged('id'),
                        'updated_date' => $date_now,
                        'updated_by' => logged('id'),
                        'status' => 1,
                    );
                    $this->db->insert('school_checklist_items', $data_set);
                }
            }

            if (count($school_sharing_tag) > 0) {
                for ($i = 0; $i < count($school_sharing_tag); $i++) {
                    $this->db->insert('school_sharing', ['school_id' => $id, 'tag_name' => $school_sharing_tag[$i], 'url' => $school_sharing_url[$i], 'created_date' => date('Y-m-d H:i:s'), 'created_by' => logged('id')]);
                }
            }

            $school_download_key = array_filter(array_keys($_FILES), function ($key) {
                $startString = "school_download";
                $len = strlen($startString);
                if ((substr($key, 0, $len) === $startString)) {
                    return true;
                }
                return false;
            });

            $this->checkFolder('school_data');
            $this->checkFolder('school_data/' . $id);
            $this->checkFolder('school_data/' . $id . '/school_download');

            $uploads_dir = 'school_data/' . $id . '/school_download';

            $upload_result = array();

            foreach ($school_download_key as $key) {
                $file_school_download = $_FILES[$key];
                if ($file_school_download['name'] != "") {
                    $upload_result = $this->fileuploadlibs->uploadFile($file_school_download, $uploads_dir);
                    if ($upload_result['status']) {
                        $school_download_id = explode("#", $key)[1];
                        $v = $upload_result['data']['orig_name'];
                        $data_set = array(
                            'school_id' => $id,
                            'school_download_id' => $school_download_id,
                            'path' => $v,
                            'created_date' => $date_now,
                            'created_by' => logged('id'),
                            'updated_date' => $date_now,
                            'updated_by' => logged('id'),
                        );
                        $this->db->insert('school_data_download', $data_set);
                    }
                }
            }

            // if (isset($_FILES['school_download']['name']) > 0) {
            //     $this->checkFolder('school_data');
            //     $this->checkFolder('school_data/' . $id);
            //     $this->checkFolder('school_data/' . $id . '/school_download');
            //     $uploads_dir = '../public/uploads/school_data/' . $id . '/school_download';
            //     foreach ($_FILES['school_download']['name'] as $key => $v) {
            //         $tmp_name = $_FILES["school_download"]["tmp_name"][$key];
            //         $name = basename($_FILES["school_download"]["name"][$key]);
            //         if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
            //             $data_set = array(
            //                 'school_id' => $id,
            //                 'school_download_id' => $key,
            //                 'path' => $v,
            //                 'created_date' => date('Y-m-d H:i:s'),
            //                 'created_by' => logged('id'),
            //                 'updated_date' => date('Y-m-d H:i:s'),
            //                 'updated_by' => logged('id'),
            //             );
            //             $this->db->insert('school_data_download', $data_set);
            //         }
            //     }
            // }

            //TODO: check list

            $school_check_list_1 = explode("|", $this->input->post('school_check_list_1'));
            $school_check_list_2 = explode("|", $this->input->post('school_check_list_2'));
            $school_check_list_3 = explode("|", $this->input->post('school_check_list_3'));
            $school_check_list_4 = explode("|", $this->input->post('school_check_list_4'));

            $all_school_check_list = array($school_check_list_1, $school_check_list_2, $school_check_list_3, $school_check_list_4);

            $this->checkFolder('school_data');
            $this->checkFolder('school_data/' . $id);
            $this->checkFolder('school_data/' . $id . '/school_checklist');
            $uploads_dir = '../public/uploads/school_data/' . $id . '/school_checklist';
            $school_check_list_id = 0;

            foreach ($all_school_check_list as $school_check_list) {

                $school_check_list_id++;

                foreach ($school_check_list as $image_base64) {
                    if ($image_base64 != "") {

                        $check_list_path = $this->fileuploadlibs->base64ToImage($image_base64, $uploads_dir);
                        //TODO: Improve resize image

                        $data_set = array(
                            'school_id' => $id,
                            'school_checklist_id' => $school_check_list_id,
                            'path' => $check_list_path,
                            'created_date' => $date_now,
                            'created_by' => logged('id'),
                            'updated_date' => $date_now,
                            'updated_by' => logged('id'),
                        );
                        $this->db->insert('school_data_checklist', $data_set);
                    }
                }
            }

            // if (isset($_FILES['images']['name']) > 0) {
            //     $this->checkFolder('school_data');
            //     $this->checkFolder('school_data/' . $id);
            //     $this->checkFolder('school_data/' . $id . '/school_checklist');
            //     $uploads_dir = '../public/uploads/school_data/' . $id . '/school_checklist';
            //     foreach ($_FILES['images']['name'] as $keys => $v) {
            //         foreach ($_FILES['images']['name'][$keys] as $key => $value) {
            //             $tmp_name = $_FILES["images"]["tmp_name"][$keys][$key];
            //             $name = basename($_FILES["images"]["name"][$keys][$key]);
            //             if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
            //                 $data_set = array(
            //                     'school_id' => $id,
            //                     'school_checklist_id' => $keys,
            //                     'path' => $value,
            //                     'created_date' => date('Y-m-d H:i:s'),
            //                     'created_by' => logged('id'),
            //                     'updated_date' => date('Y-m-d H:i:s'),
            //                     'updated_by' => logged('id'),
            //                 );
            //                 $this->db->insert('school_data_checklist', $data_set);
            //             }
            //         }
            //     }
            // }

            if (isset($_FILES['images_standard']['name']) > 0) {
                $this->checkFolder('school_data');
                $this->checkFolder('school_data/' . $id);
                $this->checkFolder('school_data/' . $id . '/images_standard');
                $uploads_dir = 'school_data/' . $id . '/images_standard';
                foreach ($_FILES['images_standard']['name'] as $key => $v) {

                    $_FILES['images_info']['name'] = $_FILES["images_standard"]["name"][$key];
                    $_FILES['images_info']['type'] = $_FILES["images_standard"]["type"][$key];
                    $_FILES['images_info']['tmp_name'] = $_FILES["images_standard"]["tmp_name"][$key];
                    $_FILES['images_info']['error'] = $_FILES["images_standard"]["error"][$key];
                    $_FILES['images_info']['size'] = $_FILES["images_standard"]["size"][$key];

                    $check_list_info = $this->fileuploadlibs->uploadFile($_FILES['images_info'], $uploads_dir);

                    if ($check_list_info['status']) {
                        $data_set = array(
                            'path' => $check_list_info['data']['orig_name'],
                            'updated_date' => $date_now,
                            'created_date' => $date_now,
                            'updated_by' => logged('id'),
                        );

                        $where = array('school_id' => $id, 'school_checklist_id' => $key);

                        if ($this->schools_model->count_school_info($where) > 0) {
                            $this->db->update('school_data_standard', $data_set, $where);
                        } else {
                            $data_set['school_id'] = $id;
                            $data_set['school_checklist_id'] = $key;
                            $this->schools_model->insert_school_check_list_info($data_set);
                        }
                    }

                    // $tmp_name = $_FILES["images_standard"]["tmp_name"][$key];
                    // $name = basename($_FILES["images_standard"]["name"][$key]);
                    // if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
                    //     $data_set = array(
                    //         'school_id' => $id,
                    //         'school_checklist_id' => $key,
                    //         'path' => $v,
                    //         'created_date' => $date_now,
                    //         'created_by' => logged('id'),
                    //         'updated_date' => $date_now,
                    //         'updated_by' => logged('id'),
                    //     );
                    //     $this->db->insert('school_data_standard', $data_set);
                    // }
                }
            }

            if (!empty($_FILES['school_image']['name'])) {
                $path = $_FILES['school_image']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $image_name = md5($_FILES["school_image"]["name"] . time()) . '.' . $ext;
                $this->uploadlib->initialize([
                    'file_name' => $image_name . '.' . $ext,
                ]);
                $this->checkFolder('school_data');
                $this->checkFolder('school_data/' . $id);
                $this->checkFolder('school_data/' . $id . '/school_image');
                $image = $this->uploadlib->uploadImage('school_image', 'school_data/' . $id . '/school_image');
                if ($image['status']) {
                    $this->db->update('school', ['school_image' => $image['data']['orig_name']], ['id' => $id]);
                }
            }

            $this->showSuccessMessage('เพิ่มข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_schools_data', 'refresh');
    }

    public function update($id)
    {
        if (empty($id)) {
            redirect('manage_schools_data');
        }

        $school_name = $_POST['school_name'];
        $school_affiliation = $_POST['school_affiliation'];
        $school_founded = dateFormat($_POST['school_founded'], 'DD/MM/YYYY', 'YYYY-MM-DD', 'BE2AD');
        $school_address = $_POST['school_address'];
        $school_province = $_POST['school_province'];
        $school_amphur = $_POST['school_amphur'];
        $school_district = $_POST['school_district'];
        $school_post_code = $_POST['school_post_code'];
        $school_tel = $_POST['school_tel'];
        $school_type = $_POST['school_type'];
        $school_level = !empty($_POST['school_level']) ? implode(',', $_POST['school_level']) : '';
        $school_style = $_POST['school_style'];
        $school_student_primary_education = $_POST['school_student_primary_education'];
        $school_student_upperelementary = $_POST['school_student_upperelementary'];
        $school_student_junior_high_school = $_POST['school_student_junior_high_school'];
        $school_student_high_school = $_POST['school_student_high_school'];
        $school_web = $_POST['school_web'];
        $school_lat = $_POST['school_lat'];
        $school_lng = $_POST['school_lng'];
        $school_update_date = $this->getDateNow(); //$_POST['school_update_date'];
        $school_source = $_POST['school_source'];
        $school_sharing_tag = isset($_POST['school_sharing_tag']) ? $_POST['school_sharing_tag'] : array();
        $school_sharing_url = isset($_POST['school_sharing_url']) ? $_POST['school_sharing_url'] : array();

        if ($school_name != '') {
            $date_now = date('Y-m-d H:i:s');
            $data = array(
                'name' => $school_name,
                'affiliation' => $school_affiliation,
                'founded' => $school_founded,
                'address' => $school_address,
                'province' => $school_province,
                'amphur' => $school_amphur,
                'district' => $school_district,
                'post_code' => $school_post_code,
                'tel' => $school_tel,
                'school_type' => $school_type,
                'level' => $school_level,
                'school_style' => $school_style,
                'primary_education' => $school_student_primary_education,
                'student_upperelementary' => $school_student_upperelementary,
                'junior_high_school' => $school_student_junior_high_school,
                'high_school' => $school_student_high_school,
                'web' => $school_web,
                'lat' => $school_lat,
                'lng' => $school_lng,
                'update_date' => $school_update_date,
                'source' => $school_source,
                'updated_date' => $date_now,
                'updated_by' => logged('id'),
                'status' => 1,
            );

            $update = $this->db->update('school', $data, array('id' => $id));

            if (count($school_sharing_tag) > 0) {
                $this->db->delete('school_sharing', ['school_id' => $id]);
                for ($i = 0; $i < count($school_sharing_tag); $i++) {
                    $this->db->insert('school_sharing', ['school_id' => $id, 'tag_name' => $school_sharing_tag[$i], 'url' => $school_sharing_url[$i], 'created_date' => date('Y-m-d H:i:s'), 'created_by' => logged('id')]);
                }
            }

            $school_download_key = array_filter(array_keys($_FILES), function ($key) {
                $startString = "school_download";
                $len = strlen($startString);
                if ((substr($key, 0, $len) === $startString)) {
                    return true;
                }
                return false;
            });

            $this->checkFolder('school_data');
            $this->checkFolder('school_data/' . $id);
            $this->checkFolder('school_data/' . $id . '/school_download');

            $uploads_dir = 'school_data/' . $id . '/school_download';

            $upload_result = array();

            foreach ($school_download_key as $key) {
                $file_school_download = $_FILES[$key];
                if ($file_school_download['name'] != "") {
                    $upload_result = $this->fileuploadlibs->uploadFile($file_school_download, $uploads_dir);

                    if ($upload_result['status']) {
                        $school_download_id = explode("#", $key)[1];
                        $v = $upload_result['data']['orig_name'];
                        //TODO: Check update or insert
                        $check_data = $this->schools_model->checkDataSchoolDownload($id, $school_download_id);

                        if (!empty($check_data)) {
                            $data_set = array(
                                'path' => $v,
                                'updated_date' => date('Y-m-d H:i:s'),
                                'updated_by' => logged('id'),
                            );
                            $this->db->update('school_data_download', $data_set, array('school_id' => $id, 'school_download_id' => $school_download_id));
                        } else {
                            $data_set = array(
                                'school_id' => $id,
                                'school_download_id' => $school_download_id,
                                'path' => $v,
                                'created_date' => date('Y-m-d H:i:s'),
                                'created_by' => logged('id'),
                                'updated_date' => date('Y-m-d H:i:s'),
                                'updated_by' => logged('id'),
                            );
                            $this->db->insert('school_data_download', $data_set);
                        }
                    }
                }
            }

            //TODO: check list upload

            $school_check_list_1 = explode("|", $this->input->post('school_check_list_1'));
            $school_check_list_2 = explode("|", $this->input->post('school_check_list_2'));
            $school_check_list_3 = explode("|", $this->input->post('school_check_list_3'));
            $school_check_list_4 = explode("|", $this->input->post('school_check_list_4'));

            $all_school_check_list = array($school_check_list_1, $school_check_list_2, $school_check_list_3, $school_check_list_4);

            $this->checkFolder('school_data');
            $this->checkFolder('school_data/' . $id);
            $this->checkFolder('school_data/' . $id . '/school_checklist');
            $uploads_dir = '../public/uploads/school_data/' . $id . '/school_checklist';
            $school_check_list_id = 0;

            foreach ($all_school_check_list as $school_check_list) {

                $school_check_list_id++;

                foreach ($school_check_list as $image_base64) {
                    if ($image_base64 != "") {

                        $check_list_path = $this->fileuploadlibs->base64ToImage($image_base64, $uploads_dir);
                        //TODO: Improve resize image

                        $data_set = array(
                            'school_id' => $id,
                            'school_checklist_id' => $school_check_list_id,
                            'path' => $check_list_path,
                            'created_date' => $date_now,
                            'created_by' => logged('id'),
                            'updated_date' => $date_now,
                            'updated_by' => logged('id'),
                        );
                        $this->db->insert('school_data_checklist', $data_set);
                    }
                }
            }

            // if (isset($_FILES['images']['name']) > 0) {
            //     $this->checkFolder('school_data');
            //     $this->checkFolder('school_data/' . $id);
            //     $this->checkFolder('school_data/' . $id . '/school_checklist');
            //     $uploads_dir = '../public/uploads/school_data/' . $id . '/school_checklist';
            //     foreach ($_FILES['images']['name'] as $keys => $v) {
            //         foreach ($_FILES['images']['name'][$keys] as $key => $value) {
            //             $tmp_name = $_FILES["images"]["tmp_name"][$keys][$key];
            //             $name = basename($_FILES["images"]["name"][$keys][$key]);
            //             if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
            //                 $data_set = array(
            //                     'school_id' => $id,
            //                     'school_checklist_id' => $keys,
            //                     'path' => $value,
            //                     'created_date' => date('Y-m-d H:i:s'),
            //                     'created_by' => logged('id'),
            //                     'updated_date' => date('Y-m-d H:i:s'),
            //                     'updated_by' => logged('id'),
            //                 );
            //                 $this->db->insert('school_data_checklist', $data_set);
            //             }
            //         }
            //     }
            // }

            if (isset($_FILES['images_standard']['name']) > 0) {
                $this->checkFolder('school_data');
                $this->checkFolder('school_data/' . $id);
                $this->checkFolder('school_data/' . $id . '/images_standard');
                $uploads_dir = 'school_data/' . $id . '/images_standard';
                foreach ($_FILES['images_standard']['name'] as $key => $v) {

                    $_FILES['images_info']['name'] = $_FILES["images_standard"]["name"][$key];
                    $_FILES['images_info']['type'] = $_FILES["images_standard"]["type"][$key];
                    $_FILES['images_info']['tmp_name'] = $_FILES["images_standard"]["tmp_name"][$key];
                    $_FILES['images_info']['error'] = $_FILES["images_standard"]["error"][$key];
                    $_FILES['images_info']['size'] = $_FILES["images_standard"]["size"][$key];

                    $check_list_info = $this->fileuploadlibs->uploadFile($_FILES['images_info'], $uploads_dir);

                    if ($check_list_info['status']) {
                        $data_set = array(
                            'path' => $check_list_info['data']['orig_name'],
                            'updated_date' => $date_now,
                            'created_date' => $date_now,
                            'updated_by' => logged('id'),
                        );

                        $where = array('school_id' => $id, 'school_checklist_id' => $key);

                        if ($this->schools_model->count_school_info($where) > 0) {
                            $this->db->update('school_data_standard', $data_set, $where);
                        } else {
                            $data_set['school_id'] = $id;
                            $data_set['school_checklist_id'] = $key;
                            $this->schools_model->insert_school_check_list_info($data_set);
                        }
                    }
                }
            }

            if (!empty($_FILES['school_image']['name'])) {
                $path = $_FILES['school_image']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $image_name = md5($_FILES["school_image"]["name"] . time()) . '.' . $ext;
                $this->uploadlib->initialize([
                    'file_name' => $image_name . '.' . $ext,
                ]);
                $this->checkFolder('school_data');
                $this->checkFolder('school_data/' . $id);
                $this->checkFolder('school_data/' . $id . '/school_image');
                $image = $this->uploadlib->uploadImage('school_image', 'school_data/' . $id . '/school_image');
                if ($image['status']) {
                    $this->db->update('school', ['school_image' => $image['data']['orig_name']], ['id' => $id]);
                }
            }

            $this->showSuccessMessage('แก้ไขข้อมูลสำเร็จ');
        } else {
            $this->showErrorMessage('ไม่สามารถทำรายการได้ ลองใหม่อีกครั้ง');
        }
        redirect('manage_schools_data', 'refresh');
    }

    public function delete($id)
    {
        if (empty($id)) {
            redirect('manage_schools_data');
        }

        $file_path = '../public/uploads/school_data/' . $id;
        @rrmdir($file_path);
        $this->db->delete('school', array('id' => $id));
        $this->db->delete('school_checklist', array('school_id' => $id));
        $this->db->delete('school_checklist_items', array('school_id' => $id));
        $this->db->delete('school_check_list_rating', array('school_id' => $id));
        $this->db->delete('school_check_list_rating_logs', array('school_id' => $id));
        $this->db->delete('school_comment', array('school_id' => $id));
        $this->db->delete('school_data_checklist', array('school_id' => $id));
        $this->db->delete('school_data_download', array('school_id' => $id));
        $this->db->delete('school_data_standard', array('school_id' => $id));
        $this->showSuccessMessage('ลบข้อมูลสำเร็จ');
        redirect('manage_schools_data');
    }

    public function data_table_query()
    {
        $this->jsonResponse($this->schools_model->data_table_schools_data_query($this->inputGet()));
    }

    public function checkFolder($path)
    {
        $file_path = '../public/uploads/' . $path;
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777);
        }
    }

    public function school_list()
    {
        $this->jsonResponse($this->schools_model->select_school());
    }

    public function school_check_list_file_upload()
    {
        print_r($_FILES);
    }

    public function school_check_list_image_delete()
    {
        $school_id = $this->input->post("school_id");
        $school_check_list_id = $this->input->post("school_check_list_id");
        $school_check_list_image_id = $this->input->post("school_check_list_image_id");

        $result = $this->schools_model->delete_image_check_list(
            array(
                "school_id" => $school_id,
                "school_checklist_id" => $school_check_list_id,
                "id" => $school_check_list_image_id,
            )
        );

        $this->sendResponse($result);
    }

    public function school_download_item_delete()
    {
        $school_id = $this->input->post("school_id");
        $school_download_id = $this->input->post("school_download_id");
        $school_download_files_id = $this->input->post("school_download_files_id");
        $file_name = $this->input->post("file_name");

        $result = $this->schools_model->delete_school_download(
            array(
                "school_id" => $school_id,
                "school_download_id" => $school_download_id,
                "id" => $school_download_files_id,
            )
        );

        if ($result) {
            $uploads_dir = 'school_data/' . $school_id . '/school_download';

            $sc = '../public/uploads/' . $uploads_dir . "/" . $file_name;

            unlink($sc);
        }

        $this->sendResponse($result);
    }

    public function school_check_list_info_delete()
    {
        $school_id = $this->input->post("school_id");
        $check_list_id = $this->input->post("check_list_id");
        $check_list_info_id = $this->input->post("check_list_info_id");
        $image_name = $this->input->post("image_name");

        $result = $this->schools_model->delete_school_check_list_image_info(
            array(
                "school_id" => $school_id,
                "school_checklist_id" => $check_list_id,
                "id" => $check_list_info_id,
            )
        );

        if ($result) {
            $uploads_dir = 'school_data/' . $school_id . '/images_standard';

            $sc = '../public/uploads/' . $uploads_dir . "/" . $image_name;
            if (file_exists($sc)) {
                unlink($sc);
            }

        }

        $this->sendResponse($result);
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */
