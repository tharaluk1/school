<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FileUploadLibs
{
    protected $ci;

    public $Config;

    public function __construct()
    {
        $this->ci = &get_instance();

        $config['upload_path'] = '../public/uploads';
        // $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
        $config['allowed_types'] = '*';
        $config['overwrite'] = true;
        $config['remove_spaces'] = true;
        $config['width'] = 0;
        $config['height'] = 0;

        $this->Config = $config;
        $this->ci->load->library('upload', $config);
    }

    public function initialize($config = [])
    {
        $this->Config = array_merge($this->Config, $config);
        return $this->ci->upload->initialize($this->Config);
    }

    public function uploadFile($file = null, $path = '/')
    {
        if ($file == null) {
            return;
        }

        $_FILES['file_upload']['name'] = $file['name'];
        $_FILES['file_upload']['type'] = $file['type'];
        $_FILES['file_upload']['tmp_name'] = $file['tmp_name'];
        $_FILES['file_upload']['error'] = $file['error'];
        $_FILES['file_upload']['size'] = $file['size'];

        $config = $this->Config;
        $config['file_name'] = uniqid();
        
        $config['upload_path'] = $config['upload_path'] . '/' . trim($path, '/');

        $this->ci->upload->initialize($config);
        
        $return = array();
        
        if (!$this->ci->upload->do_upload('file_upload')) {
            $return = array('status' => false, 'error' => $this->ci->upload->display_errors());
        } else {
            $return = array('status' => true, 'data' => $this->ci->upload->data());
        }

        return $return;
    }

    // public function uploadFileMultiple($fileArray = null, $path = '/')
    // {
    //     // print_r($fileArray);
    //     $filesCount = count($fileArray['demo_file']['name']);
    //     $return = array();

    //     for ($i = 0; $i < $filesCount; $i++) {

    //         // $_FILES['file']['name'] = $fileArray['name'][$i];
    //         // $_FILES['file']['type'] = $fileArray['type'][$i];
    //         // $_FILES['file']['tmp_name'] = $fileArray['tmp_name'][$i];
    //         // $_FILES['file']['error'] = $fileArray['error'][$i];
    //         // $_FILES['file']['size'] = $fileArray['size'][$i];

    //         $_FILES['upload_File']['name'] = $_FILES['demo_file']['name'][$i];
    //         $_FILES['upload_File']['type'] = $_FILES['demo_file']['type'][$i];
    //         $_FILES['upload_File']['tmp_name'] = $_FILES['demo_file']['tmp_name'][$i];
    //         $_FILES['upload_File']['error'] = $_FILES['demo_file']['error'][$i];
    //         $_FILES['upload_File']['size'] = $_FILES['demo_file']['size'][$i];
            
    //         $uploadPath = 'uploads/files/';

    //         //$config = $this->Config;
    //         $config['upload_path'] = '../public/uploads';
    //         $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
    //         $config['overwrite'] = true;
    //         $config['remove_spaces'] = true;
    //         $config['width'] = 0;
    //         $config['height'] = 0;
    //         $config['upload_path'] = $config['upload_path'] . '/' . trim($path, '/');

    //         $this->ci->upload->initialize($config);

           
    //         if (!$this->ci->upload->do_upload('upload_File')) {
    //             $return[] = array('status' => false, 'error' => $this->ci->upload->display_errors());
    //         } else {
    //             $return[] = array('status' => true, 'data' => $this->ci->upload->data());
    //         }

    //     }

    //     return $return;
    // }

    public function base64ToImage($img_base64,$folderPath)

    {

        $image_parts = explode(";base64,", $img_base64);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);
        
        $file_name = uniqid().date('Y_m_d_H_i_s'). '.png';

        $file = $folderPath .'/'. $file_name;

        file_put_contents($file, $image_base64);

        return $file_name;
    }

}
