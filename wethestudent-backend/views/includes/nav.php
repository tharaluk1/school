<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<ul class="sidebar-menu" data-widget="tree">

  <li class="header">We The Students</li>

  <?php
foreach ($menu_list as $key => $v) {
    if (count($v->submenu) > 0) {
        $check = false;
        foreach ($v->submenu as $key => $submenu) {
            $check = hasPermissions($submenu->code);
            if ($check) {
                break;
            }
        }
        if ($check) {
            ?>
      <li class="treeview <?php echo ($page->menu == $v->code) ? 'active' : '' ?>">
        <a href="#">
          <i class="<?=$v->icon?>"></i> <span><?php echo $v->label; ?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php
foreach ($v->submenu as $key => $submenu) {
                ?>
            <?php if (hasPermissions($submenu->code)): ?>
            <li <?php echo ($page->submenu == $submenu->code) ? 'class="active"' : '' ?>>
              <a href="<?php echo url($submenu->url); ?>">
                <i class="fa fa-circle-o"></i> <?php echo $submenu->label; ?>
              </a>
            </li>
            <?php endif?>
            <?php
}
            ?>
        </ul>
      </li>
      <?php
}
    } else {
        ?>
      <?php if (hasPermissions($v->code)): ?>
      <li <?php echo ($page->menu == $v->code) ? 'class="active"' : '' ?>>
        <a href="<?php echo url($v->url); ?>">
          <i class="<?=$v->icon?>"></i> <span><?php echo $v->label; ?></span>
        </a>
      </li>
      <?php endif?>
      <?php
}
}
?>
</ul>