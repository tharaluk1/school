<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php include viewPath('includes/header'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $page->title;?>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">หน้ารายการ</h3>

      <div class="box-tools pull-right">
        <a href="<?php echo url('manage_schools_data/add') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> เพิ่มโรงเรียน</a>
        <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i></button>-->
      </div>

    </div>
    <div class="box-body">
      <table id="dataTableSchoolData" class="table table-bordered table-striped display" style="width:100%">
        <thead>
          <tr>
            <th>#</th>
            <th>ชื่อสังกัด</th>
            <th>ชื่อโรงเรียน</th>
            <th>วันที่แก้ไขล่าสุด</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer">
      Footer
    </div> -->
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->

<?php include viewPath('includes/footer'); ?>
<script src="<?php echo $url->assets ?>js/school_data.js"></script>