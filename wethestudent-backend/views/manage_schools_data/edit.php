<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
<?php include viewPath('includes/header');?>
<link rel="stylesheet" href="<?php echo $url->assets ?>/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        เมนูจัดการข้อมูลโรงเรียน
        <small>
            School Data
        </small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- form start -->
    <script>
        school_id = <?=$id?>
    </script>
    <?php echo form_open_multipart('manage_schools_data/update/' . $id, 'id="frm_school_data" class="form-horizontal form-validate"'); ?>
    <!-- <form class="form-horizontal form-validate" id="frm_school_data" form_open_multipart()
          action="<?php echo url('manage_schools_data/update/' . $id) ?>" method="post"
          enctype="multipart/form-data"> -->
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">เพิ่ม / แก้ไข</h3>
                <div class="box-tools pull-right">
                    <a href="<?php echo url('manage_schools_data') ?>" class="btn btn-flat btn-default"><i
                                class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_name">
                            ชื่อโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_name" name="school_name" required
                                   placeholder="ชื่อโรงเรียน" type="text" value="<?php echo $school_data->name; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_affiliation">
                            สังกัดสถานศึกษา
                        </label>
                        <div class="col-sm-8">
                            <select class="form-control select2" id="school_affiliation" name="school_affiliation" required>
                                <option value="">-= กรุณาเลือก =-</option>
                                <?php
foreach ($school_affiliation_list as $key => $row) {
    $select = '';
    if ($school_data->affiliation == $row->id) {
        $select = 'selected="selected"';
    }
    echo '<option ' . $select . ' value="' . $row->id . '">' . $row->school_affiliation . '</option>';
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_founded">
                            วัน / เดือน / ปี ก่อตั้ง
                        </label>
                        <div class="col-sm-8">
                            <input data-date-language="th-th" data-provide="school_founded" autocomplete="off" class="form-control" id="school_founded" name="school_founded" placeholder="วัน / เดือน / ปี ก่อตั้ง" type="text" value="<?php echo ($school_data->founded != '0000-00-00' ? dateFormat($school_data->founded, 'YYYY-MM-DD', 'DD/MM/YYYY', 'AD2BE') : ''); ?>"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_address">
                            ที่อยู่
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="7" id="school_address" name="school_address"
                                      placeholder="ที่อยู่"><?php echo $school_data->address; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_province">
                            จังหวัด
                        </label>
                        <div class="col-sm-8">
                        <select class="form-control select2" id="school_province"
                                            name="school_province" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($province as $key => $row) {
    $select = '';
    if ($school_data->province == $row->PROVINCE_ID) {
        $select = 'selected="selected"';
    }
    echo '<option ' . $select . ' value="' . $row->PROVINCE_ID . '">' . $row->PROVINCE_NAME . '</option>';
}
?>
                                    </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_amphur">
                        อำเภอ
                        </label>
                        <div class="col-sm-8">
                        <select class="form-control select2" id="school_amphur" name="school_amphur" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($amphur as $key => $row) {
    $select = '';
    if ($school_data->amphur == $row->AMPHUR_ID) {
        $select = 'selected="selected"';
    }
    echo '<option ' . $select . ' value="' . $row->AMPHUR_ID . '">' . $row->AMPHUR_NAME . '</option>';
}
?>
                                    </select>
                    </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_district">
                        ตำบล
                        </label>
                        <div class="col-sm-8">
                        <select class="form-control select2" id="school_district"
                                            name="school_district" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($district as $key => $row) {
    $select = '';
    if ($school_data->district == $row->DISTRICT_ID) {
        $select = 'selected="selected"';
    }
    echo '<option ' . $select . ' value="' . $row->DISTRICT_ID . '">' . $row->DISTRICT_NAME . '</option>';
}
?>
                                    </select>
                    </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_post_code">
                        รหัสไปรษณีย์
                        </label>
                        <div class="col-sm-8">
                        <input class="form-control" id="school_post_code" name="school_post_code"
                                           placeholder="รหัสไปรษณีย์" type="text"
                                           value="<?php echo $school_data->post_code; ?>">
                                    </input>
                    </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_tel">
                            เบอร์โทรศัพท์
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="school_tel" name="school_tel"
                                      placeholder="เบอร์โทรศัพท์"
                                      rows="7"><?php echo $school_data->tel; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_type">
                            ประเภทโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input checked type="radio" name="school_type" <?=($school_data->school_type == 'ST-001' ? 'checked="checked"' : '');?> id="school_type1" value="ST-001"> โรงเรียนรัฐบาล
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_type" <?=($school_data->school_type == 'ST-002' ? 'checked="checked"' : '');?> id="school_type2" value="ST-002"> โรงเรียนเอกชน
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_level">
                            ระดับที่เปิดสอน
                        </label>
                        <div class="col-sm-8">
                            <?php
$school_level = explode(',', $school_data->level);
?>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" <?=(in_array('อนุบาล', $school_level) ? 'checked="checked"' : '');?> id="school_level1" value="อนุบาล"> อนุบาล
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" <?=(in_array('ประถมศึกษา', $school_level) ? 'checked="checked"' : '');?> id="school_level2" value="ประถมศึกษา"> ประถมศึกษา
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" <?=(in_array('มัธยมต้น', $school_level) ? 'checked="checked"' : '');?> id="school_level3" value="มัธยมต้น"> มัธยมต้น
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" <?=(in_array('มัธยมปลาย', $school_level) ? 'checked="checked"' : '');?> id="school_level4" value="มัธยมปลาย"> มัธยมปลาย
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_style">
                            ลักษณะโรงเรียน
                        </label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input checked type="radio" name="school_style" <?=($school_data->school_style == 'SS-001' ? 'checked="checked"' : '');?> id="school_style1" value="SS-001"> ชายล้วน
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_style" <?=($school_data->school_style == 'SS-002' ? 'checked="checked"' : '');?> id="school_style2" value="SS-002"> หญิงล้วน
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="school_style" <?=($school_data->school_style == 'SS-003' ? 'checked="checked"' : '');?> id="school_style3" value="SS-003"> สหศึกษา
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_primary_education">จำนวนนักเรียนอนุบาล</label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_primary_education"
                                   name="school_student_primary_education"
                                   placeholder="จำนวนนักเรียนอนุบาล" type="number"
                                   value="<?php echo $school_data->primary_education; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_upperelementary">
                        จำนวนนักเรียนประถมศึกษา
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_upperelementary"
                                   name="school_student_upperelementary"
                                   placeholder="จำนวนนักเรียนประถมศึกษา" type="number"
                                   value="<?php echo $school_data->student_upperelementary; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_junior_high_school">
                        จำนวนนักเรียนมัธยมต้น
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_junior_high_school"
                                   name="school_student_junior_high_school" placeholder="จำนวนนักเรียนมัธยมต้น"
                                   type="number" value="<?php echo $school_data->junior_high_school; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_high_school">
                        จำนวนนักเรียนมัธยมปลาย
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_high_school"
                                   name="school_student_high_school" placeholder="จำนวนนักเรียนมัธยมปลาย" type="number"
                                   value="<?php echo $school_data->high_school; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_web">
                            เว็บไซต์
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_web" name="school_web" placeholder="เว็บไซต์"
                                   type="text" value="<?php echo $school_data->web; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_image">
                            รูปภาพ
                        </label>
                        <div class="col-sm-8">
                            <input type="file" accept=".jpg,.jpeg,.png" id="school_image" name="school_image">
                            <div id="show_school_image" <?php echo ($school_data->school_image == '' ? 'style="display: none;"' : ''); ?>>
                                <br>
                                <img id="image_school_example" src="<?php echo ($school_data->school_image != '' ? urlUpload('school_data/' . $id . '/school_image/' . $school_data->school_image) : ''); ?>" width="200">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_map">
                            ตำแหน่งโรงเรียน
                        </label>
                        <div class="col-sm-8">
                           <div id="mapid" style="height:450px;"></div>
                        </div>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-4">
                            <label class="control-label" for="school_lat">ละติจูด</label>
                            <input class="form-control" id="school_lat" name="school_lat" placeholder="ละติจูด"
                                   type="text" required>
                            </input>
                        </div>
                        <div class="col-sm-4">
                            <label class="control-label" for="school_lng">ลองจิจูด</label>
                            <input class="form-control" id="school_lng" name="school_lng" placeholder="ลองจิจูด"
                                   type="text" required>
                            </input>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Download</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_source">แหล่งที่มาข้อมูล</label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_source" name="school_source"
                                    placeholder="แหล่งที่มาของข้อมูล" type="text"
                                    value="<?php echo $school_data->source; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                <?php
if (count($school_download) > 0) {
    foreach ($school_download as $key => $v) {?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_download#<?=$v->id?>"><?=$v->title?></label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control" name="school_download#<?=$v->id?>" id="school_download_<?=$v->id?>" placeholder="Upload School Download" accept="application/msword, application/vnd.ms-excel, .xls, application/vnd.ms-powerpoint,
                            text/plain, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" onclick="fileClicked(event)" onchange="fileChanged(event)">
                        <?php
if (!empty($school_download_files[$v->id])) {
        ?>
                            <p class="help-block school_download_link_<?=$school_download_files_id[$v->id]?>"><a href="<?=urlUpload('school_data/' . $id . '/school_download' . '/' . $school_download_files[$v->id])?>" target="_bank"><?=$school_download_files[$v->id]?></a>&nbsp;&nbsp;<span onclick="on_delete_download_file('<?=$id?>','<?=$v->id?>','<?=$school_download_files_id[$v->id]?>','<?=$school_download_files[$v->id]?>','delete')" class="ic_closes" title="Delete" style="background-color: rgb(185, 185, 185);
    padding: 1px 7px !important;
    color: rgb(255, 0, 0) !important;
    font-weight: bold !important;
    cursor: pointer !important;
    text-align: center !important;
    font-size: 16px !important;
    line-height: 10px !important;
    border-radius: 50% !important;
    border: 0px solid red !important;">&times;</span></p>
                        <?php
}
        ?>
                        </div>
                    </div>
                    <?php
}
}
?>
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Checklist Admin</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                        <?php
$i = 1;
foreach ($school_checklist as $key => $v) {
    $active = '';
    if ($i == 1) {
        $active = 'active';
    }
    ?>
                            <li class="<?=$active?>"><a href="#tab_<?=$v->id?>" data-toggle="tab" aria-expanded="true"><?=$v->name?></a></li>
                            <?php
$i++;
}
?>
                </ul>
                <div class="tab-content">
                    <?php
$i = 1;
foreach ($school_checklist as $key => $v) {
    $active = '';
    if ($i == 1) {
        $active = 'active';
    }
    ?>
                    <div class="tab-pane <?=$active?>" id="tab_<?=$v->id?>">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">
                                    รูปภาพ  (<span id="current_add_image_<?=$v->id?>">0</span>-20)
                                </label>
                                    <div class="image_check_list_item_<?=$v->id?>">
                                        <input type="file" accept=".jpg, .jpeg, .png" class="images" onclick="school_check_list_image_click(this,<?=$v->id?>)" onchange="school_check_list_image_change(this,<?=$v->id?>)" id="image_check_list_<?=$v->id?>" name="image_check_list#<?=$v->id?>" multiple="multiple">
                                    </div>
                                    <div class="row" id="show_check_list_image_<?=$v->id?>">
                                        <?php
$element_id = $v->id;
    $images_check_list = $this->schools_model->getSchoolCheckListImages($id, $element_id);
    ?>
                                                <script>
                                                    if(school_check_list_image['<?=$element_id?>'] == undefined)
                                                            school_check_list_image['<?=$element_id?>'] = []
                                                </script>
                                                <?php
foreach ($images_check_list as $image_check_list_item) {
        $image_path = $image_check_list_item->path;
        $preview_id = 'image_preview_' . $image_check_list_item->id . '_' . $element_id;?>
                                                    <script>
                                                            file_item = {
                                                                    "name": '<?=$image_path?>',
                                                                    "lastModified": <?=$image_check_list_item->id?>,
                                                                    "file_base": ""
                                                                }
                                                            school_check_list_image['<?=$element_id?>'].push(file_item)
                                                    </script>
                                                                    <div class="col-md-3 col-lg-2 img-wraps" id="<?=$preview_id?>">
                                                                        <span onclick="on_delete_image_check_lists(<?=$preview_id?>,<?=$element_id?>,<?=$image_check_list_item->id?>,'delete')" class="closes" title="Delete">&times;</span>
                                                                        <img src="<?=urlUpload('school_data/' . $id . '/school_checklist' . '/' . $image_path)?>" class="img-responsive image_check_list">
                                                                    </div>
                                            <?php
                                            }
                                            ?>
                                            <script>
                                                $('#current_add_image_'+<?=$element_id?>).text(school_check_list_image['<?=$element_id?>'].length);
                                            </script>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <?php
                                $image = $this->schools_model->getCheckListItemsImage($id, $element_id);
                                $check_list_id = $v->id;
                                $check_list_info_id = $check_list_id;
                               if(!empty($image)){
                                $check_list_info_id = $image->id;
                               }
                                
                                    ?>
                                <div class="col-md-12 form-group">
                                    <label class="control-label">รูปเกณฑ์มาตราฐาน</label>
                                    <input type="file" accept=".jpg, .jpeg, .png" class="images_standard" id="<?=$check_list_id?>" name="images_standard[<?=$check_list_id?>]"> 
                                    <div class="row">
                                        <div class="col-md-3 col-lg-2 img-wraps" id="show_image_info_<?=$check_list_id?>" style="display: <?=empty($image)?'none':''?>">
                                            <span class="closes" id="btn_delete_image_info_<?=$check_list_id?>" onclick="on_delete_image_info('<?=$id?>','<?=$check_list_id?>','<?=$check_list_info_id?>','<?=(!empty($image->path) && $image->path != '') ? $image->path : ''; ?>')" title="Delete">&times;</span>
                                            <img id="show_standard_<?=$check_list_id?>" src="<?=!empty($image->path) && $image->path != '' ? urlUpload('school_data/' . $id . '/images_standard' . '/' . $image->path) : 'data:,'?>" class="img-responsive image_check_list">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                    $i++;
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Checklist User</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                        <?php
                            $i = 1;
                            foreach ($school_checklist as $key => $v) {
                                $active = '';
                                if ($i == 1) {
                                    $active = 'active';
                                }
                            ?>
                            <li class="<?=$active?>"><a href="#tab1_<?=$v->id?>" data-toggle="tab" aria-expanded="true"><?=$v->name?></a></li>
                            <?php
                                $i++;
                                }
                            ?>
                </ul>
                <div class="tab-content">
                    <?php
                        $i = 1;
                        foreach ($school_checklist as $key => $v) {
                            $active = '';
                            if ($i == 1) {
                                $active = 'active';
                            }
                    ?>
                    <div class="tab-pane <?=$active?>" id="tab1_<?=$v->id?>">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div class="row" id="show_check_list_image_<?=$v->id?>">
                                        <?php
                                            $element_id = $v->id;
                                            $images_check_list = $this->schools_model->getSchoolCheckListImages_show_cus($id, $element_id);
                                        ?>
                                                <script>
                                                    if(school_check_list_image['<?=$element_id?>'] == undefined)
                                                            school_check_list_image['<?=$element_id?>'] = []
                                                </script>
                                            <?php
                                                foreach ($images_check_list as $image_check_list_item) {
                                                    $image_path = $image_check_list_item->path;
                                                    $preview_id = 'image_preview_' . $image_check_list_item->id . '_' . $element_id;
                                            ?>
                                                <script>
                                                        file_item = {
                                                                "name": '<?=$image_path?>',
                                                                "lastModified": <?=$image_check_list_item->id?>,
                                                                "file_base": ""
                                                            }
                                                        school_check_list_image['<?=$element_id?>'].push(file_item)
                                                </script>
                                                <div class="col-md-3 col-lg-2 img-wraps" id="<?=$preview_id?>">
                                                    <span onclick="on_delete_image_check_lists(<?=$preview_id?>,<?=$element_id?>,<?=$image_check_list_item->id?>,'delete')" class="closes" title="Delete">&times;</span>
                                                    <img src="<?=urlUpload('school_data/' . $id . '/school_checklist' . '/' . $image_path)?>" class="img-responsive image_check_list">
                                                </div>
                                            <?php
                                                }
                                            ?>
                                            <script>
                                                $('#current_add_image_'+<?=$element_id?>).text(school_check_list_image['<?=$element_id?>'].length);
                                            </script>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <?php 
                    $i++;
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Sharing</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <label for="school_sharing_tag">Tag Name</label>
                            <input type="text" class="form-control" id="school_sharing_tag" placeholder="Tag Name">
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <label for="school_sharing_url">URL</label>
                            <input type="text" class="form-control" id="school_sharing_url" placeholder="URL">
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-primary" style="width: 100%;" id="btn_add_tag">เพิ่ม</button>
                        </div>
                        <!-- <div class="col-xs-4">
                            <label for="school_sharing_tag">Tag Name</label>
                            <input type="text" class="form-control" id="school_sharing_tag"
                                   placeholder="Tag Name">
                        </div>
                        <div class="col-xs-7">
                            <label for="school_sharing_url">URL</label>
                            <input type="text" class="form-control" id="school_sharing_url" placeholder="URL">
                        </div>
                        <div class="col-xs-1">
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <button type="button" class="btn btn-primary" id="btn_add_tag">เพิ่ม</button>
                        </div> -->
                    </div>
                    <?php
if (count($school_sharing) > 0) {
    $no = 1;
    foreach ($school_sharing as $key => $v) {
        echo '
                        <div id="tab_' . $no . '">
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <label for="school_sharing_tag">Tag Name</label>
                                    <input type="text" class="form-control" id="school_sharing_tag" placeholder="Tag Name" name="school_sharing_tag[]" value="' . $v->tag_name . '">
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <label for="school_sharing_url">URL</label>
                                    <input type="text" class="form-control" id="school_sharing_url" placeholder="URL" name="school_sharing_url[]" value="' . $v->url . '">
                                </div>
                                <div class="col-sm-12 col-md-2">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <button type="button" class="btn btn-danger btn_delete_tag" style="width: 100%;" id="' . $no . '">ลบ</button>
                                </div>
                            </div>
                        </div>
                    ';
        $no++;
    }
}
?>
                    <div id="form_tag"></div>
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

        <!-- Default box -->
        <div class="box">
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                        <button type="button" id="btn_submit" style="width: 100%" class="btn btn-flat btn-primary">บันทึกข้อมูล</button>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </form>
</section>
<!-- /.content -->

<script>
    var schoolLat = "<?=$school_data->lat;?>"
    var schoolLng = "<?=$school_data->lng;?>"
</script>
<!-- <script src="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<script src="<?php echo $url->assets ?>js/school_map.js"></script>
<script src="<?php echo $url->assets ?>js/school_data.js"></script>

<script>
    var txt_form_tag_no = <?php echo count($school_sharing); ?>;
    $(document).ready(function () {
        $('.form-validate').validate();
        //Initialize Select2 Elements
        // $('.select2').select2();
    });

    // $('#btn_submit').on('click', function () {
    //     $('#frm_school_data').submit();
    // });

    $('#btn_add_tag').on('click', function () {
        if ($('input[name="school_sharing_tag[]"]').length < 10) {
            var school_sharing_tag = $('#school_sharing_tag').val();
            var school_sharing_url = $('#school_sharing_url').val();
            var txt_form_tag = '<div id="tab_' + txt_form_tag_no + '"><hr><div class="row"><div class="col-sm-12 col-md-4"><label for="school_sharing_tag">Tag Name</label><input type="text" class="form-control" id="school_sharing_tag" name="school_sharing_tag[]" placeholder="Tag Name" value="' + school_sharing_tag + '"></div><div class="col-sm-12 col-md-6"><label for="school_sharing_url">URL</label><input type="text" class="form-control" id="school_sharing_url" name="school_sharing_url[]" placeholder="URL" value="' + school_sharing_url + '"></div><div class="col-sm-12 col-md-2"><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><button type="button" class="btn btn-danger btn_delete_tag" id="' + txt_form_tag_no + '"  style="width: 100%;">ลบ</button></div></div></div>';
            $('#form_tag').append(txt_form_tag);
            $('#school_sharing_tag').val('');
            $('#school_sharing_url').val('');
            txt_form_tag_no++;
        }
    });

    $(document).on("click", ".btn_delete_tag", function () {
        var id = $(this).attr('id');
        $('#tab_' + id).remove();
    });

    $('#school_province').on('change', function () {
        var province = $(this).val();
        if (province != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_amphur_list') ?>",
                type: "post",
                data: {
                    province: province
                },
                success: function (result) {
                    $('#school_amphur').html(result);
                }
            });
        }
    });
    $('#school_amphur').on('change', function () {
        var amphur = $(this).val();
        if (amphur != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_district_list') ?>",
                type: "post",
                data: {
                    amphur: amphur
                },
                success: function (result) {
                    $('#school_district').html(result);
                }
            });
        }
    });
    $('#school_district').on('change', function () {
        var amphur = $('#school_amphur').val();
        if (amphur != 0) {
            $.ajax({
                url: "<?php echo base_url('manage_schools_data/get_post_code') ?>",
                type: "post",
                data: {
                    amphur: amphur
                },
                success: function (result) {
                    $('#school_post_code').val(result);
                }
            });
        }
    });

    $('.images').change(function() {
        var id = $(this).attr('id');
        readURL(this, id);
    });

    $('.images_standard').change(function() {
        var id = $(this).attr('id');
        readURL1(this, id);
    });

    function readURL(input, div_id) {
        if (input.files) {
            var filesAmount = input.files.length;
            if (filesAmount > 20)
            {
                alert('เลือกรูปได้ไม่เกิน 20 รูป');
            }
            else
            {
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var image = '<div class="col-sm-6 col-md-3"><div class="thumbnail"><img id="image_example_' + div_id + '_' + i + '" class="img-fluid img-thumbnail" src="' + e.target.result + '" style="width:100%"></div></div>';
                            $('#show_images_' + div_id).append(image);
                        };
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }
    }

    function readURL1(input, div_id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            console.log(div_id);
            reader.onload = function(e) {
                $('#show_image_' + div_id).show();
                $("#btn_delete_image_info_"+div_id).show()
                $("#show_image_info_"+div_id).show()
                $('#show_standard_' + div_id).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#school_image').change(function() {
        readURLSchool(this);
    });

    function readURLSchool(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_school_example').attr('src', e.target.result);
                $('#show_school_image').show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php include viewPath('includes/footer');?>
<script>
    $(document).ready(function () {
        //Date picker
        $('#school_founded').datepicker();


    });

    function on_delete_download_file(school_id,school_download_id,school_download_files_id,file_name,action){

        showConfirmDialog(
            "แจ้งเตือน",
            "ยืนยันลบไฟล์ \""+file_name +"\" ออกจากรายการนี้",
            function(){
                let data = {
                        "school_id" : school_id,
                        "school_download_id" : school_download_id,
                        "school_download_files_id" : school_download_files_id,
                        "file_name":file_name
                    }
                $.ajax({
                    type: "POST",
                    url: base_url + 'manage_schools_data/school_download_item_delete',
                    data: data,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result)
                        if(result.data){
                            $(".school_download_link_"+school_download_files_id).hide()
                        }
                    }
                });
            },function(){

            }
        )
    }

    function on_delete_image_info(school_id,check_list_id,check_list_info_id,image_name){

        if(image_name==''){
                $("#show_standard_"+check_list_id).attr("src","")
                $("#"+check_list_id).val("")
                $("#btn_delete_image_info_"+check_list_id).hide()
                $("#show_image_info_"+check_list_id).hide()
            return;
        }
        let data = {
                        "school_id" : school_id,
                        "check_list_id" : check_list_id,
                        "check_list_info_id" : check_list_info_id,
                        "image_name":image_name
                    }
                $.ajax({
                    type: "POST",
                    url: base_url + 'manage_schools_data/school_check_list_info_delete',
                    data: data,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result)
                        if(result.data){
                            $("#show_standard_"+check_list_id).attr("src","")
                            $("#btn_delete_image_info_"+check_list_id).hide()
                            $("#show_image_info_"+check_list_id).hide()
                            $("#"+check_list_id).val("")
                        }
                    },
                    error:function(e){
                        console.log(e)
                    }
                });
    }
</script>
