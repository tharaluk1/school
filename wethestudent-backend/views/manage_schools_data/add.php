<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
    <?php include viewPath('includes/header');?>
    <link rel="stylesheet" href="<?php echo $url->assets ?>/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            เมนูจัดการข้อมูลโรงเรียน
            <small>
            School Data
        </small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- form start -->
        <!-- form-validate  -->
        <form class="form-horizontal form-validate" id="frm_school_data" novalidate action="<?php echo url('manage_schools_data/insert') ?>" method="post" enctype="multipart/form-data">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">เพิ่ม / แก้ไข</h3>

                    <div class="box-tools pull-right">
                        <a href="<?php echo url('manage_schools_data') ?>" class="btn btn-flat btn-default"><i
                                class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_name">
                            ชื่อโรงเรียน
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_name" name="school_name" required placeholder="ชื่อโรงเรียน" type="text">
                                </input>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_affiliation">
                            สังกัดสถานศึกษา
                        </label>
                            <div class="col-sm-8">
                                <select class="form-control" id="school_affiliation" name="school_affiliation" required>
                                <option value="">-= กรุณาเลือก =-</option>
                                <?php
foreach ($school_affiliation_list as $key => $row) {
    echo '<option value="' . $row->id . '">' . $row->school_affiliation . '</option>';
}
?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_founded">
                            วัน / เดือน / ปี ก่อตั้ง
                        </label>
                            <div class="col-sm-8">
                                <input data-date-language="th-th" data-provide="school_founded" autocomplete="off" class="form-control" id="school_founded" name="school_founded" placeholder="วัน / เดือน / ปี ก่อตั้ง" type="text"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_address">
                            ที่อยู่
                        </label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows="7" id="school_address" name="school_address" placeholder="ที่อยู่"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_province">
                            จังหวัด
                        </label>
                            <div class="col-sm-8">
                                <select class="form-control select2" id="school_province" name="school_province" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($province as $key => $row) {
    echo '<option value="' . $row->PROVINCE_ID . '">' . $row->PROVINCE_NAME . '</option>';
}
?>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_amphur">
                        อำเภอ
                        </label>
                            <div class="col-sm-8">
                                <select class="form-control select2" id="school_amphur" name="school_amphur" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($amphur as $key => $row) {
    echo '<option value="' . $row->AMPHUR_ID . '">' . $row->AMPHUR_NAME . '</option>';
}
?>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_district">
                        ตำบล
                        </label>
                            <div class="col-sm-8">
                                <select class="form-control select2" id="school_district" name="school_district" required>
                                        <option value="">-= กรุณาเลือก =-</option>
                                        <?php
foreach ($district as $key => $row) {
    echo '<option value="' . $row->DISTRICT_ID . '">' . $row->DISTRICT_NAME . '</option>';
}
?>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_post_code">
                        รหัสไปรษณีย์
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_post_code" name="school_post_code" placeholder="รหัสไปรษณีย์" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_tel">
                            เบอร์โทรศัพท์
                        </label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="school_tel" name="school_tel" placeholder="เบอร์โทรศัพท์" rows="7"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_type">
                            ประเภทโรงเรียน
                        </label>
                            <div class="col-sm-8">
                                <label class="radio-inline">
                                <input type="radio" name="school_type" id="school_type1" value="ST-001" checked> โรงเรียนรัฐบาล
                            </label>
                                <label class="radio-inline">
                                <input type="radio" name="school_type" id="school_type2" value="ST-002" > โรงเรียนเอกชน
                            </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_level">
                            ระดับที่เปิดสอน
                        </label>
                            <div class="col-sm-8">
                                <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" id="school_level1" value="อนุบาล" checked> อนุบาล
                            </label>
                                <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" id="school_level2" value="ประถมศึกษา" checked> ประถมศึกษา
                            </label>
                                <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" id="school_level3" value="มัธยมต้น" checked> มัธยมต้น
                            </label>
                                <label class="checkbox-inline">
                                <input type="checkbox" name="school_level[]" id="school_level4" value="มัธยมปลาย" checked> มัธยมปลาย
                            </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_style">
                            ลักษณะโรงเรียน
                        </label>
                            <div class="col-sm-8">
                                <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style1" value="SS-001"> ชายล้วน
                            </label>
                                <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style2" value="SS-002"> หญิงล้วน
                            </label>
                                <label class="radio-inline">
                                <input type="radio" name="school_style" id="school_style3" value="SS-003" checked> สหศึกษา
                            </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_student_primary_education">
                            จำนวนนักเรียนอนุบาล
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_student_primary_education" name="school_student_primary_education" placeholder="จำนวนนักเรียนอนุบาล" type="number">
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_student_upperelementary">
                            จำนวนนักเรียนประถมศึกษา
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_student_upperelementary" name="school_student_upperelementary" placeholder="จำนวนนักเรียนประถมศึกษา" type="number">
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_student_junior_high_school">
                            จำนวนนักเรียนมัธยมต้น
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_student_junior_high_school" name="school_student_junior_high_school" placeholder="จำนวนนักเรียนมัธยมต้น" type="number">
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_student_high_school">จำนวนนักเรียนมัธยมปลาย</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_student_high_school" name="school_student_high_school" placeholder="จำนวนนักเรียนมัธยมปลาย" type="number">
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_web">
                            เว็บไซต์
                        </label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_web" name="school_web" placeholder="เว็บไซต์" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_image">
                            รูปภาพ
                        </label>
                            <div class="col-sm-8">
                                <input type="file" accept=".jpg,.jpeg,.png" id="school_image" name="school_image">
                                <div id="show_school_image" style="display: none;">
                                    <br>
                                    <img id="image_school_example" src="" width="200">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_map">
                            ตำแหน่งโรงเรียน
                        </label>
                            <div class="col-sm-8">
                                <div id="mapid" style="height:450px;"></div>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-4">
                                <label class="control-label" for="school_lat">ละติจูด</label>
                                <input class="form-control" id="school_lat" name="school_lat" placeholder="ละติจูด" type="text" required>
                                </input>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="school_lng">ลองจิจูด</label>
                                <input class="form-control" id="school_lng" name="school_lng" placeholder="ลองจิจูด" type="text" required>
                                </input>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">School Download</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="school_source">แหล่งที่มาข้อมูล</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="school_source" name="school_source" placeholder="แหล่งที่มาของข้อมูล" type="text" value="">
                                </input>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
if (count($school_download) > 0) {
    foreach ($school_download as $key => $v) {
        ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="school_download#<?=$v->id?>"><?=$v->title?></label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="school_download#<?=$v->id?>" id="school_download#<?=$v->id?>" placeholder="Upload School Download" accept="application/msword, application/vnd.ms-excel, .xls, application/vnd.ms-powerpoint, text/plain, application/pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                        onclick="fileClicked(event)" onchange="fileChanged(event)">
                                </div>
                            </div>
                            <?php
}
}
?>
                    </div>
                </div>
            </div>

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">School Checklist</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    </div>
                </div>

                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <?php
$i = 1;
foreach ($school_checklist as $key => $v) {
    $active = '';
    if ($i == 1) {
        $active = 'active';
    }
    ?>
                            <li class="<?=$active?>"><a href="#tab_<?=$v->id?>" data-toggle="tab" aria-expanded="true"><?=$v->name?></a></li>
                            <?php
$i++;
}
?>
                    </ul>
                    <div class="tab-content">
                        <?php
$i = 1;
foreach ($school_checklist as $key => $v) {
    $active = '';
    if ($i == 1) {
        $active = 'active';
    }
    $check_list_id = $v->id;
    ?>
                            <div class="tab-pane <?=$active?>" id="tab_<?=$v->id?>">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">
                                            รูปภาพ (<span id="current_add_image_<?=$v->id?>">0</span>-20)
                                        </label>
                                            <div class="image_check_list_item_<?=$v->id?>">
                                                <input type="file" accept=".jpg, .jpeg, .png" class="images" onclick="school_check_list_image_click(this,<?=$v->id?>)" onchange="school_check_list_image_change(this,<?=$v->id?>)" id="image_check_list_<?=$v->id?>" name="image_check_list#<?=$v->id?>" multiple="multiple">
                                            </div>
                                            <div class="row" id="show_check_list_image_<?=$v->id?>"></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">รูปเกณฑ์มาตราฐาน</label>
                                            <input type="file" accept=".jpg, .jpeg, .png" class="images_standard" id="<?=$check_list_id?>" name="images_standard[<?=$check_list_id?>]">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-2 img-wraps" id="show_image_info_<?=$check_list_id?>" style="display: <?=empty($image)?'none':''?>">
                                                    <span class="closes" onclick="on_delete_image_info('<?=$check_list_id?>')" id="btn_delete_image_info_<?=$check_list_id?>"  title="Delete">&times;</span>
                                                    <img id="show_standard_<?=$check_list_id?>" src="<?=!empty($image->path) && $image->path != '' ? urlUpload('school_data/' . $id . '/images_standard' . '/' . $image->path) : 'data:,'?>" class="img-responsive image_check_list">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
$i++;
}
?>
                    </div>
                </div>
            </div>
                <!-- /.box -->

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">School Sharing</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <label for="school_sharing_tag">Tag Name</label>
                                    <input type="text" class="form-control" id="school_sharing_tag" placeholder="Tag Name">
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <label for="school_sharing_url">URL</label>
                                    <input type="text" class="form-control" id="school_sharing_url" placeholder="URL">
                                </div>
                                <div class="col-sm-12 col-md-2">
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <button type="button" class="btn btn-primary" style="width: 100%;" id="btn_add_tag">เพิ่ม</button>
                                </div>
                            </div>
                            <div id="form_tag"></div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- Default box -->
                <div class="box">
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-2">
                                <button type="button" id="btn_submit" style="width: 100%" class="btn btn-flat btn-primary">บันทึกข้อมูล</button>
                            </div>
                            <div class="col-md-5"></div>
                        </div>
                       
                    </div>
                    <!-- /.box-footer-->

                </div>
                <!-- /.box -->
        </form>
    </section>
    <!-- /.content -->
    <script>
        var schoolLat = 13.7384679;
        var schoolLng = 100.5298571;
    </script>
    <!-- <script src="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
    <script src="<?php echo $url->assets ?>js/school_map.js"></script>
    <script src="<?php echo $url->assets ?>js/school_data.js"></script>

    <script>
        var txt_form_tag_no = 1;

        $(document).ready(function() {
            
            //Initialize Select2 Elements
            // $('.select2').select2();
        });

        $('#btn_add_tag').on('click', function() {
            if ($('input[name="school_sharing_tag[]"]').length < 10) {
                var school_sharing_tag = $('#school_sharing_tag').val();
                var school_sharing_url = $('#school_sharing_url').val();
                var txt_form_tag = '<div id="tab_' + txt_form_tag_no + '"><hr><div class="row"><div class="col-sm-12 col-md-4"><label for="school_sharing_tag">Tag Name</label><input type="text" class="form-control" id="school_sharing_tag" name="school_sharing_tag[]" placeholder="Tag Name" value="' + school_sharing_tag + '"></div><div class="col-sm-12 col-md-6"><label for="school_sharing_url">URL</label><input type="text" class="form-control" id="school_sharing_url" name="school_sharing_url[]" placeholder="URL" value="' + school_sharing_url + '"></div><div class="col-sm-12 col-md-2"><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><button type="button" class="btn btn-danger btn_delete_tag" id="' + txt_form_tag_no + '" style="width: 100%;">ลบ</button></div></div></div>';
                $('#form_tag').append(txt_form_tag);
                $('#school_sharing_tag').val('');
                $('#school_sharing_url').val('');
                txt_form_tag_no++;
            }
        });

        $(document).on("click", ".btn_delete_tag", function() {
            var id = $(this).attr('id');
            $('#tab_' + id).remove();
        });

        $('#school_province').on('change', function() {
            var province = $(this).val();
            if (province != 0) {
                $.ajax({
                    url: "<?php echo base_url('manage_schools_data/get_amphur_list') ?>",
                    type: "post",
                    data: {
                        province: province
                    },
                    success: function(result) {
                        $('#school_amphur').html(result);
                        $('#school_amphur').change();
                    }
                });
            }
        });

        $('#school_amphur').on('change', function() {
            var amphur = $(this).val();
            if (amphur != 0) {
                $.ajax({
                    url: "<?php echo base_url('manage_schools_data/get_district_list') ?>",
                    type: "post",
                    data: {
                        amphur: amphur
                    },
                    success: function(result) {
                        $('#school_district').html(result);
                        $('#school_district').change();
                    }
                });
            }
        });

        $('#school_district').on('change', function() {
            var amphur = $('#school_amphur').val();
            if (amphur != 0) {
                $.ajax({
                    url: "<?php echo base_url('manage_schools_data/get_post_code') ?>",
                    type: "post",
                    data: {
                        amphur: amphur
                    },
                    success: function(result) {
                        $('#school_post_code').val(result);
                    }
                });
            }
        });

        // $('.images').change(function() {
        //     var id = $(this).attr('id');
        //     readURL(this, id);
        // });

        $('.images_standard').change(function() {
            var id = $(this).attr('id');
            readURL1(this, id);
        });

        // function readURL(input, div_id) {
        //     if (input.files) {
        //         var filesAmount = input.files.length;
        //         if (filesAmount > 20) {
        //             alert('เลือกรูปได้ไม่เกิน 20 รูป');
        //         } else {
        //             for (i = 0; i < filesAmount; i++) {
        //                 var reader = new FileReader();
        //                 reader.onload = function(e) {
        //                     //Initiate the JavaScript Image object.
        //                     var image = new Image();

        //                     //Set the Base64 string return from FileReader as source.
        //                     image.src = e.target.result;

        //                     //Validate the File Height and Width.
        //                     image.onload = function() {
        //                         var image = '<div class="col-sm-6 col-md-3"><div class="thumbnail"><img id="image_example_' + div_id + '_' + i + '" class="img-fluid img-thumbnail" src="' + e.target.result + '" style="width:100%"></div></div>';
        //                         $('#show_images_' + div_id).append(image);
        //                     };
        //                 }
        //                 reader.readAsDataURL(input.files[i]);
        //             }
        //         }
        //     }
        // }

        function readURL1(input, div_id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#show_standard_' + div_id).attr('src', e.target.result);
                    $('#show_image_info_' + div_id).show();
                    $("#btn_delete_image_info_"+div_id).show()
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#school_image').change(function() {
            readURLSchool(this);
        });

        function readURLSchool(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image_school_example').attr('src', e.target.result);
                    $('#show_school_image').show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <?php include viewPath('includes/footer');?>

    <script>
        $(document).ready(function() {
            //Date picker
            $('#school_founded').datepicker();

            // $('.closes').click(function(){
            //     var id = $(this).attr('id');


            // });
        });

        function on_delete_image_info(check_list_id){
            $("#show_standard_"+check_list_id).attr("src","")
            $("#btn_delete_image_info_"+check_list_id).hide()
            $("#show_image_info_"+check_list_id).hide()
            $("#"+check_list_id).val("")
        }
    </script>