<?php
defined('BASEPATH') or exit('No direct script access allowed');?>

<?php include viewPath('includes/header');?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $page->title; ?>
  </h1>
</section>

<!-- <'#comment_delete_view.col-sm-12 col-md-1'> -->

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">หน้ารายการ</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-danger" id="btn_delete_select">ลบที่เลือก</button>
      </div>
    </div>
    <div class="box-body">
      <!-- <table id="dataTableSchoolComment" class="table table-bordered table-striped display" style="width:100%"> -->
      <table id="dataTableSchoolComment" class="table table-bordered table-striped display" style="width:100%">
        <thead>
          <tr>
            <th>#</th>
            <th>ชื่อ</th>
            <th>ข้อความ</th>
            <th>วันที่แก้ไขล่าสุด</th>
            <th>Action  
              <div class="checkbox">
                <label><input type="checkbox" id="select_all"></label>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer">
      Footer
    </div> -->
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
<script src="<?php echo $url->assets ?>js/school_comment.js"></script>
<script type="text/javascript">

</script>
<?php include viewPath('includes/footer');?>

