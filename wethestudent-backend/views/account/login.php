<?php
defined('BASEPATH') or exit('No direct script access allowed');?>

<?php include 'includes/header.php'?>
<div class="login_image" id="login_image" style="text-align: center; margin-top:0%; margin-bottom:-50%">
  <img src="/assets/img/wethestudents-color-logo.png" width="40%"/>
</div>
<div class="login-box login-panel">
  <!-- /.login-logo -->
  <div class="login-box-body">
  <div class="row">
    <div class="col-sm-4 col-md-3 col-lg-3"></div>
    <div class="col-sm-4 col-md-6 col-lg-6">
      <img src="/assets/img/wethestudents-white-logo.png" width="100%"/>
    </div>
    <div class="col-sm-4 col-md-3 col-lg-3"></div>
  </div>
  <div class="login-logo">
    <a href="<?php echo url('/') ?>"><b>Admin</b> Panel</a>
  </div>
    <p class="login-box-msg">Sign in to start your session</p>
    <?php if (isset($message)): ?>
      <div class="alert alert-<?php echo $message_type ?>">
        <p><?php echo $message ?></p>
      </div>
    <?php endif;?>

    <?php if (!empty($this->session->flashdata('message'))): ?>
      <div class="alert alert-<?php echo $this->session->flashdata('message_type'); ?>">
        <p><?php echo $this->session->flashdata('message') ?></p>
      </div>
    <?php endif;?>

    <?php // include( VIEWPATH.'/includes/notifications.php' ); ?>


    <?php echo form_open('/login/check', ['method' => 'POST', 'autocomplete' => 'off']); ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Enter Username or Email..." value="<?php echo post('username') ?>" name="username" autofocus />
        <span class="fa fa-user form-control-feedback"></span>
        <?php echo form_error('username', '<div class="error" style="color: red;">', '</div>'); ?>
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="fa fa-lock form-control-feedback"></span>
        <?php echo form_error('password', '<div class="error" style="color: red;">', '</div>'); ?>
      </div>

      <?php if (setting('google_recaptcha_enabled') == '1'): ?>

      <script src="https://www.google.com/recaptcha/api.js" async defer></script>

      <div class="form-group">
        <div class="g-recaptcha" data-sitekey="<?php echo setting('google_recaptcha_sitekey') ?>"></div>
        <?php echo form_error('g-recaptcha-response', '<div class="error" style="color: red;">', '</div>'); ?>
      </div>

      <?php endif?>



      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" <?php echo post('remember_me') ? 'checked' : '' ?> name="remember_me" /> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->

        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close(); ?>

    <!-- <a href="<?php echo url('login/forget?username=' . post('username')) ?>">Forgot your password ?</a><br> -->
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php include 'includes/footer.php'?>

