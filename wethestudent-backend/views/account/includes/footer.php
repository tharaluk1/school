<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!-- jQuery 3 -->
<script src="<?php echo $assets ?>/js/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $assets ?>/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo $assets ?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

<script>
  $(document).ready(function() {
    let login_width = $('.login-panel').width();
    let body_width = $(window).width();
    let body_height = $(window).height();

    console.log("login_width: "+login_width);

    console.log("body_width: "+body_width);

    console.log("body_height: "+body_height);

    $("#login_image").width(body_width-login_width);

    $("#login_image").height(body_height);

  });

</script>
</body>
</html>
