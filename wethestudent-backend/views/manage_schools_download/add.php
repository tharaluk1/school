<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
<?php include viewPath('includes/header');?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        เมนูจัดการข้อมูลโรงเรียน
        <small>
            School Download
        </small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">เพิ่ม / แก้ไข</h3>

      <div class="box-tools pull-right">
        <a href="<?php echo url('manage_schools_download') ?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
        <!--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i></button>-->
      </div>

    </div>
    <div class="box-body">
        <!-- form start -->
        <form class="form-horizontal form-validate" action="<?php echo url('manage_schools_download/insert') ?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="title">
                        หัวข้อ
                    </label>
                    <div class="col-sm-8">
                        <input class="form-control" id="title" name="title" required placeholder="หัวข้อ" type="text">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="icon">
                        รูปไอคอน
                    </label>
                    <div class="col-sm-8">
                        <input type="file" accept=".jpg,.jpeg,.png" id="icon" name="icon">
                        <div id="show_icon_image" style="display: none;">
                            <br>
                            <img id="icon_image" src="" width="200">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <button type="submit" id="btn_submit" style="width: 100%" class="btn btn-flat btn-primary">บันทึกข้อมูล</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                    <!-- <div class="col-sm-offset-3 col-sm-8">
                        <button class="btn btn-flat btn-primary" type="submit" id="btn-submit" name="btn-submit">Save</button>
                    </div> -->
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
<script>
    $(document).ready(function() {
        $('.form-validate').validate();
    });

    $('#icon').change(function() {
        readURL(this);
    });

    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#icon_image').attr('src', e.target.result);
            $('#show_icon_image').show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<?php include viewPath('includes/footer');?>