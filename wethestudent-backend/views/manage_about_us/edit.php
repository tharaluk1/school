<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
<?php include viewPath('includes/header');?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $page->title; ?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">แก้ไข</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>

    </div>
    <div class="box-body">
        <!-- form start -->
        <form class="about_form form-horizontal form-validate" action="<?php echo url('manage_about_us/update/'); ?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="title">
                        หัวข้อ
                    </label>
                    <div class="col-sm-8">
                        <input class="form-control" id="title" name="title" required placeholder="หัวข้อ" type="text" value="<?php echo $data->title; ?>">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="description">
                        รายละเอียด
                    </label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="description" name="description" required placeholder="รายละเอียด"><?php echo $data->description; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="line_id">
                        Line ID
                    </label>
                    <div class="col-sm-8">
                        <input class="form-control" id="line_id" name="line_id" required placeholder="Line ID" type="text" value="<?php echo $data->line_id; ?>">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="image">
                        รูปภาพ
                    </label>
                    <div class="col-sm-7">
                        <input type="file" accept=".jpg,.jpeg,.png" id="image" name="image">
                        <div class="col-md-2 img-wraps">
                            <span  id="btn_delete_image_about" class="closes" title="Delete">&times;</span>
                            <img  id="image_about" src="<?php echo ($data->image != '' ? urlUpload('/about_us' . '/' . $data->image) : ''); ?>" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="form-group">

                <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <button type="submit" id="btn_submit" style="width: 100%" class="btn_save_about btn btn-flat btn-primary">บันทึกข้อมูล</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>

                    <!-- <div class="col-md-4">
                    </div>
                    <div class="col-md-4" style="text-align: center;">
                        <button class="btn_save_about btn btn-flat btn-primary" type="button" id="btn-submit" name="btn-submit" style="width: 40%;">Save</button>
                    </div>
                    <div class="col-md-4">
                    </div> -->
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
<script src="<?php echo $url->assets ?>plugins/ckeditor/ckeditor.js"></script>
<script>
    let is_delete_image_about = "0"
    $(document).ready(function() {
        $('.form-validate').validate();
        CKEDITOR.replace('description');
        CKEDITOR.config.height = 300;

        $("#btn_delete_image_about").click(function(){
            $("#image_about").attr('src',"");
            $("#btn_delete_image_about").hide();
            is_delete_image_about = "1";
            // showConfirmDialog(
            //     "แจ้งเตือน",
            //     "ยืนยันการลบรูปภาพ",
            //     function(){
            //         $("#image_about").attr('src',"");
            //         $("#btn_delete_image_about").hide();

            //         //TODO: Call api delete image about
            //         if('<?=$data->image?>' == '')
            //             return;
            //         $.ajax({
            //             type: "POST",
            //             url: base_url + 'manage_about_us/delete_image',
            //             data: {},
            //             dataType: 'json',
            //             success: function(result) {
            //                 console.log(result)
            //             }
            //         });

            //     },
            //     function(){

            //     }
            // )
        });

        $(".btn_save_about").click(function(){
            var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "image_delete").val(is_delete_image_about);
                $('.about_form').append(input);
            $('.about_form').submit();
        });

        if('<?=$data->image?>' == ''){
            $("#btn_delete_image_about").hide();
        }
    });

    $('#image').change(function() {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_about').attr('src', e.target.result);
                $('#show_image').show();
                $("#btn_delete_image_about").show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php include viewPath('includes/footer');?>