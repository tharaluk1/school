<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
<?php include viewPath('includes/header');?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?php echo $page->title; ?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">เพิ่ม / แก้ไข</h3>

      <div class="box-tools pull-right">
        <a href="<?php echo url('manage_affiliation_schools') ?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> ย้อนกลับ</a>
      </div>

    </div>
    <div class="box-body">
        <!-- form start -->
        <form class="form-horizontal form-validate" action="<?php echo url('manage_affiliation_schools/update/' . $id); ?>" method="post">
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="school_affiliation">
                        ชื่อสังกัดสถานศึกษา
                    </label>
                    <div class="col-sm-8">
                        <input class="form-control" id="school_affiliation" name="school_affiliation" required placeholder="ชื่อสังกัดสถานศึกษา" type="text" value="<?php echo $data->school_affiliation; ?>">
                        </input>
                    </div>
                </div>
                <div class="form-group">
                <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <button type="submit" id="btn_submit" style="width: 100%" class="btn btn-flat btn-primary">บันทึกข้อมูล</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
  $('.form-validate').validate();
});
</script>
<?php include viewPath('includes/footer');?>