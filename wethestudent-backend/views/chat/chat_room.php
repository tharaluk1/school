<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php include viewPath('includes/header'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $page->title;?>
  </h1>
</section>
<!-- form start -->
<script>
  var room = "<?=$room?>"
</script>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">ห้องแชท</h3>
      <div class="box-tools pull-right">
        <a href="<?php echo url('manage_chat') ?>" class="btn btn-flat btn-default">
          <i class="fa fa-arrow-left"></i> ย้อนกลับ
        </a>
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"title="Collapse">
          <i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body" style="padding-left: 200px;">

    <div class="row" >
    <div id="Text" class="col-12">
      <!-- <div class="container">
        <img src="https://www.w3schools.com/w3images/bandmember.jpg" alt="Avatar">
        <p>Hello. How are you today?</p>
        <span class="time-right">11:00</span>
      </div> -->
    </div>
    </div>


      <!-- <div class="container">
        <img src="https://www.w3schools.com/w3images/bandmember.jpg" alt="Avatar">
        <p>Hello. How are you today?</p>
        <span class="time-right">11:00</span>
      </div>

      <div class="container darker">
        <img src="https://www.w3schools.com/w3images/avatar_g2.jpg" alt="Avatar" class="right">
        <p>Hey! I'm fine. Thanks for asking!</p>
        <span class="time-left">11:01</span>
      </div> -->

      
      <!-- <input type="text" class="container" placeholder="กรอกอีเมลเพื่อติดตามข่าวสาร" id="EmailSubscribe" maxlength="30">
      <button type="button" class="btn btn-success">Success</button> -->

      <div class="container">
        <div class="form-group">
            <div class="col-sm-11">
              <input type="text" id="sendText" class="form-control"  placeholder="ข้อความ">
            </div>
            <button type="button" id="btnSend" class="btn btn-success">Send</button>
        </div>
      </div>

      

    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->

<?php include viewPath('includes/footer'); ?>
<script type="text/javascript">
$(document).ready(function() {
  Calldata();
  $("#sendText").keyup(function(event) {
    if (event.which == 13) {
      $("#btnSend").click();
    }
  });

});

function delay()
{
  console.log("checkdelay");
  Calldata();
}

// function Calldata_log() 
// {

// }

function Calldata() 
{
  $.ajax({
    url: "<?php echo base_url('manage_chat/data_chat_room_list') ?>",
    type: "post",
    data: {
        room: room
    },
    success: function (result) {
      console.log(result);
      // $("#Text div ").remove();
      $("#Text div ").remove();
      var texthtml = ''
      $.each(result, function(index,item) {
        if(item.type == 2){
          texthtml += `<div class="container"  style="width: 585px;">`
          texthtml +=    `<img src="https://www.w3schools.com/w3images/bandmember.jpg" alt="Avatar">`
          texthtml +=    `<p>${item.text}</p>`
          texthtml +=    `<span class="time-left">${item.create_date}</span>`
          texthtml +=`</div>`
        }else{
          texthtml += `<div class="container darker" style="width: 585px;margin-left: 585px;">`
          texthtml +=    `<img src="https://www.w3schools.com/w3images/avatar_g2.jpg" alt="Avatar" class="right">`
          texthtml +=    `<p style="text-align: right;">${item.text}</p>`
          texthtml +=    `<span class="time-right">${item.create_date}</span>`
          texthtml +=`</div>`
        }
      });
      $('#Text').append(texthtml);
      setTimeout(function() { delay() },5000)
    }
  });
}

$("#btnSend").click(function(){
  let text =  $("#sendText").val();
  if(text == ''){
    alert("กรุณากรอกข้อความ");
    return;
  }
  let data = {
              room: room,
              name: "admin",
              text: text,
              type:1,
              flag:1
            }
  $.ajax({
    url: "<?php echo base_url('manage_chat/send_text_admin') ?>",
    type: "post",
    data: data,
    success: function (result) {
      console.log(result);
      $("#sendText").val("");
      Calldata();
    }
  });
});

</script>


<style>

.container {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.container::after {
  content: "";
  clear: both;
  display: table;
}

.container img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.container img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: #aaa;
}

.time-left {
  float: left;
  color: #999;
}
</style>