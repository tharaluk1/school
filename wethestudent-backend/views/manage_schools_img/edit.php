<?php
defined('BASEPATH') or exit('No direct script access allowed');?>
<?php include viewPath('includes/header');?>
<link rel="stylesheet" href="<?php echo $url->assets ?>/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        เมนูจัดการรูปภาพ
        <small>
            School Img
        </small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- form start -->
    <script>
        school_id = <?=$id?>
    </script>
    <?php echo form_open_multipart('manage_schools_data/update/' . $id, 'id="frm_school_data" class="form-horizontal form-validate"'); ?>
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">เพิ่ม / แก้ไข</h3>
                <div class="box-tools pull-right">
                    <a href="<?php echo url('manage_schools_img') ?>" class="btn btn-flat btn-default"><i
                                class="fa fa-arrow-left"></i> ย้อนกลับ</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_name">
                            ชื่อ
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_name" name="school_name" readonly
                                   placeholder="ชื่อโรงเรียน" type="text" value="<?php echo $school_data->name; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_primary_education">Email</label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_primary_education"
                                   name="school_student_primary_education"
                                   placeholder="จำนวนนักเรียนอนุบาล" type="text" readonly
                                   value="<?php echo $school_data->email; ?>">
                            </input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="school_student_upperelementary">
                        เบอร์โทรศัพท์
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" id="school_student_upperelementary"
                                   name="school_student_upperelementary"
                                   placeholder="จำนวนนักเรียนประถมศึกษา" type="text" readonly
                                   value="<?php echo $school_data->tel; ?>">
                            </input>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">School Checklist</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="tab-content">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" aria-expanded="true"><?=$school_data->school_name_type?></a></li>
                    </ul>
                    <?php
                        $i = 1;
                        foreach ($school_checklist as $key => $v) {
                            $active = '';
                            if ($i == 1) {
                                $active = 'active';
                            }
                    ?>
                    <div class="tab-pane <?=$active?>" id="tab_<?=$school_data->school_checklist_id?>">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <!-- <label class="control-label">
                                        รูปภาพ  (<span id="current_add_image_<?=$school_data->school_checklist_id?>">0</span>-20)
                                    </label> -->
         
                                    <div class="row" id="show_check_list_image_<?=$school_data->school_checklist_id?>">
                                        <?php
                                            $element_id = $school_data->school_checklist_id;
                                            $images_check_list = $this->schools_model->getSchoolCheckListImages_cus($id, $element_id);
                                        ?>
                                                <script>
                                                    if(school_check_list_image['<?=$element_id?>'] == undefined)
                                                            school_check_list_image['<?=$element_id?>'] = []
                                                </script>
                                                <?php
                                                    foreach ($images_check_list as $image_check_list_item) {
                                                        $image_path = $image_check_list_item->path;
                                                        $preview_id = 'image_preview_' . $image_check_list_item->id . '_' . $element_id;         
                                                ?>
                                                    <script>
                                                            file_item = {
                                                                    "name": '<?=$image_path?>',
                                                                    "lastModified": <?=$image_check_list_item->id?>,
                                                                    "file_base": ""
                                                                }
                                                            school_check_list_image['<?=$element_id?>'].push(file_item)
                                                    </script>
                                                                    <div class="col-md-3 col-lg-2 img-wraps cursor-img" id="<?=$preview_id?>">
                                                                        <span onclick="delete_image_cus(<?=$element_id?>,<?=$image_check_list_item->id?>)" class="closes" style="top: -13px; color: #ff0000; background-color: rgb(255 255 255);border: 1px solid #ff0000;" title="Delete">
                                                                            <span class="glyphicon glyphicon-remove" style="top: -1px;"></span> 
                                                                        </span>
                                                                        <span onclick="approve_image_cus(<?=$image_check_list_item->id?>,<?=$preview_id?>)" class="closes" style="top: 126px; color: #00a65a; background-color: rgb(255 255 255);border: 1px solid #00a65a;" title="Delete">
                                                                            <span class="glyphicon glyphicon-check" style="top: -1px;"></span> 
                                                                        </span>
                                                                        <img src="<?=urlUpload('school_data/' . $id . '/school_checklist' . '/' . $image_path)?>" onclick="myFunction(this ,<?=$image_check_list_item->id?> , <?=$preview_id?> , <?=$element_id?>);" class="img-responsive image_check_list">
                                                                    </div> 
                                            <?php
                                                }
                                            ?>
                                            <script>
                                                // $('#current_add_image_'+<?=$element_id?>).text(school_check_list_image['<?=$element_id?>'].length);
                                            </script>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <?php 
                    $i++;
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- The Close Button -->
            <span class="close">&times;</span>

            <!-- Modal Content (The Image) -->
            <img class="modal-content" id="img01">

            <!-- Modal Caption (Image Text) -->
            <div id="caption">
            <div style="display: flex; justify-content:space-between; flex-direction:column-reverse;">
                <div class="close-del" style="color: #ff0000; " title="Delete">
                    <span class="glyphicon glyphicon-remove" style="font-size: 34px;"></span> 
                </div>
                <div class="closes-11" style="color: #00a65a; " title="Approve">
                    <span class="glyphicon glyphicon-check" style="font-size: 34px;"></span> 
                </div>
            </div>
            </div>
        </div>

        <!-- Default box -->
        <!-- <div class="box">
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                        <button type="button" id="btn_submit" style="width: 100%" class="btn btn-flat btn-primary">บันทึกข้อมูล</button>
                    </div>
                    <div class="col-md-5"></div>
                </div>
            </div> -->
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </form>
</section>
<!-- /.content -->

<script>
    var schoolLat = "<?=$school_data->lat;?>"
    var schoolLng = "<?=$school_data->lng;?>"
</script>
<!-- <script src="<?php echo $url->assets ?>plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<script src="<?php echo $url->assets ?>js/school_map.js"></script>
<script src="<?php echo $url->assets ?>js/school_img.js"></script>

<script>
    var txt_form_tag_no = <?php echo count($school_sharing); ?>;
    $(document).ready(function () {
        $('.form-validate').validate();
    });

    // $('#btn_submit').on('click', function () {
    //     $('#frm_school_data').submit();
    // });

    $('#btn_add_tag').on('click', function () {
        if ($('input[name="school_sharing_tag[]"]').length < 10) {
            var school_sharing_tag = $('#school_sharing_tag').val();
            var school_sharing_url = $('#school_sharing_url').val();
            var txt_form_tag = '<div id="tab_' + txt_form_tag_no + '"><hr><div class="row"><div class="col-sm-12 col-md-4"><label for="school_sharing_tag">Tag Name</label><input type="text" class="form-control" id="school_sharing_tag" name="school_sharing_tag[]" placeholder="Tag Name" value="' + school_sharing_tag + '"></div><div class="col-sm-12 col-md-6"><label for="school_sharing_url">URL</label><input type="text" class="form-control" id="school_sharing_url" name="school_sharing_url[]" placeholder="URL" value="' + school_sharing_url + '"></div><div class="col-sm-12 col-md-2"><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><button type="button" class="btn btn-danger btn_delete_tag" id="' + txt_form_tag_no + '"  style="width: 100%;">ลบ</button></div></div></div>';
            $('#form_tag').append(txt_form_tag);
            $('#school_sharing_tag').val('');
            $('#school_sharing_url').val('');
            txt_form_tag_no++;
        }
    });

    $(document).on("click", ".btn_delete_tag", function () {
        var id = $(this).attr('id');
        $('#tab_' + id).remove();
    });


</script>
<?php include viewPath('includes/footer');?>
<script>
    function myFunction(imgs,check_list_id,preview_id,element_id) {
        // Get the modal
        var modal = document.getElementById("myModal");

        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");

        modal.style.display = "block";
        modalImg.src = imgs.src;

        var span = document.getElementsByClassName("close")[0];
        var btn_approve = document.getElementsByClassName("closes-11")[0];
        var btn_delete = document.getElementsByClassName("close-del")[0];

        console.log(btn_approve);
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        btn_approve.onclick = function() {
            showConfirmDialog("แจ้งเตือน", "กรุณากดตกลงเพื่อยืนยัน", function() {
                approve_image_cus(check_list_id,preview_id);
                modal.style.display = "none";
            }, function() {
                return;

            });
        }

        btn_delete.onclick = function() {
            showConfirmDialog("แจ้งเตือน", "กรุณากดตกลงเพื่อลบรูปภาพ", function() {
                delete_image_cus(element_id,check_list_id);
                // modal.style.display = "none";
            }, function() {
                return;

            });
        }
        

    }

    function approve_image_cus(check_list_id,preview_id){
        let data = {
                    "check_list_id" : check_list_id
                }
        $.ajax({
            type: "POST",
            url: base_url + 'manage_schools_img/update_school_check_list_cus',
            data: data,
            dataType: 'json',
            success: function(result) {
                console.log(result)
                if(result.data){
                    $(preview_id).hide()
                    // $("#show_standard_"+check_list_id).attr("src","")
                    // $("#btn_delete_image_info_"+check_list_id).hide()
                    // $("#"+check_list_id).val("")
                }
            },
            error:function(e){
                console.log(e)
            }
        });
    }

    function delete_image_cus(element_id, file_lastModified) {
        let data = {
            "school_id": school_id,
            "school_check_list_id": element_id,
            "school_check_list_image_id": file_lastModified,
        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: base_url + 'manage_schools_data/school_check_list_image_delete',
            data: data,
            dataType: 'json',
            success: function(result) {
                console.log(result)
                location.reload();
            }
        });
    }

</script>




<style>
/* Style the Image Used to Trigger the Modal */
#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  z-index: 99999999; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {transform:scale(0)}
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #ffffff00;
  font-size: 60px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.closes-11:hover,
.close-11:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.closes-del:hover,
.close-del:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}
.cursor-img:hover,
.cursor-img:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}

</style>
