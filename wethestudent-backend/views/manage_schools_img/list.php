<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php include viewPath('includes/header'); ?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php echo $page->title;?>
  </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">หน้ารายการ</h3>


    </div>
    <div class="box-body">
      <table id="dataTableImg" class="table table-bordered table-striped display" style="width:100%">
        <thead>
          <tr>
            <th>#</th>
            <th>ชื่อสังกัด</th>
            <th>ชื่อโรงเรียน</th>
            <th>ประเภท</th>
            <th>เวลา</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer">
      Footer
    </div> -->
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->

<?php include viewPath('includes/footer'); ?>
<script src="<?php echo $url->assets ?>js/school_img.js"></script>