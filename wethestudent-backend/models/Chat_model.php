<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chat_model extends Mobility2u_Model
{

    public function chat_list_data($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $order_index = $input['order'][0]['column'];
        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (s.name like '%{$keyword}%')";
        }

        $select = "a.* , b.cnt , b.u_email";
        $this->db->select($select);
        $this->db->from("chat as a");
        $this->db->join( "(SELECT id, count( type ) AS cnt , email AS u_email  FROM chat WHERE type = 2 AND (flag = 0 || flag = 1) GROUP BY room) b" , 'a.id = b.id' ,'left');
        $this->db->where("flag != 2 ");
        $this->db->group_by('room');  

        $q = $this->db->get();
        // print_r($this->db->last_query());exit;
        $datas = $q->result_array();
        $count_row = $q->num_rows();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count_row;
        $data['recordsFiltered'] = $count_row;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;
    }

    public function log_chat_list_data($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $order_index = $input['order'][0]['column'];
        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (s.name like '%{$keyword}%')";
        }

        $select = "a.* , b.cnt , b.u_email";
        $this->db->select($select);
        $this->db->from("chat as a");
        $this->db->join( "(SELECT id, count( type ) AS cnt , email AS u_email  FROM chat WHERE type = 2 GROUP BY room) b" , 'a.id = b.id' ,'left');
        $this->db->group_by('room'); 

        $q = $this->db->get();
        // print_r($this->db->last_query());exit;
        $datas = $q->result_array();
        $count_row = $q->num_rows();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count_row;
        $data['recordsFiltered'] = $count_row;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;
    }

    public function get_chat_room($room)
    {
        $this->db->where('room', $room);
        $this->db->where("flag != 2 ");
        $query = $this->db->get('chat');
        // print_r($this->db->last_query());exit;
        return $query->result();
    }

    // public function get_chat_room_row($room)
    // {
    //     $this->db->where('room', $room);
    //     $this->db->where("flag != 2 ");
    //     $query = $this->db->get('chat');
    //     // print_r($this->db->last_query());exit;
    //     return $query->result();
    // }

    public function get_log_chat_detail($room)
    {
        $this->db->where('room', $room);
        $query = $this->db->get('chat');
        return $query->result();
    }

    public function insert_chat_admin($data)
    {
        return $this->db->insert('chat', $data);
    }

    public function delete_chat_text_room($array_where){

        $data = [
            'flag' => '2',
        ];
        $this->db->where($array_where);
        return $this->db->update('chat', $data);

    }
}