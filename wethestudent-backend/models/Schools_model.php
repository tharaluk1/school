<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Schools_model extends Mobility2u_Model
{
    public function getProvince()
    {
        $this->db->order_by('PROVINCE_NAME', 'asc');
        $query = $this->db->get('province');
        return $query->result();
    }

    public function getAmphur($province_id)
    {
        $this->db->where('PROVINCE_ID', $province_id);
        $this->db->order_by('AMPHUR_NAME', 'asc');
        $query = $this->db->get('amphur');
        return $query->result();
    }

    public function getDistrict($amphur_id)
    {
        $this->db->where('AMPHUR_ID', $amphur_id);
        $this->db->order_by('DISTRICT_NAME', 'asc');
        $query = $this->db->get('district');
        return $query->result();
    }

    public function getPostCode($amphur_id)
    {
        $this->db->where('AMPHUR_ID', $amphur_id);
        $query = $this->db->get('amphur');
        return $query->row();
    }

    public function getSchoolAffiliationList()
    {
        $this->db->where('status', 1);
        $query = $this->db->get($this->_school_affiliation());
        return $query->result();
    }

    public function getSchoolAffiliationByID($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get($this->_school_affiliation());
        return $query->row();
    }

    public function getSchoolDataByID($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get('school');
        return $query->row();
    }

    public function getSchoolDownloadByID($id)
    {
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get('school_download');
        return $query->row();
    }

    public function data_table_affiliation_schools_query($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (school_affiliation like '%{$keyword}%')";
        }
        $condition = $condition . ' and status != 0';
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get($this->_school_affiliation());
        $datas = $query->result();

        $count_condition = $this->db->from($this->_school_affiliation())->where($condition)->count_all_results();

        $condition = "1=1 and status != 0";
        $count = $this->db->from($this->_school_affiliation())->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;

        /*   $param['page_size'] = $input['length'];
    $param['start'] = $input['start'];
    $param['draw'] = $input['draw'];

    $param['keyword'] =
    $param['order_column'] = $input["columns"][$order_index]["data"];
    $param['order_action'] = $input['order'][0]['dir'];

    return $param;*/
    }

    public function data_table_schools_data_query($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (s.name like '%{$keyword}%')";
        }
        $condition = $condition . ' and s.status = 1';
        $this->db->select("s.*,sa.school_affiliation as school_affair");
        $this->db->join($this->_school_affiliation() . ' as sa', "sa.id = s.affiliation");
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get('school' . ' as s');
        // print_r($this->db->last_query());exit;
        $datas = $query->result();

        $count_condition = $this->db->from('school' . ' as s')->join($this->_school_affiliation() . ' as sa', "sa.id = s.affiliation")->where($condition)->count_all_results();

        $condition = "1=1 and s.status = 1";
        $count = $this->db->from('school' . ' as s')->join($this->_school_affiliation() . ' as sa', "sa.id = s.affiliation")->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;

        /*   $param['page_size'] = $input['length'];
    $param['start'] = $input['start'];
    $param['draw'] = $input['draw'];

    $param['keyword'] =
    $param['order_column'] = $input["columns"][$order_index]["data"];
    $param['order_action'] = $input['order'][0]['dir'];

    return $param;*/
    }

    public function data_table_schools_download_query($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (title like '%{$keyword}%')";
        }
        $condition = $condition . ' and status != 0';
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get('school_download');
        $datas = $query->result();

        $count_condition = $this->db->from('school_download')->where($condition)->count_all_results();

        $condition = "1=1 and status != 0";
        $count = $this->db->from('school_download')->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;

        /*   $param['page_size'] = $input['length'];
    $param['start'] = $input['start'];
    $param['draw'] = $input['draw'];

    $param['keyword'] =
    $param['order_column'] = $input["columns"][$order_index]["data"];
    $param['order_action'] = $input['order'][0]['dir'];

    return $param;*/
    }

    public function data_table_schools_comment_query($input,
        $school_id, $school_checklist_id) {

        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (sc.comment_message like '%{$keyword}%')";
        }

        if ($school_id != "0") {
            $condition .= " and sc.school_id = " . $school_id;
        }

        if ($school_checklist_id != "0") {
            $condition .= " and check_list_id = " . $school_checklist_id;
        }

        $condition = $condition . ' and sc.status = 1';

        $this->db->select("sc.*");
        $this->db->join($this->_school() . ' as s', 's.id = sc.school_id');
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get('school_comment' . ' as sc');
        $datas = $query->result();

        $count_condition = $this->db->from('school_comment' . ' as sc')->join($this->_school() . ' as s', 's.id = sc.school_id')->where($condition)->count_all_results();

        $condition = "1=1 and sc.status = 1";
        $count = $this->db->from('school_comment' . ' as sc')->join($this->_school() . ' as s', 's.id = sc.school_id')->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;

        /*   $param['page_size'] = $input['length'];
    $param['start'] = $input['start'];
    $param['draw'] = $input['draw'];

    $param['keyword'] =
    $param['order_column'] = $input["columns"][$order_index]["data"];
    $param['order_action'] = $input['order'][0]['dir'];

    return $param;*/
    }

    public function getSchoolSharingByID($id)
    {
        $this->db->where('school_id', $id);
        $query = $this->db->get('school_sharing');
        return $query->result();
    }

    public function getAboutUs()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('about_us');
        return $query->row();
    }

    public function getSchoolDownload()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('school_download');
        return $query->result();
    }

    public function getSchoolDownloadFiles($id)
    {
        $this->db->where('school_id', $id);
        $query = $this->db->get('school_data_download');
        return $query->result();
    }

    public function getSchoolCheckListMaster()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('school_checklist_master');
        return $query->result();
    }

    public function getCheckListItemsMaster()
    {
        $this->db->where('status', 1);
        $query = $this->db->get('school_checklist_items_master');
        return $query->result();
    }

    public function getCheckListItemsBySchool($school_id, $check_list_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_checklist_id', $check_list_id);
        $this->db->where('status', 1);
        $query = $this->db->get('school_checklist_items');
        return $query->result();
    }

    public function getSchoolCheckListImages($school_id, $check_list_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_checklist_id', $check_list_id);
        $this->db->where("(status  is null  )");//or status != 1
        $query = $this->db->get('school_data_checklist');
        // print_r($this->db->last_query());
        return $query->result();
    }

    public function getCheckListItemsImage($school_id, $check_list_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_checklist_id', $check_list_id);
        $query = $this->db->get('school_data_standard');
        return $query->row();
    }

    public function checkListPoint($school_id, $check_list_id, $school_checklist_item_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('check_list_id', $check_list_id);
        $this->db->where('check_list_item_id', $school_checklist_item_id);
        $query = $this->db->get('school_check_list_rating');
        return $query->row();
    }

    public function checkDataSchoolDownload($school_id, $school_download_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_download_id', $school_download_id);
        $query = $this->db->get('school_data_download');
        return $query->row();
    }

    public function select_school()
    {
        $this->db->where('status', 1);
        $query = $this->db->get($this->_school());
        return $query->result();
    }

    public function delete_image_check_list($array_where){
      return  $this->db->delete($this->_school_data_checklist(), $array_where);
    }


    public function count_school_info($where){
        $this->db->where($where);
        $this->db->from($this->_school_data_standard());
        return $this->db->count_all_results();
    }

    public function insert_school_check_list_info($data){
       return $this->db->insert($this->_school_data_standard(), $data);
    }

    public function delete_school_download($array_where){
        return  $this->db->delete($this->_school_data_download(), $array_where);
    } 
      
    public function delete_school_check_list_image_info($array_where){
        return  $this->db->delete($this->_school_data_standard(), $array_where);
    }


    public function data_table_schools_data_query_cus($input)
    {
        $order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $order_index = $input['order'][0]['column'];
        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (s.name like '%{$keyword}%')";
        }

        $select = "aa.school_id 
        ,aa.school_checklist_id
        ,s.name
        ,sa.school_affiliation
        , (CASE
        WHEN aa.school_checklist_id = 1 THEN 'ห้องเรียน'
        WHEN aa.school_checklist_id = 2 THEN 'ห้องน้ำ'
        WHEN aa.school_checklist_id = 3 THEN 'โรงอาหาร'
        WHEN aa.school_checklist_id = 4 THEN 'ลานกีฬา'
        ELSE ''
        END) AS school_name_type,
        aa.created_date,
        CONCAT(`aa`.`school_id`,'and' ,`aa`.`school_checklist_id`) AS key_cus 
        ";
        $this->db->select($select);
        $this->db->from("school_data_checklist as aa");
        $this->db->join("school_data_checklist_cus as bb ","(aa.id = bb.id_school_checklist)");
        $this->db->join("school as s ","(aa.school_id  = s.id )");
        $this->db->join("school_affiliation as sa ","(sa.id = s.affiliation)");
        $this->db->where("aa.status = 1");
        $this->db->where("bb.status_confirm = 0");
        $this->db->where($condition);
        $this->db->group_by('school_id , aa.school_checklist_id');  
        $this->db->order_by('bb.id', 'desc');
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);    

        $q = $this->db->get();
        //  print_r($this->db->last_query());exit;
        $datas = $q->result_array();
        $count_row = $q->num_rows();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count_row;
        $data['recordsFiltered'] = $count_row;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

        return $data;
    }

    public function getDetail_cus_ByID($school_id,$school_checklist_id)
    {
        $this->db->select("bb.* 
        ,aa.school_checklist_id
        ,(CASE
        WHEN aa.school_checklist_id = 1 THEN 'ห้องเรียน'
        WHEN aa.school_checklist_id = 2 THEN 'ห้องน้ำ'
        WHEN aa.school_checklist_id = 3 THEN 'โรงอาหาร'
        WHEN aa.school_checklist_id = 4 THEN 'ลานกีฬา'
        ELSE ''
        END) AS school_name_type
        ");
        $this->db->from("school_data_checklist as aa");
        $this->db->join("school_data_checklist_cus as bb ","(aa.id = bb.id_school_checklist)");
        $this->db->where('aa.school_id', $school_id);
        $this->db->where('aa.school_checklist_id', $school_checklist_id);
        $this->db->where("aa.status = 1");
        $this->db->where("bb.status_confirm = 0");
        $this->db->group_by('school_id');      
        
        $q = $this->db->get();
        $data = $q->row();
        // print_r($this->db->last_query());exit;

        return $data;
    }

    public function getSchoolCheckListImages_cus($school_id, $check_list_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_checklist_id', $check_list_id);
        $this->db->where('status = 1');

        $query = $this->db->get('school_data_checklist');

        // print_r($this->db->last_query());exit;
        
        return $query->result();
    }

    public function update_school_check_list_image_cus($array_where)
    {
        // return  $this->db->delete("school_data_checklist", $array_where);

        $data = [
            'status' => '2',
        ];
        $this->db->where($array_where);
        return $this->db->update('school_data_checklist', $data);

    }

    public function getSchoolCheckListImages_show_cus($school_id, $check_list_id)
    {
        $this->db->where('school_id', $school_id);
        $this->db->where('school_checklist_id', $check_list_id);
        $this->db->where("status = '2' ");
        $query = $this->db->get('school_data_checklist');
        // print_r($this->db->last_query());
        return $query->result();
    }
}
