<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_permissions_model extends Mobility2u_Model {

	public $table = 'school_role_permissions';

	public function __construct()
	{
		parent::__construct();
	}

	public function save_rols($data)
    {
        $this->db->insert_batch($this->table, $data);
        return $this->db->insert_id();
    }

    public function getRoleByWhere($whereArg, $args = [])
    {

        if (isset($args['order']))
            $this->db->order_by($args['order'][0], $args['order'][1]);

        return $this->db->get_where($this->table, $whereArg)->result();
    }
}

/* End of file Role_permissions_model.php */
/* Location: ./application/models/Role_permissions_model.php */