<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends Mobility2u_Model {

	public $table = 'permissions';

	public function __construct()
	{
		parent::__construct();
	}

	public function getMenuList()
	{
        $query = $this->db->get('school_menu');
        return $query->result();
	}

	public function getSubMenuList($menu_code)
	{
		$this->db->where('school_menu_code', $menu_code);
        $query = $this->db->get('school_menu_sub');
        return $query->result();
	}
}

/* End of file Permissions_model.php */
/* Location: ./application/models/Permissions_model.php */