<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends Mobility2u_Model {

	public $table = 'users';

	public function attempt($data)
	{
		$this->db->where('username', $data['username']);
		$this->db->or_where('email', $data['username']);

		$query = $this->db->get($this->table);

		// validate user
		if(!empty($query) && $query->num_rows() > 0){

			// checks the password
			if($query->row()->password == hash( "sha256", $data['password'] )){

				if ($query->row()->status==='1')
					return 'valid'; // if valid password and username and allowed
				else
					return 'not_allowed';

			}
			else
				return 'invalid_password'; // if invalid password

		}

		return false;

	}

	public function login($row, $remember = false)
	{
		$time = time();

		// encypting userid and password with current time $time
		$login_token = sha1($row->id.$row->password.$time);

		if($remember===false){
			$array = [
				'login' => true,
				// saving encrypted userid and password as token in session
				'login_token' => $login_token,
				'logged' => [
					'id' => $row->id,
					'time' => $time,
				]
			];
			$this->session->set_userdata( $array );
		}else{

			$data = [
				'id' => $row->id,
				'time' => time(),
			];
			$expiry = strtotime('+7 days');
			set_cookie( 'login', true, $expiry );
			set_cookie( 'logged', json_encode($data), $expiry );
			set_cookie( 'login_token', $login_token, $expiry );

		}

		$this->update($row->id, [
			'last_login' => date('Y-m-d H:m:i')
		]);

		$this->activity_model->add($row->name.' ('.$row->username.') Logged in', $row->id);

	}

	public function logout()
	{
		// Deleting Sessions
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('logged');
		// Deleting Cookie
		delete_cookie('login');
		delete_cookie('logged');
		delete_cookie('login_token');
	}
	

	public function resetPassword($data)
	{

		$this->db->where('username', $data['username']);
		$this->db->or_where('email', $data['username']);

		$user = $this->db->get_where($this->table)->row();

		if(!empty($user)){ }else{
			return 'invalid';
		}

		$reset_token	=	password_hash((time().$user->id), PASSWORD_BCRYPT);

		$this->db->where('id', $user->id);
		$this->db->update($this->table, compact('reset_token'));

		$this->email->from(setting('company_email'), setting('company_name') );
		$this->email->to($user->email);

		$this->email->subject('Reset Your Account Password | ' . setting('company_name') );

		$reset_link = url('login/new_password?token='.$reset_token);

		$data = getEmailShortCodes();
		$data['user_id'] = $user->id;
		$data['user_name'] = $user->name;
		$data['user_email'] = $user->email;
		$data['user_username'] = $user->username;
		$data['reset_link'] = $reset_link;

		$html = $this->parser->parse('templates/email/reset', $data, true);

		$this->email->message( $html );

		$this->email->send();

		return $user->email;

	}

	function is_role_use($role_id){
		$this->db->where(array("role"=>$role_id));
		$this->db->from($this->_users());
		return  $this->db->count_all_results()>0;
	}

	function data_table_user_query($input){

		
		$order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (u.name like '%{$keyword}%' OR u.username like '%{$keyword}%' OR u.email like '%{$keyword}%')";
		}

		$this->db->select("u.id,CONCAT(u.img_type) as image,u.name,u.email,sr.title as role,u.last_login,u.status");
		$this->db->join($this->_school_roles(). ' as sr',"sr.id = u.role");
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get($this->_users().' as u');
		$datas = $query->result();
		
		foreach($datas as $item){
			$item->image = userProfile($item->id);
		}

        $count_condition = $this->db->from($this->_users().' as u')->where($condition)->count_all_results();

        $condition = "1=1";
        $count = $this->db->from($this->_users())->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

		return $data;
	}
}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */