<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_model extends Mobility2u_Model {

	public $table = 'school_roles';

	public function __construct()
	{
		parent::__construct();
	}

	public function getRoleById($id)
    {
        return $this->db->get_where($this->table, [$this->table_key => $id])->row();
	}
	
	public function data_table_role_permission_query($input){
		$order_index = $input['order'][0]['column'];
        $keyword = trim($input['search']['value']);

        $condition = "1=1";
        if (!empty($keyword)) {
            $condition .= " and (title like '%{$keyword}%')";
        }
        $this->db->where($condition);
        $this->db->limit($input['length'], $input['start']);
        $this->db->order_by($input["columns"][$order_index]["data"], $input['order'][0]['dir']);

        $query = $this->db->get($this->_school_roles());
        $datas = $query->result();

        $count_condition = $this->db->from($this->_school_roles())->where($condition)->count_all_results();

        $condition = "1=1";
        $count = $this->db->from($this->_school_roles())->where($condition)->count_all_results();

        $data['draw'] = $input['draw'];
        $data['recordsTotal'] = $count;
        $data['recordsFiltered'] = $count_condition;
        $data['data'] = $datas;
        $data['error'] = $this->db->error();
        $data['start'] = $input['start'];

		return $data;
		
	}

	function delete_role($array = array()){
		return $this->db->delete($this->_school_roles(),$array);
	}
}

/* End of file Roles_model.php */
/* Location: ./application/models/Roles_model.php */