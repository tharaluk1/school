<?php


class Mobility2u_Controller extends CI_Controller
{
    public $page_data;

    /**
     * Extends by most of controllers not all controllers
     */

    public function __construct()
    {

        parent::__construct();

        if (!empty($this->db->username) && !empty($this->db->hostname) && !empty($this->db->database)) {
        } else {
            $this->users_model->logout();
            die('Database is not configured');
        }

        date_default_timezone_set(setting('timezone'));

        if (!is_logged()) {
            redirect('login', 'refresh');
        }

        $this->page_data['url'] = (object)[
            'assets' => assets_url() . '/',
            'base_url' => url()
        ];

        $this->page_data['app'] = (object)[
            'site_title' => setting('company_name')
        ];

        $this->page_data['page'] = (object)[
            'title' => 'Dashboard',
            'menu' => 'dashboard',
            'submenu' => '',
        ];

        $list_menu = $this->db->get('school_menu')->result();
        foreach ($list_menu as $key => $v) {
            $v->submenu = $this->db->where('school_menu_code', $v->code)->get('school_menu_sub')->result();
        }
        
        $this->page_data['menu_list'] = $list_menu;
    }

    function loadView($viewPath, $data = array())
    {
        $this->load->view($viewPath, $data);
    }

    function showSuccessMessage($message)
    {
        $this->session->set_flashdata('alert-type', 'success');
        $this->session->set_flashdata('alert', $message);
    }

    function showErrorMessage($message)
    {
        $this->session->set_flashdata('alert-type', 'danger');
        $this->session->set_flashdata('alert', $message);
    }

    function showWarningMessage($message)
    {
        $this->session->set_flashdata('alert-type', 'warning');
        $this->session->set_flashdata('alert', $message);
    }

    function getDateNow($format = "Y-m-d H:i:s")
    {
        return date($format);
    }

    function inputGet($key = null)
    {
        if ($key == null)
            return $this->input->get();
        return $this->input->get($key);
    }

    function inputPost($key = null)
    {
        if ($key == null)
            return $this->input->get();
        return $this->input->get($key);
    }

    function jsonResponse($dataArray = array())
    {
        $this->output->set_content_type('application/json')
            ->set_output(json_encode($dataArray));
    }

    function inputSegment($index = 0)
    {
        return $this->uri->segment($index);
    }

    function sendResponse($data = array())
    {
        if (!$data) {
            $data = array();
        }
        $size = 1;
        if (is_array($data))
            $size = sizeof($data);
        $res = array(
            "statusCode" => $size == 0 ? ResponseCode::HTTP_NOT_FOUND : ResponseCode::HTTP_OK,
            "message" => $size == 0 ? "ไม่พบข้อมูล" : "สำเร็จ",
            "data" => $data
        );
        echo json_encode($res);
    }
}